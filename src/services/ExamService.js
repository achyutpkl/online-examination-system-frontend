import { apiUrls } from '../utils/constants';
import { get, post, put, patch, del } from '../utils/httpUtil';

export const getExams = async props => {
  let examType = undefined;

  if (typeof props === 'object') {
    examType = props.examType ? props.examType : '';
  }

  // export const getExams = async (filter, size = 100) => {
  //   let searchQuery = '';
  //   for (let key in filter) {
  //     if (isArray(filter[key]) && filter[key].length > 0) {
  //       searchQuery += key + ':';
  //       searchQuery += addFilterForArray(filter[key]);
  //     } else if (!isArray(filter[key]) && filter[key]) {
  //       searchQuery += `${key}: ${filter[key]}`;
  //     }
  // if (typeof props === 'string') {
  //   examType = props;
  // }

  const searchQuery = examType ? 'search=examType:' + examType + '&' : '';
  const url = `${apiUrls.exam}?${searchQuery}size=2000&sortby=createdAt`;
  const response = await get(url);

  return (response && response.data) || [];
};

export const getExam = async examId => {
  const response = await get(`${apiUrls.exam}/${examId}`);

  return (response && response.data) || {};
};

export const getExamResult = async examId => {
  const response = await get(apiUrls.examScore.replace(':examId', examId));

  return (response && response.data) || {};
};

export const addExam = async examDetails => {
  const response = await post(apiUrls.exam, examDetails);

  return (response && response.data) || {};
};

export const editExam = async examDetails => {
  const response = await put(
    `${apiUrls.exam}/${examDetails.id}/details`,
    examDetails,
  );

  return (response && response.data) || {};
};

export const publishExamResults = async examId => {
  const response = await patch(`${apiUrls.exam}/${examId}/publish-results`, {});

  return (response && response.data) || {};
};

export const getExamDetails = async examId => {
  const response = await get(`${apiUrls.exam}/${examId}/details`);

  return (response && response.data) || {};
};

/**
 * Call API to delete and return response.
 *
 * @param {examId} examId Id of exam to be deleted.
 * @returns {Object} Response from API.
 */
export const deleteExam = async examId => {
  const response = await del(`${apiUrls.exam}/${examId}`);

  return response || {};
};

export const submitExamAnswers = async (examId, answers) => {
  const response = await post(`${apiUrls.exam}/${examId}/answers`, answers);

  return (response && response.data) || {};
};

export const getExamScores = async examId => {
  const response = await get(`${apiUrls.exam}/${examId}/scores`);

  return (response && response.data) || [];
};

export const getExamScoreDetails = async (examId, scoreId) => {
  const response = await get(`${apiUrls.exam}/${examId}/scores/${scoreId}`);

  return (response && response.data) || [];
};

export const getStudentExamScoreByAdmin = async (examId, scoreId) => {
  const response = await get(
    `${apiUrls.exam}/${examId}/students/${scoreId}/scores`,
  );

  return (response && response.data) || [];
};

export const getExamTypes = async () => {
  const response = await get(apiUrls.examType);

  return (response && response.data && response.data.data) || [];
};
