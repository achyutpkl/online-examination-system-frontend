import { apiUrls } from '../utils/constants';
import { get } from '../utils/httpUtil';

// TODO: Maybe move this to a constants file
const DEFAULT_PAGE_SIZE = 500;

export const getSubjects = async (pageSize = DEFAULT_PAGE_SIZE) => {
  let subjects = await get(`${apiUrls.subject}?size=${pageSize}`);

  return (subjects && subjects.data) || [];
};

export const getDivisions = async (subjectId, pageSize = DEFAULT_PAGE_SIZE) => {
  let divisions = await get(
    `${apiUrls.subject}/${subjectId}/divisions?size=${pageSize}`,
  );

  return (divisions && divisions.data) || [];
};

export const getSubDivisions = async (
  divisionId,
  pageSize = DEFAULT_PAGE_SIZE,
) => {
  let subDivisions = await get(
    `${apiUrls.division}/${divisionId}/subdivisions?size=${pageSize}`,
  );

  return (subDivisions && subDivisions.data) || [];
};
