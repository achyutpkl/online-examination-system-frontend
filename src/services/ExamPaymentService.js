import uuid from 'uuid';

import { apiUrls } from '../utils/constants';
import { get, post } from '../utils/httpUtil';

export const getPaymentIdForExam = () => uuid.v4();

export const getExamPaymentDetails = async (examId, studentId) => {
  const response = await get(
    `${apiUrls.examPaymentDetails}/exams/${examId}/students/${studentId}`,
  );

  return (response && response.data) || {};
};

export const addExamPaymentDetail = async details => {
  const response = await post(apiUrls.examPaymentDetails, details);

  return (response && response.data) || {};
};
