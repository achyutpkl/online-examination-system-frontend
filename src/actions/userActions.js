import { userActionTypes as actionTypes } from './actionTypes';

export const setUser = user => ({ type: actionTypes.SET_USER, payload: user });
