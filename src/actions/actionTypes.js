import { APP_NAME } from '../utils/constants';

// user action types
export const SET_USER = `${APP_NAME}/User/SET_USER`;
// export all user action types
export const userActionTypes = {
  SET_USER,
};
