import React from 'react';

const QuestionTitle = ({ className, ...otherProps }) => (
  <div className={`no-child-paragraph-margin ${className}`} {...otherProps} />
);

export default QuestionTitle;
