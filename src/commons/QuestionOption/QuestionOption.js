import React from 'react';

const QuestionOption = ({ className = '', ...otherProps }) => (
  <div className={`no-child-paragraph-margin ${className}`} {...otherProps} />
);

export default QuestionOption;
