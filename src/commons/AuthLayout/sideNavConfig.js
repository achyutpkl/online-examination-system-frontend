import { canEnter } from './utils';

class SideNavItem {
  constructor(name, url, icon, children) {
    this.name = name;
    this.url = url;
    this.icon = icon;
    this.children = children;
  }
}

const getSideNavConfigByRolesString = (rolesString, routes) => {
  return {
    items: routes.reduce((navItems, authRoute) => {
      if (!canEnter(rolesString, authRoute.roles) || authRoute.isNotInSideNav) {
        return navItems;
      }

      navItems.push(
        new SideNavItem(
          authRoute.sideNavName,
          authRoute.path,
          authRoute.icon,
          authRoute.children,
        ),
      );

      return navItems;
    }, []),
  };
};

const getMemoizedGetSideNavConfigByRolesString = () => {
  let cache = {};

  return (rolesString, routes) => {
    // if (rolesString in cache) {
    //   return cache[rolesString];
    // }
    let result = getSideNavConfigByRolesString(rolesString, routes);

    cache[rolesString] = result;

    return result;
  };
};

export const memoizedGetSideNavConfigByRolesString = getMemoizedGetSideNavConfigByRolesString();
