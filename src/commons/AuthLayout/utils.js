/**
 * Check if a set of user roles can access route with given allowed roles.
 *
 * @param {string | Array<string>} userRoles Roles of user
 * @param {array} allowedRoles Allowed roles for a route
 * @returns {Node} React node for home view
 */
export const canEnter = (userRoles = [], allowedRoles = []) =>
  allowedRoles.some(userRole => userRoles.includes(userRole));
