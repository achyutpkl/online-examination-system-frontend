import React from 'react';

const AuthFooter = () => (
  <>
    <span>&copy;</span>
    <a href="http://entrancecart.com">Entrance Cart</a> &nbsp;
    {new Date().getFullYear()}
  </>
);

export default AuthFooter;
