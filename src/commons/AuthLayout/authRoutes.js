import { userRole, viewRoutes } from '../../utils/constants';

import User from '../../views/User/User';
import ExamType from '../../views/ExamType';
import EditExam from '../../views/Exams/EditExam';
import Profile from '../../views/Profile/Profile';
import StudentExam from '../../views/StudentExam';
import Subjects from '../../views/Subjects/Subjects';
import CreateExam from '../../views/Exams/CreateExam';
import Dashboard from '../../views/Dashboard/Dashboard';
import Register from '../../views/User/common/Register';
import ExamPaper from '../../views/StudentExam/ExamPaper';
import AddQuestion from '../../views/Question/AddQuestion';
import AdminExamList from '../../views/Exams/AdminExamList';
import StudentExamList from '../../views/StudentExam/StudentExamList';
import ProgressReport from '../../views/ProgressReport/ProgressReport';
import PracticeBoard from '../../views/PracticeQuestion/PracticeBoard';
import AdminResources from '../../views/AdminResources/AdminResources';
import StudentPayment from '../../views/StudentPayments/StudentPayments';
import StudentDashboard from '../../views/StudentDashboard/StudentDashboard';
import PracticeQuestion from '../../views/PracticeQuestion/PracticeQuestion';
import ViewPracticeQuestions from '../../views/Question/ViewPracticeQuestions';
import StudentExamScores from '../../views/StudentExamScores/StudentExamScores';
import StudentExamScoreDetails from '../../views/StudentExamScores/StudentExamScoreDetails';

const routes = [
  {
    path: viewRoutes.adminDashboard,
    name: 'Dashboard',
    roles: [userRole.admin],
    component: Dashboard,
    icon: 'icon-speedometer',
    sideNavName: 'Dashboard',
  },
  {
    path: viewRoutes.subjects,
    name: 'Subjects',
    roles: [userRole.admin],
    component: Subjects,
    icon: 'icon-puzzle',
    sideNavName: 'Subjects',
  },
  {
    path: viewRoutes.addQuestions,
    name: 'Add Question',
    roles: [userRole.admin],
    component: AddQuestion,
    icon: 'icon-plus',
    sideNavName: 'Add Practice Question',
  },
  {
    path: viewRoutes.viewPracticeQuestions,
    name: 'View Question',
    roles: [userRole.admin],
    component: ViewPracticeQuestions,
    icon: 'icon-plus',
    sideNavName: 'View Practice Question',
  },
  {
    path: viewRoutes.createExam,
    name: 'Create Exam',
    roles: [userRole.admin],
    component: CreateExam,
    icon: 'icon-plus',
    sideNavName: 'Create Exam',
  },
  {
    path: viewRoutes.adminExamList,
    name: 'Exams',
    roles: [userRole.admin],
    component: AdminExamList,
    icon: 'icon-plus',
    sideNavName: 'Exams',
    exact: true,
  },
  {
    path: viewRoutes.adminExamEdit,
    name: 'Edit Exam',
    roles: [userRole.admin],
    component: EditExam,
    isNotInSideNav: true,
  },
  {
    path: viewRoutes.examType,
    name: 'Exam Type',
    roles: [userRole.admin],
    component: ExamType,
    icon: 'icon-list',
    sideNavName: 'Exam Type',
  },
  {
    path: viewRoutes.adminResources,
    name: 'Admin Resources',
    roles: [userRole.admin],
    component: AdminResources,
    icon: 'icon-layers',
    sideNavName: 'Resources',
  },
  {
    path: viewRoutes.studentDashboard,
    name: 'Dashboard',
    roles: [userRole.student],
    component: StudentDashboard,
    icon: 'icon-speedometer',
    sideNavName: 'Dashboard',
  },
  // TODO: Edit the following two routes for StudentExamList
  {
    path: `${viewRoutes.studentExamList}/:title`,
    name: 'Exams',
    roles: [userRole.student],
    component: StudentExamList,
    icon: 'cui-puzzle',
    sideNavName: 'Exams',
    isNotInSideNav: true,
  },
  {
    path: `${viewRoutes.studentPracticeBoard}`,
    name: 'Subjects',
    roles: [userRole.student],
    component: PracticeBoard,
    icon: 'icon-book-open',
    sideNavName: 'Subjects',
    exact: true,
  },
  {
    path: viewRoutes.studentExamList,
    name: 'Exams',
    roles: [userRole.student],
    component: StudentExamList,
    icon: 'cui-puzzle',
    sideNavName: 'Exams',
    isOpen: false,
  },
  {
    path: viewRoutes.studentExamDetails,
    name: 'Exam Details',
    roles: [userRole.student],
    component: StudentExam,
    icon: 'icon-plus',
    sideNavName: 'Exam View',
    isNotInSideNav: true,
  },
  {
    path: viewRoutes.studentExamPaper,
    name: 'ExamPaper',
    roles: [userRole.student],
    component: ExamPaper,
    icon: 'icon-plus',
    sideNavName: 'Take Exam',
    isNotInSideNav: true,
  },
  {
    path: viewRoutes.studentExamScores,
    name: 'ExamScores',
    roles: [userRole.student],
    component: StudentExamScores,
    isNotInSideNav: true,
    exact: true,
  },
  {
    path: viewRoutes.studentExamScoreDetails,
    name: 'ExamScoreDetails',
    roles: [userRole.student],
    component: StudentExamScoreDetails,
    isNotInSideNav: true,
    exact: true,
  },
  {
    path: viewRoutes.adminStudentExamScoreDetails,
    name: 'ExamScoreDetails',
    roles: [userRole.admin],
    component: StudentExamScoreDetails,
    isNotInSideNav: true,
    exact: true,
  },
  {
    path: viewRoutes.studentPracticeQuestions,
    name: 'Practice Question',
    roles: [userRole.student],
    component: PracticeQuestion,
    isNotInSideNav: true,
  },
  {
    path: viewRoutes.studentPracticeBoard,
    exact: true,
    name: 'Practice Board',
    roles: [userRole.student],
    component: PracticeBoard,
    isNotInSideNav: true,
  },
  {
    path: viewRoutes.studentProgressReport,
    name: 'ProgressReport',
    roles: [userRole.student],
    component: ProgressReport,
    icon: 'icon-graph',
    sideNavName: 'Progress Report',
  },
  {
    path: viewRoutes.adminProfile,
    name: 'Profile',
    roles: [userRole.admin],
    component: Profile,
    icon: 'icon-plus',
    sideNavName: 'Profile',
    isNotInSideNav: true,
  },
  {
    path: viewRoutes.studentProfile,
    name: 'Profile',
    roles: [userRole.student],
    component: Profile,
    icon: 'icon-plus',
    sideNavName: 'Profile',
    isNotInSideNav: true,
  },
  {
    path: viewRoutes.adminUser,
    name: 'User',
    roles: [userRole.admin],
    component: User,
    icon: 'icon-plus',
    sideNavName: 'User List',
  },
  {
    path: viewRoutes.adminRegister,
    name: 'Register',
    roles: [userRole.admin],
    component: Register,
    sideNavName: 'Create User',
    icon: 'icon-plus',
  },
  {
    path: viewRoutes.studentPayments,
    name: 'Payments',
    roles: [userRole.student],
    component: StudentPayment,
    sideNavName: 'Payments',
    icon: 'icon-plus',
    isNotInSideNav: true,
  },
];

export default routes;
