import { Button } from 'reactstrap';
import React, { Component } from 'react';
import { AppSidebarToggler } from '@coreui/react';

import avatar from '../../assets/images/avatar.jpg';

class AuthHeader extends Component {
  openProfile = () => {
    const paths = this.props.location.pathname.split('/');

    this.props.history.push(`/${paths[1]}/profile`);
  };

  render() {
    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppSidebarToggler className="d-md-down-none" display="lg" />
        <div className="pr-1">
          <img
            src={avatar}
            alt="Profile"
            className="profile-image"
            onClick={this.openProfile}
          />
          <Button onClick={this.props.handleLogout}>Logout</Button>
        </div>
      </React.Fragment>
    );
  }
}

export default AuthHeader;
