import { Container } from 'reactstrap';
import React, { Component } from 'react';
import { Switch, Redirect } from 'react-router-dom';

import {
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarNav,
  AppBreadcrumb,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarFooter,
  AppSidebarMinimizer,
} from '@coreui/react';

import routes from './authRoutes';
import { canEnter } from './utils';
import { viewRoutes, userRole } from '../../utils/constants';
import { memoizedGetSideNavConfigByRolesString } from './sideNavConfig';

import AuthHeader from './AuthHeader';
import AuthFooter from './AuthFooter';
import PrivateRoute from '../PrivateRoute/PrivateRoute';
import { getSubjects } from '../../services/SubjectService';
import { showApiErrorMessage } from '../../utils/httpUtil';
import PracticeBoard from '../../views/PracticeQuestion/PracticeBoard';
import { getExamTypes } from '../../services/ExamService';
import StudentExamList from '../../views/StudentExam/StudentExamList';

const subjectIcon = {
  english: 'icon-docs',
  health: 'icon-heart',
  chemistry: 'icon-chemistry',
  physics: 'icon-rocket',
  botany: 'icon-eye',
  biology: 'icon-eye',
  zoology: 'icon-social-twitter',
  math: 'icon-pie-chart',
  'applied science': 'icon-paper-plane',
};

class AuthLayout extends Component {
  state = {
    subjects: [],
    routes: routes,
    isLoadingSubjects: false,
    isLoadingExamTypes: false,
  };

  componentDidMount() {
    this.fetchSubjects();
    this.fetchExamTypes();
  }

  fetchSubjects = async () => {
    this.setState({ isLoadingSubjects: true });

    let { data } = (await getSubjects().catch(showApiErrorMessage)) || {
      data: [],
    };

    const subjectIndex = this.state.routes.findIndex(item => {
      return item.name === 'Subjects' && item.roles.includes(userRole.student);
    });

    let routes = this.state.routes.slice();
    if (subjectIndex !== -1) {
      routes[subjectIndex].children = data.map(item => ({
        url: `${viewRoutes.studentPracticeBoard.replace(
          ':subjectId',
          item.id,
        )}`,
        name: item.title,
        roles: [userRole.student],
        component: PracticeBoard,
        icon: this.getIconForSubject(item.title),
        sideNavName: item.title,
      }));
    }

    this.setState({ routes, isLoadingSubjects: false });
  };

  getIconForSubject = subjectname => {
    const keys = Object.keys(subjectIcon);
    const index = keys.findIndex(key => {
      return subjectname.toLowerCase().includes(key);
    });

    if (index !== -1) {
      return subjectIcon[keys[index]];
    }

    return 'icon-plus';
  };

  fetchExamTypes = async () => {
    this.setState({ isLoadingExamTypes: true });

    let data = (await getExamTypes().catch(showApiErrorMessage)) || [];

    const examIndex = this.state.routes.findIndex(item => {
      return (
        item.path === viewRoutes.studentExamList &&
        item.roles.includes(userRole.student)
      );
    });

    let routes = this.state.routes.slice();
    if (examIndex !== -1) {
      routes[examIndex].children = data.map(item => ({
        url: `${viewRoutes.studentExamList}/${item.title}`,
        name: item.title,
        roles: [userRole.student],
        component: StudentExamList,
        icon: this.getIconForSubject(item.title),
        sideNavName: item.title,
      }));
    }

    this.setState({ routes, isLoadingExamTypes: false });
  };

  render() {
    const { roles } = this.props;
    const { pathname } = this.props.location;
    const sideNavItems = memoizedGetSideNavConfigByRolesString(
      (roles || []).join(','),
      this.state.routes,
    );

    if (this.state.isLoadingSubjects || this.state.isLoadingExamTypes) {
      return null;
    }

    return (
      <div className="app">
        <AppHeader fixed>
          <AuthHeader {...this.props} handleLogout={this.props.handleLogout} />
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg">
            <AppSidebarHeader />
            <AppSidebarForm />
            <AppSidebarNav navConfig={sideNavItems} {...this.props} />
            <AppSidebarFooter />
            <AppSidebarMinimizer />
          </AppSidebar>
          <main className="main">
            <AppBreadcrumb appRoutes={routes} />
            <Container fluid className="authLayoutContent">
              <Switch>
                {routes.map((route, idx) => {
                  return route.component ? (
                    <PrivateRoute
                      key={idx}
                      redirectPath="/404"
                      path={route.path}
                      exact={route.exact}
                      name={route.name}
                      isAllowed={canEnter(roles, route.roles)}
                      component={route.component}
                      componentProps={{ handleLogout: this.handleLogout }}
                    />
                  ) : null;
                })}

                <Redirect
                  exact
                  from={viewRoutes.admin}
                  to={viewRoutes.adminDashboard}
                />

                <Redirect
                  exact
                  from={viewRoutes.student}
                  to={viewRoutes.studentDashboard}
                />

                <Redirect
                  to={{
                    pathname: '/404',
                    state: { pathname },
                  }}
                />
              </Switch>
            </Container>
          </main>
        </div>
        <AppFooter>
          <AuthFooter />
        </AppFooter>
      </div>
    );
  }
}

export default AuthLayout;
