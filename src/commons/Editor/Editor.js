import katex from 'katex';
import CKEditor from 'ckeditor4-react';
import React, { PureComponent } from 'react';

import './styles.css';
import 'katex/dist/katex.min.css';

class Editor extends PureComponent {
  constructor(props) {
    super(props);

    window.katex = katex;
  }

  handleOnChange = e => {
    this.props.handleOnChange(e.editor.getData());
  };

  render() {
    const { placeholder, value } = this.props;

    return (
      <div className="editor">
        <CKEditor
          data={value || ''}
          placeholder={placeholder}
          onChange={this.handleOnChange}
          config={{
            height: 150,
          }}
        />
      </div>
    );
  }
}

export default Editor;
