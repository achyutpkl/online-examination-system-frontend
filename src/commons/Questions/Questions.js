import { toast } from 'react-toastify';
import React, { Component } from 'react';
import { Row, Col, Card, Input, Label, Button } from 'reactstrap';

import './styles.scss';

import { arrayMove } from '../../utils/stateModifications';
import { getQuestionsFromFile } from '../../utils/fileUtils';
import { maxQuestionsToImport } from '../../utils/constants';

import QuestionList from './commons/QuestionList';
import QuestionFormModal from './commons/QuestionFormModal';

class Questions extends Component {
  state = {
    isShowForm: false,
    isFormEdit: false,
    editQuestionDetails: null,
  };

  toggleForm = () => this.setState({ isShowForm: !this.state.isShowForm });

  onFormSubmit = (question = {}) => {
    const currentQuestions = this.props.questions;

    let questionIndex = currentQuestions.length;

    if (this.state.isFormEdit && this.state.editQuestionDetails) {
      questionIndex = this.state.editQuestionDetails.questionIndex;
    }

    this.toggleForm();

    this.props.setQuestions([
      ...currentQuestions.slice(0, questionIndex),
      question,
      ...currentQuestions.slice(questionIndex + 1),
    ]);
  };

  onFormCancel = () => {
    this.toggleForm();
  };

  handleEditClick = questionIndex => {
    const questionToEdit = this.props.questions[questionIndex];

    if (!questionToEdit) {
      return;
    }

    this.setState({
      isFormEdit: true,
      editQuestionDetails: { ...questionToEdit, questionIndex },
    });

    this.toggleForm();
  };

  handleAddClick = () => {
    this.setState({ editQuestionDetails: null, isFormEdit: false });
    this.toggleForm();
  };

  handleQuestionsImport = e => {
    const file = e.target.files[0];

    // clearing the input value, in case the user wants to import same file again
    e.target.value = null;

    getQuestionsFromFile(file, maxQuestionsToImport)
      .then((questions = []) => {
        this.props.setQuestions([...this.props.questions, ...questions]);
      })
      .catch(error => {
        const errorMessage =
          (error && error.message) ||
          'An error occured while importing the file';

        toast.error(errorMessage);
      });
  };

  handleDelete = questionIndex => {
    this.props.openModal({
      handleClose: ({ isConfirmed }) => {
        if (!isConfirmed) {
          return;
        }

        const currentQuestions = this.props.questions;

        this.props.setQuestions([
          ...currentQuestions.slice(0, questionIndex),
          ...currentQuestions.slice(questionIndex + 1),
        ]);
      },
    });
  };

  onSortEnd = ({ oldIndex, newIndex }) => {
    const newQuestions = arrayMove(this.props.questions, oldIndex, newIndex);

    this.props.setQuestions(newQuestions);
  };

  render() {
    const { questions, parentId, isPracticeQuestion } = this.props;
    const { isShowForm, isFormEdit, editQuestionDetails } = this.state;

    return (
      <Card body>
        <QuestionFormModal
          isOpen={isShowForm}
          isEdit={isFormEdit}
          questionDetails={editQuestionDetails}
          isPracticeQuestion={isPracticeQuestion}
          handleSubmit={this.onFormSubmit}
          handleCancel={this.onFormCancel}
        />

        <div className="mb-2">
          <QuestionList
            parentId={parentId}
            questionIndexIsKey={true}
            questions={questions}
            onEdit={this.handleEditClick}
            onDelete={this.handleDelete}
            onSortEnd={this.onSortEnd}
          />
        </div>

        <Row>
          <Col xs="12" sm="6" className="mb-1">
            <Label
              className="btn btn-secondary w-100"
              htmlFor={`questionsInput-${parentId}`}
            >
              Import Questions
              <Input
                type="file"
                id={`questionsInput-${parentId}`}
                style={{ display: 'none' }}
                onChange={this.handleQuestionsImport}
              />
            </Label>
          </Col>

          <Col xs="12" sm="6" className="mb-1">
            <Button className="w-100" onClick={this.handleAddClick}>
              Add Question
            </Button>
          </Col>
        </Row>
      </Card>
    );
  }
}

export default Questions;
