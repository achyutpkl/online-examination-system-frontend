import { toast } from 'react-toastify';
import React, { PureComponent } from 'react';
import {
  Col,
  Row,
  Card,
  Form,
  Input,
  Label,
  Button,
  CardBody,
  CardHeader,
} from 'reactstrap';

import { getTrimmedIfString } from '../../../utils/stringUtils';
import {
  formValidationMessages,
  numberOfQuestionOptions,
} from '../../../views/Exams/utils/constants';

import Editor from '../../Editor/Editor';
import GotoResourceButton from '../../GotoResourceButton/GotoResourceButton';

/**
  {
    "title": "What is SI unit of time?",
    "options": { "1": "sec", "2": "min", "3": "hr","4":"other"},
    "correctAnswer": "2",
    "noOfOption": 4,
    "explanation": "",
  }
 */

const QUESTION_TYPES = ['Free', 'Premium'];
const DIFFICULTY_LEVELS = ['EASY', 'MEDIUM', 'HARD'];

class QuestionForm extends PureComponent {
  constructor(props) {
    super(props);

    let initialState = {
      options: [], // ['First Option', 'Second Option', 'Third Option']
      tempOption: '',
      explanation: '',
      questionTitle: '',
      correctOptionIndex: '',
    };

    const receivedQuestionDetails = this.props.questionDetails;

    if (receivedQuestionDetails) {
      let correctOptionIndex;

      const {
        title,
        options: optionsObject,
        correctAnswer,
        ...otherProps
      } = receivedQuestionDetails;

      const options = Object.entries(optionsObject || {}).map(
        ([key, value], index) => {
          if (String(key) === String(correctAnswer)) {
            correctOptionIndex = index;
          }

          return value;
        },
      );

      initialState = {
        ...initialState,
        options,
        correctOptionIndex,
        questionTitle: title,
        ...otherProps,
      };

      if (this.props.isPracticeQuestion) {
        initialState.type = initialState.type || QUESTION_TYPES[0];
        initialState.difficultyLevel =
          initialState.difficultyLevel || DIFFICULTY_LEVELS[0];
      }
    }

    this.state = initialState;
  }

  render() {
    return (
      <div className="question-form">
        <Card body>
          <div className="d-flex justify-content-end mb-1">
            <GotoResourceButton name="Get formula cheatsheet" />
          </div>

          {this.props.isPracticeQuestion
            ? this._renderQuestionTypeAndLevel()
            : null}
          {this._renderQuestionSection()}
          {this._renderHintSection()}
          {this._renderOptionSection()}
        </Card>

        <Button color="primary" onClick={this._onSave}>
          Save Question
        </Button>

        <Button color="warning" onClick={this._onCancel}>
          Cancel
        </Button>
      </div>
    );
  }

  // rendering question type and level card
  _renderQuestionTypeAndLevel = () => (
    <Card className="border-secondary">
      <CardBody>
        <Row>
          <Col>
            <Label for="questionType">Question Type</Label>
            <Input
              type="select"
              name="questionType"
              id="questionType"
              value={this.state.type}
              onChange={this._handleOnChangeQuestionType}
            >
              {QUESTION_TYPES &&
                QUESTION_TYPES.map(type => (
                  <option key={type} value={type}>
                    {type}
                  </option>
                ))}
            </Input>
          </Col>
          <Col>
            <Label for="difficultyLevel">Difficulty Level</Label>
            <Input
              type="select"
              id="difficultyLevel"
              name="difficultyLevel"
              value={this.state.difficultyLevel}
              onChange={this._handleOnChangeDifficultyLevel}
            >
              {DIFFICULTY_LEVELS &&
                DIFFICULTY_LEVELS.map(level => (
                  <option key={level} value={level}>
                    {level}
                  </option>
                ))}
            </Input>
          </Col>
        </Row>
      </CardBody>
    </Card>
  );

  //rendering quesetion section with card
  _renderQuestionSection = () => (
    <Card>
      <CardHeader>Question</CardHeader>
      <CardBody>
        <Row>
          <Col xs="12">
            <Editor
              value={this.state.questionTitle}
              handleOnChange={this._handleOnChangeQuestion}
              placeholder="Enter Question"
            />
          </Col>
        </Row>
      </CardBody>
    </Card>
  );

  // rendering hint section
  _renderHintSection = () => (
    <Card>
      <CardHeader>
        <span>Explanation</span>
        <span className="text-muted pl-1">/ Hints</span>
      </CardHeader>
      <CardBody>
        <Row>
          <Col xs="12">
            <Editor
              showAdvance={false}
              value={this.state.explanation}
              handleOnChange={this._handleOnChangeExplanation}
              placeholder="Enter Explanation/Hints"
            />
          </Col>
        </Row>
      </CardBody>
    </Card>
  );

  // render option section
  _renderOptionSection = () => {
    const { options } = this.state;

    return (
      <Card className="options-container">
        <CardHeader>
          <span>Options</span>
        </CardHeader>
        <CardBody>
          <Row>
            <Col xs="12">
              <div className="mb-4">
                {(options &&
                  options.length &&
                  options.map((option, index) => {
                    return (
                      <div className="rounded p-2 mb-2 bg-gray-200" key={index}>
                        <Editor
                          value={option}
                          handleOnChange={data =>
                            this._handleOptionEdit(index, data)
                          }
                          style={{ backgroundColor: 'red' }}
                        />
                        {this._renderSetCorrectAnswerButton(index)}
                        <Button
                          color="danger"
                          onClick={() => this._deleteOption(index)}
                          className="m-1"
                        >
                          Delete
                        </Button>
                      </div>
                    );
                  })) ||
                  'No options created'}
              </div>

              <Form onSubmit={this._addNewOption}>
                <Editor
                  value={this.state.tempOption}
                  placeholder="Add New Option"
                  handleOnChange={this._handleOnChangeTempOption}
                />
                <Button
                  color="primary"
                  onClick={this._addNewOption}
                  className="m-1"
                >
                  <i className="fa fa-plus-circle pr-2" />
                  Add
                </Button>
              </Form>
            </Col>
          </Row>
        </CardBody>
      </Card>
    );
  };

  _renderSetCorrectAnswerButton = index => {
    if (index === this.state.correctOptionIndex) {
      return (
        <div className="d-inline-block text-success font-weight-bold">
          Correct Answer
        </div>
      );
    }

    return (
      <Button
        color="success"
        onClick={() => this._setCorrectOptionIndex(index)}
      >
        Set Correct Answer
      </Button>
    );
  };

  // functionality methods

  _handleOnChangeQuestion = questionTitle => {
    this.setState({
      questionTitle,
    });
  };

  _handleOnChangeExplanation = explanation => {
    this.setState({ explanation });
  };

  _handleOnChangeTempOption = optionValue => {
    this.setState({ tempOption: optionValue });
  };

  _handleOnChangeQuestionType = event => {
    this.setState({ type: event.target.value });
  };

  _handleOnChangeDifficultyLevel = event => {
    this.setState({ difficultyLevel: event.target.value });
  };

  _addNewOption = e => {
    e.preventDefault();

    const newOption = this.state.tempOption.trim();

    if (newOption === '') {
      return;
    }

    this.setState({
      options: [...this.state.options, newOption],
      tempOption: '',
    });

    // TODO: The following is to battle a possible bug with react quill while clearing value
    setTimeout(() => {
      this.setState({ tempOption: '' });
    });
  };

  _handleOptionEdit = (index, newValue) => {
    this.setState({
      options: [
        ...this.state.options.slice(0, index),
        newValue,
        ...this.state.options.slice(index + 1),
      ],
    });
  };

  _setCorrectOptionIndex = correctOptionIndex => {
    this.setState({ correctOptionIndex });
  };

  _deleteOption = index => {
    this.setState({
      options: [
        ...this.state.options.slice(0, index),
        ...this.state.options.slice(index + 1),
      ],
    });

    // if deleted option is correct answer then set correct correctOptionIndex to empty
    if (this.state.correctOptionIndex === index) {
      this.setState({ correctOptionIndex: '' });
    }
  };

  _getValidationMessage = () => {
    if (this.state.questionTitle.trim() === '') {
      return formValidationMessages.noQuestionTitle;
    }

    const options = this.state.options || [];

    if (options.length !== numberOfQuestionOptions) {
      return formValidationMessages.numberOfQuestionOptions;
    }

    for (let option of options) {
      const optionValue = getTrimmedIfString(option);

      if (
        optionValue === '' ||
        optionValue === null ||
        optionValue === undefined
      ) {
        return formValidationMessages.emptyOptionValue;
      }
    }

    if (!this.state.correctOptionIndex && this.state.correctOptionIndex !== 0) {
      return formValidationMessages.noCorrectAnswer;
    }

    return '';
  };

  getOptionsInfoForSubmission = () => {
    const { options } = this.state;

    const optionsObj = {};

    options.forEach((option, index) => (optionsObj[index + 1] = option));

    return { optionsObj, noOfOption: options.length };
  };

  _getFormattedDataForSubmission = () => {
    const { optionsObj, noOfOption } = this.getOptionsInfoForSubmission();

    const {
      correctOptionIndex,
      questionTitle,
      explanation,
      tempOption, // tempOption should not be submitted
      options, // unformatted options should not be submitted
      ...otherProps
    } = this.state;

    const correctAnswer = correctOptionIndex + 1;

    return {
      title: questionTitle,
      explanation: explanation,
      options: optionsObj,
      correctAnswer,
      noOfOption,
      ...otherProps,
    };
  };

  _onSave = () => {
    const validationMessage = this._getValidationMessage();

    if (validationMessage) {
      return toast.error(validationMessage);
    }

    this.props.onSubmit(this._getFormattedDataForSubmission());
  };

  _onCancel = () => {
    this.props.onCancel();
  };
}

export default QuestionForm;
