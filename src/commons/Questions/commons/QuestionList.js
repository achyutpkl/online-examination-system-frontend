import React from 'react';
import { Label, Input, FormGroup, Button } from 'reactstrap';
import { SortableContainer, SortableElement } from 'react-sortable-hoc';

import { getQuestionValidationMessage } from '../../../views/Exams/utils/utils';

import QuestionTitle from '../../QuestionTitle/QuestionTitle';
import QuestionOption from '../../QuestionOption/QuestionOption';

const getQuestionClassName = questionProps => {
  const classes = 'question rounded mb-1';

  if (getQuestionValidationMessage(questionProps)) {
    return classes + ' invalid';
  }

  return classes;
};

const getQuestionKey = ({ questionIndexIsKey, id, index, parentId }) =>
  `${parentId}-${questionIndexIsKey ? index : id || index}`;

const QuestionItem = props => {
  const {
    question,
    index,
    questionIndexIsKey,
    onEdit,
    onDelete,
    parentId,
  } = props;

  // TODO: Try removing key field in the following and use it where loop is used
  return (
    <div
      key={getQuestionKey({ ...question, index, parentId, questionIndexIsKey })}
      className={getQuestionClassName(question)}
    >
      <div className="titleWrapper">
        <span>{index + 1}.&nbsp;</span>
        <QuestionTitle
          className="title"
          dangerouslySetInnerHTML={{ __html: question.title }}
        />
      </div>

      <div className="optionsContainer">
        {question.options &&
          Object.entries(question.options).map(
            ([optionKey, option], optionIndex) => (
              <FormGroup
                check
                inline
                key={optionIndex}
                className="option-wrapper"
              >
                <Label check>
                  <Input
                    type="radio"
                    value={option}
                    name={getQuestionKey({
                      ...question,
                      index,
                      parentId,
                      questionIndexIsKey,
                    })}
                    checked={
                      String(question.correctAnswer) === String(optionKey)
                    }
                    disabled={true}
                  />
                  <QuestionOption
                    className="option"
                    dangerouslySetInnerHTML={{ __html: option }}
                  />
                </Label>
              </FormGroup>
            ),
          )}
      </div>

      <div className="actions-container animated fadeIn">
        <Button color="success" onClick={() => onEdit(index)}>
          <i className="fa fa-edit" />
        </Button>
        <Button color="danger" onClick={() => onDelete(index)}>
          <i className="fa fa-trash" />
        </Button>
      </div>
    </div>
  );
};

const SortableQuestionItem = SortableElement(QuestionItem);

const SortableQuestionList = SortableContainer(props => {
  const { questions, ...otherProps } = props;

  return (
    <div className="questions-container">
      {questions.map((question, index) => (
        <SortableQuestionItem
          key={`item-${index}`}
          index={index}
          question={question}
          {...otherProps}
        />
      ))}
    </div>
  );
});

export default props => <SortableQuestionList distance={4} {...props} />;
