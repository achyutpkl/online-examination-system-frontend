import React from 'react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import QuestionForm from './QuestionForm';

export default ({
  isOpen,
  isEdit,
  questionDetails,
  handleSubmit,
  handleCancel,
  isPracticeQuestion,
}) => (
  <Modal isOpen={isOpen} centered={true} size="xl" backdrop="static">
    <ModalHeader toggle={handleCancel} className="question-modal-header">
      Question Form
    </ModalHeader>

    <ModalBody>
      <QuestionForm
        isEdit={isEdit}
        questionDetails={questionDetails}
        isPracticeQuestion={isPracticeQuestion}
        onSubmit={handleSubmit}
        onCancel={handleCancel}
      />
    </ModalBody>
  </Modal>
);
