import React from 'react';

export default ({ image, imageProps, ...otherProps }) => (
  <div
    className="d-inline-block my-3 bg-gray-200"
    style={{ borderRadius: '50%', padding: '16px' }}
    {...otherProps}
  >
    <img
      style={{
        display: 'inline-block',
        width: '60px',
        height: '60px',
      }}
      alt="forgot-password"
      src={image}
      {...imageProps || {}}
    />
  </div>
);
