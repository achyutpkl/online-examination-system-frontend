import React from 'react';
import { viewRoutes } from '../../utils/constants';

export default ({ name, className, ...otherProps }) => (
  <a
    href={viewRoutes.adminResources}
    target="_blank"
    rel="noopener noreferrer"
    className={`btn btn-info text-white ${className}`}
    {...otherProps}
  >
    {name}
  </a>
);
