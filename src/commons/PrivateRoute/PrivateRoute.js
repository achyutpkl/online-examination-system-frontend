import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export default props => {
  const {
    component: Component,
    isAllowed,
    redirectPath,
    componentProps = {},
    ...rest
  } = props;

  return (
    <Route
      {...rest}
      render={props =>
        isAllowed ? (
          <Component
            {...props}
            {...componentProps}
            key={props.match.params.pageid || props.match.params.path}
          />
        ) : (
          <Redirect
            to={{
              pathname: redirectPath || '/login',
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};
