import React from 'react';
import { Card, CardFooter, CardBody } from 'reactstrap';

export default ({ title, value, icon, color, footerText, handleOnClick }) => (
  <Card onClick={handleOnClick} style={{ cursor: 'pointer' }}>
    <CardBody>
      <i className={`fa fa-${icon} ${color} p-3 font-2xl mr-3 float-left`} />
      <div className={`h5 mb-0 text-${color} mt-2`}>{value}</div>
      <div className={`text-muted text-uppercase font-weight-bold font-xs`}>
        {title}
      </div>
    </CardBody>
    {footerText && (
      <CardFooter>
        <div className="font-weight-bold font-xs btn-block text-muted">
          {footerText}
          <i className="fa fa-angle-right float-right font-lg" />
        </div>
      </CardFooter>
    )}
  </Card>
);
