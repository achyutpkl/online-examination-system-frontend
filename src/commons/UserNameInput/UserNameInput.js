import React from 'react';
import { Input } from 'reactstrap';

export default ({ type, value, onChange, ...otherProps }) => (
  <Input
    type="text"
    value={value}
    onChange={e => {
      const value = e.target.value;

      if (value.charAt(value.length - 1) === ' ') {
        e.preventDefault();
        e.stopPropagation();

        return;
      }

      return onChange(e);
    }}
    {...otherProps}
  />
);
