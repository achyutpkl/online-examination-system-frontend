import React from 'react';
import { Modal, ModalBody, ModalHeader, ModalFooter, Button } from 'reactstrap';

export default class ConfirmPopup extends React.Component {
  render() {
    const {
      header,
      message,
      confirmText,
      cancelText,
      handleConfirm,
      handleCancel,
      isRequesting,
      isDisableClose,
    } = this.props;

    if (
      typeof handleConfirm === 'function' &&
      typeof handleCancel === 'function'
    ) {
      return (
        <Modal isOpen={true} backdrop="static">
          {isDisableClose ? (
            <ModalHeader>{header || ''}</ModalHeader>
          ) : (
            <ModalHeader toggle={handleCancel}>{header || ''}</ModalHeader>
          )}
          <ModalBody>
            {message || 'Are you sure?'}
            {this.props.children}
          </ModalBody>
          <ModalFooter>
            <Button disabled={isRequesting} onClick={handleConfirm}>
              {confirmText || 'Yes'}
            </Button>
            <Button disabled={isRequesting} onClick={handleCancel}>
              {cancelText || 'No'}
            </Button>
          </ModalFooter>
        </Modal>
      );
    }

    return null;
  }
}
