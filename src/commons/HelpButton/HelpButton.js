import React, { Fragment } from 'react';
import { Button, UncontrolledPopover, PopoverBody } from 'reactstrap';

export default ({ id, message, placement, ...otherProps }) => (
  <Fragment>
    <Button
      style={{
        borderRadius: '50%',
        padding: '0',
        width: '24px',
        height: '24px',
        color: 'dimgray',
      }}
      id={id}
      type="button"
      color="light"
      {...otherProps}
    >
      <i className="fa fa-question" />
    </Button>
    <UncontrolledPopover trigger="hover" placement={placement} target={id}>
      <PopoverBody>{message}</PopoverBody>
    </UncontrolledPopover>
  </Fragment>
);
