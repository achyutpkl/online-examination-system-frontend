import React, { Component, Fragment } from 'react';
import { Modal, ModalBody, ModalFooter, Button } from 'reactstrap';

export default class ConfirmModal extends Component {
  constructor() {
    super();

    this.state = this.getInitialState();
  }

  getInitialState = () => ({
    isOpen: false,
    message: '',
    cancelText: '',
    confirmText: '',
    handleClose: null,
  });

  openModal = ({ message, cancelText, confirmText, handleClose }) => {
    this.setState({
      isOpen: true,
      message,
      cancelText,
      confirmText,
      handleClose,
    });
  };

  closeModal = isConfirmed => {
    if (typeof this.state.handleClose === 'function') {
      this.state.handleClose({ isConfirmed });
    }

    this.setState(this.getInitialState());
  };

  render() {
    const { message, confirmText, cancelText, isOpen } = this.state;

    return (
      <Fragment>
        <Modal isOpen={isOpen} backdrop="static">
          <ModalBody>{message || 'Are you sure?'}</ModalBody>
          <ModalFooter>
            <Button onClick={() => this.closeModal(true)}>
              {confirmText || 'Yes'}
            </Button>
            <Button onClick={() => this.closeModal(false)}>
              {cancelText || 'No'}
            </Button>
          </ModalFooter>
        </Modal>

        {this.props.children({ openModal: this.openModal })}
      </Fragment>
    );
  }
}

export const withConfirmModal = Component => {
  return class extends React.Component {
    render() {
      return (
        <ConfirmModal>
          {confirmModalProps => (
            <Component {...this.props} {...confirmModalProps} />
          )}
        </ConfirmModal>
      );
    }
  };
};
