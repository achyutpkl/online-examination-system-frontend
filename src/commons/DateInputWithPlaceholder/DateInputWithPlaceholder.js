import { Input } from 'reactstrap';
import React, { Component } from 'react';

export default class DateInputWithPlaceholder extends Component {
  state = {
    type: 'text',
  };

  handleChange = e => {
    const dateString = e.target.value;

    if (dateString && dateString.length > 10) {
      e.preventDefault();
      e.stopPropagation();

      return;
    }

    this.props.onChange(e);
  };

  render() {
    const { type, value, placeholder, onChange, ...otherProps } = this.props;

    return (
      <Input
        {...otherProps}
        type={value ? 'date' : this.state.type}
        onFocus={() => this.setState({ type: 'date' })}
        onBlur={() => this.setState({ type: 'text' })}
        value={value}
        placeholder={placeholder}
        onChange={this.handleChange}
      />
    );
  }
}
