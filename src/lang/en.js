// Add english messages here
export const SUCCESS_DELETE_MESSAGE = 'Successfully deleted';

export const CREATE_ACCOUNT = 'Create Account';

export const SAVE = 'Save';

export const SUCCESS_REGISTER_MESSAGE = 'Registered Successfully';

export const SUCCESS_UPDATED_MESSAGE = 'Updated Successfully';

export const PASSWORD_DID_NOT_MATCH = `Password didn't match`;
