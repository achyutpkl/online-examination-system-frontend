import { userActionTypes as actionTypes } from '../actions/actionTypes';

let initialState = {
  name: '',
  email: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_USER:
      return {
        ...state,
        ...action.payload,
      };

    default:
      return state;
  }
};
