import { routerMiddleware } from 'connected-react-router';
import { compose, createStore, applyMiddleware } from 'redux';

import history from './utils/history';
import rootReducer from './reducers/index';

/**
 * Configure redux store
 *
 * @param {Object} initialState The initial state of the application
 * @returns {Object} The global store object for redux
 */
export default function configureStore(initialState = {}) {
  const middlewares = [routerMiddleware(history)];

  const enhancers = [applyMiddleware(...middlewares)];

  const composeEnhancers =
    process.env.NODE_ENV !== 'production' &&
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
      ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
      : compose;

  const store = createStore(
    rootReducer,
    initialState,
    composeEnhancers(...enhancers),
  );

  if (module.hot) {
    module.hot.accept('./reducers/index', () => {
      store.replaceReducer(require('./reducers/index').default);
    });
  }

  return store;
}
