import { isString } from 'lodash';

export const upperCaseFirstLetter = (value = '') =>
  value.charAt(0).toUpperCase() + value.slice(1);

export const upperCaseFirstLetterOnly = (value = '') =>
  value.charAt(0).toUpperCase() + value.slice(1).toLowerCase();

export const removeExtraWhiteSpace = (value = '') =>
  value.replace(/\s+/g, ' ').trim();

export const getTrimmedIfString = value =>
  isString(value) ? value.trim() : value;
