export const APP_NAME = 'OnlineExamSystem';
let serverDomain = process.env.REACT_APP_DEV_SERVER_API;
let webserverDomain;
let eSewaPaymentUrl, eSewaMerchantCode;

if (process.env.REACT_APP_ENV === 'prod') {
  serverDomain = process.env.REACT_APP_PROD_SERVER_API;
  webserverDomain = process.env.REACT_APP_PROD_WEBSERVER;
  eSewaPaymentUrl = process.env.REACT_APP_PROD_ESEWA_PAYMENT_URL;
  eSewaMerchantCode = process.env.REACT_APP_PROD_ESEWA_MERCHANT_CODE;
} else if (process.env.REACT_APP_ENV === 'uat') {
  serverDomain = process.env.REACT_APP_UAT_SERVER_API;
  webserverDomain = process.env.REACT_APP_UAT_WEBSERVER;
  eSewaPaymentUrl = process.env.REACT_APP_UAT_ESEWA_PAYMENT_URL;
  eSewaMerchantCode = process.env.REACT_APP_UAT_ESEWA_MERCHANT_CODE;
} else {
  serverDomain = process.env.REACT_APP_DEV_SERVER_API;
  webserverDomain = process.env.REACT_APP_DEV_WEBSERVER;
  eSewaPaymentUrl = process.env.REACT_APP_DEV_ESEWA_PAYMENT_URL;
  eSewaMerchantCode = process.env.REACT_APP_DEV_ESEWA_MERCHANT_CODE;
}

export const baseApiUrl = serverDomain;
export const webserverUrl = webserverDomain;

export const apiUrls = {
  user: `${baseApiUrl}/users`,
  login: `${baseApiUrl}/users/login`,
  register: `${baseApiUrl}/users/register`,
  registrations: `${baseApiUrl}/registrations`,
  forgotPassword: `${baseApiUrl}/forgot-password`,
  resetPassword: `${baseApiUrl}/reset-password`,
  addUser: `${baseApiUrl}/users/add`,
  subject: `${baseApiUrl}/subjects`,
  division: `${baseApiUrl}/divisions`,
  subDivision: `${baseApiUrl}/subdivisions`,
  refreshToken: `${baseApiUrl}/refreshToken`,
  exam: `${baseApiUrl}/exams`,
  examType: `${baseApiUrl}/examtypes`,
  examDetails: `${baseApiUrl}/exams`,
  examScore: `${baseApiUrl}/exams/:examId/scores`,
  examDetailsWithQuestions: `${baseApiUrl}/exams/:examId/details`,
  checkPracticeAnswer: `${baseApiUrl}/practiceanswers`,
  getAttemptId: `${baseApiUrl}/practicequestions/attempts?subDivisionId=`,
  nextQuestion: `${baseApiUrl}/practicequestions/attempts/:attemptId/next?page=0`,
  practiceQuestions: `${baseApiUrl}/practicequestions`,
  examReports: `${baseApiUrl}/examreports`,
  resetPracticeSession: `${baseApiUrl}/practicequestions/attempts`,
  examResult: `${baseApiUrl}/examreports/:examId`,
  examPaymentDetails: `${baseApiUrl}/exam-payment-details`,
};

export const localStorageKeys = {
  oesUser: 'oes_user',
};

export const userRole = {
  admin: 'ROLE_ADMIN',
  student: 'ROLE_STUDENT',
};

export const viewRoutes = {
  admin: '/admin',
  adminProfile: '/admin/profile',
  adminDashboard: '/admin/dashboard',
  adminExamList: '/admin/exams',
  createExam: '/admin/create-exam',
  adminExamEdit: '/admin/exams/:id/edit',
  examType: '/admin/examtype',
  adminResources: '/admin/resources',
  addQuestions: '/admin/add-question',
  subjects: '/admin/subjects',
  viewPracticeQuestions: '/admin/view-practice-questions',
  student: '/student',
  studentProfile: '/student/profile',
  studentDashboard: '/student/dashboard',
  studentExamList: '/student/exams',
  studentExamDetails: '/student/exam-details/:id',
  studentExamScores: '/student/exam/:examId/scores',
  studentExamScoreDetails: '/student/exam/:examId/scores/:scoreId',
  adminStudentExamScoreDetails: '/admin/exam/:examId/scores/:scoreId',
  studentExamPaper: '/student/exam-paper/:id',
  studentPracticeBoard: '/student/practice/:subjectId',
  studentPracticeQuestions: '/student/practice/:subjectId/subdivision/:id',
  studentProgressReport: '/student/progressreport',
  adminUser: '/admin/user',
  adminRegister: '/admin/register',
  studentPayments: '/student/payments',
  studentPaymentSuccess: '/student/payment-success',
  studentPaymentFailure: '/student/payment-failure',
};

export const maxQuestionsToImport = 1001;

export const examQuestionTemplateUrl =
  process.env.PUBLIC_URL + '/exam-question-template.csv';
export const practiceQuestionTemplateUrl =
  process.env.PUBLIC_URL + '/practice-question-template.csv';
export const formulaCheatSheetUrl =
  process.env.PUBLIC_URL + '/formula-cheatsheet.docx';

// e-sewa exports
export { eSewaPaymentUrl, eSewaMerchantCode };
