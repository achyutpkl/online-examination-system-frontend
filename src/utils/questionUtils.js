// TODO: Remove this file after confirming that no-one has used this.

export const getFormattedQuestions = (questions = []) =>
  questions.map(({ id, ...questionProps }, index) => ({
    ...questionProps,
    questionNo: index + 1,
  }));
