import Papa from 'papaparse';

import { removeExtraWhiteSpace } from './stringUtils';

export const getJsonFromFile = file => {
  return new Promise((resolve, reject) => {
    Papa.parse(file, {
      complete: result => resolve(result && result.data),
      header: true,
      error: error => reject(error),
    });
  });
};

export const getQuestionsFromFile = (file, numRowsLimit, isAppendId) => {
  return getJsonFromFile(file).then(data => {
    if (!data) {
      return [];
    }

    if (data.length > numRowsLimit) {
      throw new Error(`Only upto ${numRowsLimit} rows are allowed`);
    }

    const builtQuestions =
      data &&
      data.reduce((questions, questionItem, index) => {
        const title = questionItem.question;

        if (!title) {
          return questions;
        }

        const questionNo = questionItem.questionNo || index + 1;

        let noOfOption = 0;
        let correctAnswer;

        const builtOptions = Object.entries(questionItem).reduce(
          (options, [key, value], index) => {
            if (key.startsWith('option')) {
              const optionKey = key.replace('option', '');

              const trimmedValue = removeExtraWhiteSpace(value);
              const trimmedRightAnswer = removeExtraWhiteSpace(
                questionItem.rightAnswer,
              );

              options[optionKey] = value;

              noOfOption++;

              if (trimmedValue === trimmedRightAnswer) {
                correctAnswer = optionKey;
              }
            }

            return options;
          },
          {},
        );

        const newQuestion = {
          questionNo,
          title,
          options: builtOptions,
          noOfOption,
          correctAnswer,
          type: questionItem.type,
          difficultyLevel:
            questionItem.difficultyLevel &&
            questionItem.difficultyLevel.toUpperCase(),
          providedBy: questionItem.providedBy,
          explanation: questionItem.explanation,
        };

        if (isAppendId) {
          newQuestion.id = questionNo;
        }

        questions.push(newQuestion);

        return questions;
      }, []);

    return builtQuestions;
  });
};
