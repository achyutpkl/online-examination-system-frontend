export const editItemById = (items = [], editItem) =>
  items.map(item =>
    item.id === editItem.id ? { ...item, ...editItem } : item,
  );

export const arrayMove = (array = [], from, to) => {
  const newArray = array.slice();

  // remove item from previous position
  const movedItem = newArray.splice(from, 1)[0];

  // insert item into new position
  newArray.splice(to, 0, movedItem);

  return newArray;
};
