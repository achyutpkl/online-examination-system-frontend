import axios from 'axios';
import { toast } from 'react-toastify';

import { getItem } from './localStorage';
import { localStorageKeys as keys } from './constants';

const axiosInstance = axios.create();

axiosInstance.interceptors.request.use(
  config => {
    const { accessToken } = getItem(keys.oesUser) || {};

    if (accessToken) {
      config.headers.Authorization = `Bearer ${accessToken}`;
    }

    return config;
  },
  error => Promise.reject(error),
);

/**
 * Send GET request.
 *
 * @param {string} url Request url
 * @returns {Promise} promise
 */
const get = url => {
  return axiosInstance.get(url);
};

/**
 * Send POST request.
 *
 * @param {String} url Rrequest url
 * @param {Object} body Request payload
 * @param {Object} config Axios Request Config
 * @returns {Promise} Promise
 */
const post = (url, body = {}, config) => {
  return axiosInstance.post(url, body, config);
};

/**
 * Send PUT request.
 *
 * @param {String} url Rrequest url
 * @param {Object} body Request payload
 * @returns {Promise} Promise
 */
const put = (url, body = {}) => {
  return axiosInstance.put(url, body);
};

/**
 * Send PATCH request.
 *
 * @param {String} url Rrequest url
 * @param {Object} body Request payload
 * @returns {Promise} Promise
 */
const patch = (url, body = {}) => {
  return axiosInstance.patch(url, body);
};

/**
 * Send DELETE request.
 *
 * @param {String} url Rrequest url
 * @param {Object} body Request payload
 * @returns {Promise} Promise
 */
const del = (url, body = {}) => {
  return axiosInstance.delete(url, body);
};

const getHttpErrorMessage = (error, defaultMessage = 'An error occured') => {
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    const responseData = error.response.data;

    return (
      (responseData &&
        (responseData.message ||
          (responseData.apiError && responseData.apiError.message))) ||
      defaultMessage
    );
  }

  if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js

    // TODO: Confirm best way to handle error in this case
    return defaultMessage;
  }

  // Something happened in setting up the request that triggered an Error
  return error.message || defaultMessage;
};

const showApiErrorMessage = error => {
  if (error.blockErrorToaser) {
    /* This special case was introduced to deal with the fact that in many places where a network request is made,
    error will occur when interceptor responds with empty response. It is not due to the lack of safety accessor
    with "&" or lack of fallback response, but rather due to incorrect fallback response.
    Here's a sample of wrong fallback response:
    export const getSubjects = async (pageSize = DEFAULT_PAGE_SIZE) => {
      let subjects = await get(`${apiUrls.subject}?size=${pageSize}`);
  
      return (subjects && subjects.data) || [];
    }; 

    The corect fallback should have been "return (subjects && subjects.data) || { data: [] }", which is used here:
    fetchSubjects = async () => {
      this.setState({ isLoadingSubjects: true });

      let { data } = (await getSubjects().catch(showApiErrorMessage)) || {
        data: [],
      };
      .............
    }

    But because there is already wrong fallback returned from getSubjects, the data in fetchSubjects will be undefined.
    And data.map used a little further down in fetchSubjects will throw error.
    The above is one of many incorrect fallbacks that I have seen, and there may be more. And so for now, I'm
    not going to bother correcting every single incorrect fallback by searching for them.

    Now by default throwing error in axios is handled with showApiErrorMessage. But in some cases we might not want to
    show error like when token expire. We would want to just redirect user to the login page. Or we could show appropriate
    message like token expired from backend server which is not implemented at the time. Currently the backend responds with
    "Wrong email or password" even when token is invalid(or token expires).
    
    (You might want to see response interceptor used in setAxiosInterceptor of App.js, if you don't understand
    the parts about interceptor below)
    
    So I wanted to not throw error from response interceptor and only direct user to login page. Return some fallback
    data from interceptor is not feasible as fallback data is different for different requests. And returning null
    or undefined will result in app crashing.
    
    What seems consistent at the moment is that fallback are handled properly when a service using axios throws an
    error(Although even this could be wrong somewhere, though I've not encountered such a problem so far).
    With this, I've decided to throw error from axios's request interceptor along with calling handleLogout to
    direct user to login page, and I'll add blockErrorToaser to error before throwing(or rejecting Promise) with
    this error.

    I'm in no way happy with this workaround and I'm not a 100% sure that this will always work. But this is the best
    workaround I could think of at the moment.
    */

    return;
  }

  const errorMessage = getHttpErrorMessage(error);

  toast.error(errorMessage);

  return;
};

const HTTP_STATUS = {
  OK: 200,
};

export {
  del,
  get,
  put,
  post,
  patch,
  HTTP_STATUS,
  axiosInstance,
  getHttpErrorMessage,
  showApiErrorMessage,
};
