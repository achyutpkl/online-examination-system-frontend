import isBefore from 'date-fns/is_before';
import isAfter from 'date-fns/is_after';
import differenceInSeconds from 'date-fns/difference_in_seconds';
import differenceInMilliseconds from 'date-fns/difference_in_milliseconds';
import moment from 'moment-timezone';

import Axios from 'axios';

export const isValidDate = dateValue => !Number.isNaN(Date.parse(dateValue));

const withDateValidation = callback => dateValue => {
  if (!isValidDate(dateValue)) {
    return null;
  }

  return callback(dateValue);
};

export const getFormattedDate = withDateValidation(dateValue => {
  const date = new Date(dateValue);

  return date.toLocaleDateString(undefined, {
    hour12: true,
    year: 'numeric',
    month: 'long',
    day: '2-digit',
  });
});

export const getFormattedTime = withDateValidation(dateValue => {
  const date = new Date(dateValue);

  return date.toLocaleTimeString(undefined, {
    hour12: true,
    hour: '2-digit',
    minute: '2-digit',
  });
});

export const getFormattedDateTime = withDateValidation(dateValue => {
  const date = new Date(dateValue);

  return date.toLocaleString(undefined, {
    hour12: true,
    year: 'numeric',
    month: 'long',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
  });
});

export const sortListByDate = (list = [], dateField, latestFirst = 1) =>
  list.sort(
    (a, b) =>
      (new Date(b && b[dateField]) - new Date(a && a[dateField])) * latestFirst,
  );

export const getDateObjectOrNull = dateValue => {
  const dateInMilliSeconds = Date.parse(dateValue);

  if (Number.isNaN(dateInMilliSeconds)) {
    return null;
  }

  return new Date(dateInMilliSeconds);
};

export const isBetween = (date, startDate, endDate) => {
  return isAfter(date, startDate) && isBefore(date, endDate);
};

export const getDifferenceInMilliseconds = (startDate, endDate) => {
  return differenceInMilliseconds(startDate, endDate);
};

export const getDifferenceInSeconds = (startDate, endDate) => {
  return differenceInSeconds(startDate, endDate);
};

export const getEndOfDay = () => {
  const date = new Date();

  date.setHours(23, 59, 59, 999);

  return date;
};

export const getStartOfDay = () => {
  const date = new Date();

  date.setHours(0, 0, 0, 0);

  return date;
};

export const isToday = withDateValidation(dateValue => {
  const date = new Date(dateValue);

  const today = new Date();

  return today.toDateString() === date.toDateString();
});

export const isPastDateTime = dateValue => {
  if (!dateValue && dateValue !== 0) {
    return false;
  }

  const currentDate = moment.tz(new Date(), 'Asia/Kathmandu');

  return currentDate > new Date(dateValue);
};

export const getCurrentDate = async () => {
  let currentDate;
  try {
    currentDate = await Axios.get(
      'http://worldtimeapi.org/api/timezone/Asia/Kathmandu',
    );

    return currentDate.data.utc_datetime;
  } catch (err) {
    return new Date().toISOString();
  }
};
