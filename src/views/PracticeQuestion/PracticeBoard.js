import React, { PureComponent } from 'react';
import {
  Row,
  Col,
  Card,
  Alert,
  CardBody,
  CardHeader,
  TabContent,
  TabPane,
  ListGroup,
  ListGroupItem,
} from 'reactstrap';
import { isEmpty, first } from 'lodash';

import './style.css';
import SubDivisionRow from './commons/SubDivisionRow';
import { getDivisions, getSubDivisions } from '../../services/SubjectService';

const dataset = [
  78,
  81,
  80,
  45,
  34,
  12,
  40,
  75,
  34,
  89,
  32,
  68,
  54,
  72,
  18,
  98,
];
class PracticeBoard extends PureComponent {
  constructor() {
    super();

    this.state = {
      subjectId: '',
      activeTab: 0,
      divisionId: '',
      subDivisionId: '',
    };
  }

  componentDidMount() {
    const { subjectId } = (this.props.match && this.props.match.params) || {};

    this.setState({ subjectId });

    this._fetchInitialData(subjectId);
  }

  componentWillReceiveProps(props) {
    const { subjectId } = (props.match && props.match.params) || {};

    if (this.state.subjectId === subjectId) {
      return;
    }

    this.setState({ subjectId });

    this._fetchInitialData(subjectId);
  }

  render() {
    const { divisions, subDivisions, subjectId } = this.state;

    if (divisions && !divisions.length) {
      return <Alert>Sorry, no topics has been added so far.</Alert>;
    }

    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <strong>Practice Board</strong>
              </CardHeader>
              <CardBody className="smallCardBody">
                <Row>
                  <Col xs="12" sm="4">
                    <ListGroup id="list-tab" role="tablist">
                      {divisions &&
                        divisions.map((division, index) => (
                          <ListGroupItem
                            key={division.id}
                            onClick={() =>
                              this._fetchDivisionsAndData(division.id, index)
                            }
                            action
                            active={this.state.activeTab === index}
                          >
                            {division.title}
                          </ListGroupItem>
                        ))}
                    </ListGroup>
                  </Col>

                  <Col xs="12" sm="8">
                    <TabContent>
                      <TabPane>
                        {subDivisions &&
                          subDivisions.map(subDivision => (
                            <SubDivisionRow
                              subjectId={subjectId}
                              dataset={dataset}
                              key={subDivision.id}
                              subDivision={subDivision}
                            />
                          ))}
                      </TabPane>
                    </TabContent>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }

  _fetchDivisionsAndData = async (divisionId, tab) => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
        divisionId,
      });
      let { data } = await getSubDivisions(divisionId);
      this.setState({ subDivisions: data });
    }
  };

  _fetchInitialData = async subjectId => {
    // TODO: Handle api call errors for every api calls in this file
    let { data: divisions } = await getDivisions(subjectId);

    if (isEmpty(divisions)) {
      return this.setState({ divisions: [], subDivisions: [] });
    }

    const firstDivision = first(divisions);

    let { data: subDivisions } = (await getSubDivisions(firstDivision.id)) || {
      data: [],
    };

    this.setState({ divisions, subDivisions, divisionId: firstDivision.id });
  };
}

export default PracticeBoard;
