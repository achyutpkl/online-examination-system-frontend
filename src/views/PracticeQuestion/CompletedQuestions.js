import { CustomInput, Card, CardBody, Button, Alert } from 'reactstrap';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

import './style.css';

import QuestionTitle from '../../commons/QuestionTitle/QuestionTitle';
import QuestionOption from '../../commons/QuestionOption/QuestionOption';

class CompletedQuestions extends PureComponent {
  constructor() {
    super();

    this.state = {
      currentQuestionIndex: -1,
    };
  }

  componentDidMount() {
    if (this.props.questions && this.props.questions.length) {
      this.setState({ currentQuestionIndex: this.props.questions.length - 1 });
    }
  }

  render() {
    const { questions, subDivisionTitle } = this.props;
    const { currentQuestionIndex } = this.state;

    if (currentQuestionIndex === -1) {
      return <div>There are no questions</div>;
    }

    const currentQuestion = questions[currentQuestionIndex];

    return (
      <div className="animated fadeIn practicContainer">
        <Card>
          <CardBody>
            <strong>{subDivisionTitle}</strong>

            <div className="practiceQuestionWrp">
              <div className="d-flex">
                <span className="mr-1">{currentQuestionIndex + 1}.</span>
                <QuestionTitle
                  dangerouslySetInnerHTML={{
                    __html: currentQuestion.title,
                  }}
                />
              </div>

              <div className="optionWrp" />
              {currentQuestion.options &&
                Object.keys(currentQuestion.options).map(key => (
                  <div key={key}>
                    <CustomInput
                      disabled={true}
                      className="practiceQuestionOption"
                      type="radio"
                      id={key}
                      name={currentQuestion.id}
                      label={
                        <QuestionOption
                          dangerouslySetInnerHTML={{
                            __html: currentQuestion.options[key],
                          }}
                        />
                      }
                      value={key}
                      checked={currentQuestion.studentAnswer === key}
                    />
                    {this._renderCheckAnswer(key)}
                  </div>
                ))}

              {this._renderViewBeforeAfterSubmitting()}
            </div>
          </CardBody>
        </Card>
        <a
          style={{ textDecoration: 'none' }}
          href="mailto:kiran.regmi@gmail.com"
        >
          <span className="reportQuestion">Report this question</span>
        </a>
      </div>
    );
  }

  _renderViewBeforeAfterSubmitting = () => (
    <div>
      <Alert color="warning" className="explanationWrp">
        <div
          className="no-child-paragraph-margin"
          dangerouslySetInnerHTML={{
            __html:
              this.props.questions[this.state.currentQuestionIndex]
                .explanation ||
              'Sorry, there is no explanation available for this question',
          }}
        />
      </Alert>

      <div className="prevNextBtnWrp">
        <Button
          color="success"
          className="mr-1"
          disabled={this.state.currentQuestionIndex === 0}
          onClick={() => this._previousQuestion()}
        >
          Previous Question
        </Button>

        <Button
          color="success"
          disabled={
            this.state.currentQuestionIndex + 1 === this.props.questions.length
          }
          onClick={() => this._nextQuestion()}
        >
          Next Question
        </Button>
      </div>
    </div>
  );

  _renderCheckAnswer = key => {
    if (
      this.props.questions[this.state.currentQuestionIndex].correctAnswer ===
      key
    ) {
      return (
        <i className="answerCheck correct fa fa-check" aria-hidden="true" />
      );
    }

    if (
      this.props.questions[this.state.currentQuestionIndex].studentAnswer ===
      key
    ) {
      return <i className="answerCheck wrong fa fa-times" aria-hidden="true" />;
    }
  };

  _nextQuestion = () => {
    this.setState({
      currentQuestionIndex: this.state.currentQuestionIndex + 1,
    });
  };

  _previousQuestion = () => {
    this.setState({
      currentQuestionIndex: this.state.currentQuestionIndex - 1,
    });
  };
}

CompletedQuestions.propTypes = {
  questions: PropTypes.array.isRequired,
};

export default CompletedQuestions;
