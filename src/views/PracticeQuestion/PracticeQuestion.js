import {
  CustomInput,
  Card,
  CardBody,
  Col,
  Row,
  Button,
  Alert,
} from 'reactstrap';
import React, { Component } from 'react';

import { apiUrls } from '../../utils/constants';
import broswerHistory from '../../utils/history';
import { getDifferenceInSeconds } from '../../utils/dateUtils';
import { get, post, patch, showApiErrorMessage } from '../../utils/httpUtil';

import CompletedQuestions from './CompletedQuestions';
import CompletedSessionView from './commons/CompletedSessionView';
import PracticeQuestionFacts from './commons/PracticeQuestionFacts';
import { withConfirmModal } from '../../commons/ConfirmModal/ConfirmModal';

import './style.css';

import QuestionTitle from '../../commons/QuestionTitle/QuestionTitle';
import QuestionOption from '../../commons/QuestionOption/QuestionOption';

class PracticeQuestion extends Component {
  constructor() {
    super();

    this.state = {
      result: {},
      attemptId: '',
      studentAnswer: '',
      subDivisionId: '',
      subDivisionTitle: '',
      nextQuestionUrl: '',
      sessionProgress: [],
      currentQuestion: {},
      currentAttemptAllData: {},
      studentAnswerStartTime: '',

      isLoading: true,
      isSubmitted: false,
      isSessionCompleted: false,
      showPreviousQuestions: false,
    };
  }

  componentDidMount() {
    const { id: subDivisionId } =
      (this.props.match && this.props.match.params) || {};

    this._getAttemptId(subDivisionId);
    this._getSubDivisionProps(subDivisionId);
  }

  render() {
    const {
      isLoading,
      isSubmitted,
      studentAnswer,
      currentQuestion,
      sessionProgress,
      isSessionCompleted,
      showPreviousQuestions,
      currentAttemptAllData,
    } = this.state;

    if (isLoading) {
      return <p>Loading...</p>;
    }

    if (showPreviousQuestions) {
      return this._renderPreviousQuestions();
    }

    if (isSessionCompleted) {
      return (
        <CompletedSessionView
          resetSession={this._resetSession}
          data={sessionProgress}
          totalQuestions={
            currentAttemptAllData.practiceSessionInfo.totalQuestions
          }
          totalAttemptedQuestions={
            currentAttemptAllData.practiceSessionInfo.completedQuestions
          }
          showPreviousQuestions={() =>
            this.setState({ showPreviousQuestions: true })
          }
        />
      );
    }

    const questionIndex =
      (currentAttemptAllData.previousQuestionAnswerDetails &&
        currentAttemptAllData.previousQuestionAnswerDetails.length) ||
      0;

    return (
      <div className="animated fadeIn practicContainer">
        <Row>
          <Col
            xs="12"
            sm="8"
            className={isSubmitted ? 'makeSide' : 'center-asdf'}
          >
            <Card>
              <CardBody>
                <strong>{currentQuestion.subDivision.title}</strong>

                <div className="practiceQuestionWrp">
                  <div className="d-flex">
                    <span className="mr-1">{questionIndex + 1}.</span>
                    <QuestionTitle
                      dangerouslySetInnerHTML={{
                        __html: currentQuestion.title,
                      }}
                    />
                  </div>

                  <div className="optionWrp" />
                  {currentQuestion.options &&
                    Object.keys(currentQuestion.options).map(key => (
                      <div key={key}>
                        <CustomInput
                          disabled={isSubmitted}
                          className="option"
                          type="radio"
                          id={key}
                          name={currentQuestion.id}
                          label={
                            <QuestionOption
                              dangerouslySetInnerHTML={{
                                __html: currentQuestion.options[key],
                              }}
                            />
                          }
                          value={key}
                          checked={studentAnswer === key}
                          onChange={data =>
                            this.setState({ studentAnswer: data.target.value })
                          }
                        />
                        {this.state.isSubmitted && this._renderCheckAnswer(key)}
                      </div>
                    ))}

                  {this._renderViewBeforeAfterSubmitting()}
                </div>
              </CardBody>
            </Card>
            <a
              style={{ textDecoration: 'none' }}
              href="mailto:kiran.regmi@gmail.com"
            >
              <span className="reportQuestion">Report this question</span>
            </a>
          </Col>

          {isSubmitted && (
            <Col xs="12" sm="4">
              <PracticeQuestionFacts
                resetSession={this._resetSession}
                data={sessionProgress}
                totalQuestions={
                  currentAttemptAllData.practiceSessionInfo.totalQuestions
                }
                totalAttemptedQuestions={
                  currentAttemptAllData.practiceSessionInfo.completedQuestions
                }
              />
            </Col>
          )}
        </Row>
      </div>
    );
  }

  _renderPreviousQuestions = () => {
    const { sessionProgress, currentAttemptAllData } = this.state;

    return (
      <Row>
        <Col xs="12" sm="8" className="makeSide">
          <div style={{ marginBottom: '1.5em' }}>
            <Button
              color="primary"
              onClick={() => this.setState({ showPreviousQuestions: false })}
            >
              Back to previous view
            </Button>
          </div>

          <CompletedQuestions
            questions={
              this.state.currentAttemptAllData.previousQuestionAnswerDetails
            }
            subDivisionTitle={this.state.subDivisionTitle}
          />
        </Col>

        <Col xs="12" sm="4">
          <PracticeQuestionFacts
            resetSession={this._resetSession}
            data={sessionProgress}
            totalQuestions={
              currentAttemptAllData.practiceSessionInfo.totalQuestions
            }
            totalAttemptedQuestions={
              currentAttemptAllData.practiceSessionInfo.completedQuestions
            }
          />
        </Col>
      </Row>
    );
  };

  _renderViewBeforeAfterSubmitting = () => {
    return this.state.isSubmitted ? (
      <div>
        <Alert color="warning" className="explanationWrp">
          <div
            className="no-child-paragraph-margin"
            dangerouslySetInnerHTML={{
              __html:
                this.state.result.explanation ||
                'Sorry, there is no explanation available for this question',
            }}
          />
        </Alert>

        <div className="prevNextBtnWrp">
          <Button
            className="mr-1"
            color="success"
            disabled={
              this.state.currentAttemptAllData.previousQuestionAnswerDetails
                .length === 0
            }
            onClick={() => this.setState({ showPreviousQuestions: true })}
          >
            See Previous Questions
          </Button>

          {this.state.currentAttemptAllData.practiceSessionInfo
            .completedQuestions ===
          this.state.currentAttemptAllData.practiceSessionInfo
            .totalQuestions ? (
            <Button color="primary" onClick={() => this._nextQuestion()}>
              Finish
            </Button>
          ) : (
            <Button color="success" onClick={() => this._nextQuestion()}>
              Next Question
            </Button>
          )}
        </div>
      </div>
    ) : (
      <div className="checkAnswerBtnWrp">
        <Button
          color="success"
          onClick={this._checkAnswer}
          disabled={!this.state.studentAnswer || this.state.isLoading}
        >
          Check Answer
        </Button>
      </div>
    );
  };

  _renderCheckAnswer = key => {
    if (this.state.result.correctAnswer === key) {
      return (
        <i className="answerCheck correct fa fa-check" aria-hidden="true" />
      );
    }

    if (this.state.result.studentAnswer === key) {
      return <i className="answerCheck wrong fa fa-times" aria-hidden="true" />;
    }
  };

  _getAttemptId = async subDivisionId => {
    this.setState({ isLoading: true });

    // TODO handle error even authorization
    let { data } = await get(`${apiUrls.getAttemptId}${subDivisionId}`);

    let nextQuestionUrl = apiUrls.nextQuestion.replace(
      ':attemptId',
      data.attemptId,
    );

    this.setState(
      {
        attemptId: data.attemptId,
        subDivisionId,
        nextQuestionUrl,
      },
      () => {
        this._nextQuestion();
      },
    );
  };

  _getSubDivisionProps = async subDivisionId => {
    let { data } =
      (await get(`${apiUrls.subDivision}/${subDivisionId}`).catch(
        showApiErrorMessage,
      )) || {};

    this.setState({ subDivisionTitle: (data && data.title) || '' });
  };

  _nextQuestion = async () => {
    this.setState({ isLoading: true, result: null });

    const { data } = await get(this.state.nextQuestionUrl);
    const currentQuestion = data.question;

    if (
      !currentQuestion ||
      data.practiceSessionInfo.completedQuestions ===
        data.practiceSessionInfo.totalQuestions
    ) {
      this.setState({ isSessionCompleted: true });
    }

    const sessionProgress = [
      data.practiceSessionInfo.totalCorrect,
      data.practiceSessionInfo.completedQuestions -
        data.practiceSessionInfo.totalCorrect,
    ];
    this.setState({
      isSubmitted: false,
      studentAnswer: '',
      currentQuestion,
      sessionProgress,
      currentAttemptAllData: data,
      studentAnswerStartTime: new Date(),
      isLoading: false,
    });
  };

  _checkAnswer = async () => {
    this.setState({ isLoading: true });

    const timeTake = getDifferenceInSeconds(
      new Date(),
      this.state.studentAnswerStartTime,
    );

    const answer = {
      timeTake,
      practiceQuestionId: this.state.currentQuestion.id,
      attemptId: this.state.currentAttemptAllData.attemptId,
      studentAnswer: this.state.studentAnswer,
    };

    let result = await post(apiUrls.checkPracticeAnswer, answer);
    let sessionProgress = [
      result.data.practiceSessionInfo.totalCorrect,
      result.data.practiceSessionInfo.completedQuestions -
        result.data.practiceSessionInfo.totalCorrect,
    ];

    let currentAttemptAllData = { ...this.state.currentAttemptAllData };
    currentAttemptAllData['practiceSessionInfo'] =
      result.data.practiceSessionInfo;

    this.setState({
      isLoading: false,
      isSubmitted: true,
      sessionProgress,
      currentAttemptAllData,
      result: result.data.answerDetail,
    });
  };

  _resetSession = () => {
    this.props.openModal({
      handleClose: ({ isConfirmed }) => {
        if (!isConfirmed) {
          return;
        }

        patch(`${apiUrls.resetPracticeSession}/${this.state.attemptId}`, {
          active: false,
        }).then(data => {
          broswerHistory.go(0);
        });
      },
    });
  };
}

export default withConfirmModal(PracticeQuestion);
