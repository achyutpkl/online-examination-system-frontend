import React from 'react';
import { Button } from 'reactstrap';
import { Row, Col } from 'reactstrap';

import broswerHistory from '../../../utils/history';

const SubDivisionRow = props => {
  return (
    <div className="subDivisionWrp">
      <Row className="subdivisionRow">
        <Col xs="12" className="subDivisionTitle h3">
          {props.subDivision.title}
        </Col>
      </Row>

      <Row className="practiceQuestionRow">
        <Col xs="6" className="noOfQuestion">
          {props.subDivision.noOfQuestions} Questions
        </Col>
        <Col xs="6" className="practiceBtnWrp">
          <Button
            disabled={!props.subDivision.noOfQuestions}
            color="success"
            onClick={() =>
              broswerHistory.push(
                `/student/practice/${props.subjectId}/subdivision/${
                  props.subDivision.id
                }`,
              )
            }
          >
            Practice Now
          </Button>
        </Col>
      </Row>
    </div>
  );
};

export default SubDivisionRow;
