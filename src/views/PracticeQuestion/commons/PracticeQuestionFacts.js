import React from 'react';
import PracticeSessionProgress from './PracticeSessionProgress';
import { Button, Card, CardBody, CardHeader, Table } from 'reactstrap';

const PracticeQuestionFacts = ({
  data,
  resetSession,
  totalQuestions,
  totalAttemptedQuestions,
}) => {
  return (
    <div className="factsWrp">
      <Card>
        <CardHeader>Facts about this question</CardHeader>
        <CardBody>
          <Table>
            <tbody>
              <tr>
                <td>Difficulty level </td>
                <td>Easy</td>
              </tr>
              <tr>
                <td>No. of students who gave correct anwers </td>
                <td> 23</td>
              </tr>
              <tr>
                <td>No. of students who gave wrong answers </td>
                <td> 23</td>
              </tr>
              <tr>
                <td>Average time to solve this question </td>
                <td> 2 minutes</td>
              </tr>
            </tbody>
          </Table>
        </CardBody>
      </Card>

      <PracticeSessionProgress
        data={data}
        totalAttemptedQuestions={totalAttemptedQuestions}
        totalQuestions={totalQuestions}
      />
      {/* <Card>
        <CardHeader>Session Progress</CardHeader>
        <CardBody>
          <Pie data={piedata} />
          <div style={{ textAlign: 'center' }}>
            <div className="h3">{totalAttemptedQuestions}</div>
            <div>out {totalQuestions} questions</div>
            <Progress
              color="primary"
              value={(totalAttemptedQuestions / totalQuestions) * 100}
            />
          </div>
        </CardBody>
      </Card> */}
      <Button color="danger" style={{ float: 'right' }} onClick={resetSession}>
        Reset this session
      </Button>
    </div>
  );
};

export default PracticeQuestionFacts;
