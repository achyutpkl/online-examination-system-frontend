import React from 'react';
import { Pie } from 'react-chartjs-2';
import { Progress, Card, CardBody, CardHeader } from 'reactstrap';

const initial = {
  labels: ['Correct Answers', 'Wrong Answers'],
  datasets: [
    {
      data: [21, 12],
      backgroundColor: ['#4dbd74', '#FF6384'],
      hoverBackgroundColor: ['#4dbd74', '#FF6384'],
    },
  ],
};

const PracticeSessionProgress = ({
  data,
  totalQuestions,
  totalAttemptedQuestions,
}) => {
  let piedata = { ...initial };
  piedata.datasets[0].data = data;

  return (
    <Card>
      <CardHeader>Session Progress</CardHeader>
      <CardBody>
        <Pie data={piedata} />
        <div style={{ textAlign: 'center', marginTop: '2.5em' }}>
          <div>
            {totalAttemptedQuestions} out {totalQuestions} questions completed
          </div>
          <Progress
            color="primary"
            value={(totalAttemptedQuestions / totalQuestions) * 100}
          />
        </div>
      </CardBody>
    </Card>
  );
};

export default PracticeSessionProgress;
