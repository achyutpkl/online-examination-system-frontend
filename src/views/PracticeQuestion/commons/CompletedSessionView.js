import React from 'react';
import { Row, Col, Card, CardBody, Button } from 'reactstrap';

import PracticeSessionProgress from './PracticeSessionProgress';

const CompletedSessionView = ({
  data,
  resetSession,
  totalQuestions,
  totalAttemptedQuestions,
  showPreviousQuestions,
}) => {
  return (
    <Row style={{ margin: '0 auto' }}>
      <Col xs="12" md="6">
        <Card>
          <CardBody>
            <Row className="sessionCompleteWrp mb-2">
              <Col sm="12">
                <h2 className="display-5">
                  Congratulations!
                  <p className="lead mb-0">
                    You just completed practicing all questions.
                  </p>
                </h2>
              </Col>
              <Col sm="12" className="sessionRestartBtnWrp">
                <Button color="danger" onClick={resetSession}>
                  Start Again
                </Button>
                <em>You will start from begining</em>
              </Col>
            </Row>
            <Button
              style={{ width: '100%' }}
              color="primary"
              onClick={showPreviousQuestions}
            >
              View answers
            </Button>
          </CardBody>
        </Card>
      </Col>

      <Col xs="12" md="6">
        <PracticeSessionProgress
          data={data}
          totalAttemptedQuestions={totalAttemptedQuestions}
          totalQuestions={totalQuestions}
        />
      </Col>
    </Row>
  );
};

export default CompletedSessionView;
