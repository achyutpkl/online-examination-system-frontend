import { apiUrls } from '../../utils/constants';
import { get, del, post } from '../../utils/httpUtil';

export const getAllUser = async (role, page, size, searchValue) => {
  const searchParam = searchValue
    ? `roles:${role},name:'${searchValue}'`
    : `roles:${role}`;
  let users = await get(
    `${apiUrls.user}/?search=${searchParam}&page=${page}&size=${size}`,
  );

  return users && users.data;
};

export const deleteUser = async id => {
  let response = await del(`${apiUrls.user}/${id}`);

  return (response && response.data) || {};
};

export const postStudentRegistrations = async registrations => {
  let response = await post(apiUrls.registrations, registrations);

  return (response && response.data) || {};
};
