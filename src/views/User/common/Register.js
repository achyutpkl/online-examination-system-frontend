import {
  CardBody,
  Container,
  Row,
  Col,
  Card,
  CardFooter,
  Button,
} from 'reactstrap';
import React from 'react';
import { toast } from 'react-toastify';
import { Redirect } from 'react-router-dom';

import {
  SUCCESS_UPDATED_MESSAGE,
  SUCCESS_REGISTER_MESSAGE,
  PASSWORD_DID_NOT_MATCH,
} from '../../../lang/en';
import RegisterForm from '../../Register/common/RegisterForm';
import { getHttpErrorMessage } from '../../../utils/httpUtil';
import { editUser, addUser } from '../../Register/RegisterService';

class Register extends React.Component {
  state = {
    registerSuccessful: false,
  };

  // eslint-disable-next-line complexity
  handleSubmit = formData => {
    const {
      dob,
      role,
      address,
      lastName,
      firstName,
      mobileNumber,
      middleName,
      userName,
      email,
      password,
      collegeName,
      repeatPassword,
      registrationNumber,
    } = formData;

    const { data } = this.props.location.state || {};

    if (mobileNumber.trim() && mobileNumber.trim().length !== 10) {
      toast.error('Mobile number must be of length 10', {
        position: toast.POSITION.BOTTOM_CENTER,
      });
    } else if (
      address.trim() &&
      lastName.trim() &&
      firstName.trim() &&
      dob &&
      collegeName.trim()
    ) {
      const param = {
        email: email.trim(),
        dateOfBirth: dob.trim(),
        address: address.trim(),
        lastName: lastName.trim(),
        userName: userName.trim(),
        firstName: firstName.trim(),
        middleName: middleName.trim(),
        mobileNumber: mobileNumber.trim(),
        registrationNumber: registrationNumber.trim(),
        collegeName: collegeName && collegeName.trim(),
        roles: [role],
      };

      if (!Boolean(data)) {
        if (password !== repeatPassword) {
          toast.error(PASSWORD_DID_NOT_MATCH, {
            position: toast.POSITION.BOTTOM_CENTER,
          });

          return;
        }
        param['password'] = password.trim();
      }
      const result = this.submitForm(param);
      result
        .then(res => {
          setTimeout(() => {
            this.setState({
              registerSuccessful: true,
            });
          }, 500);

          toast.success(
            data ? SUCCESS_UPDATED_MESSAGE : SUCCESS_REGISTER_MESSAGE,
            {
              position: toast.POSITION.BOTTOM_CENTER,
            },
          );
        })
        .catch(err => {
          toast.error(getHttpErrorMessage(err), {
            position: toast.POSITION.BOTTOM_CENTER,
          });
        });
    }
  };

  submitForm = param => {
    const { data } = this.props.location.state || {};

    return Boolean(data) ? editUser(data.id, param) : addUser(param);
  };

  render() {
    const { registerSuccessful } = this.state;
    const { data } = this.props.location.state || {};
    const redirect = registerSuccessful && <Redirect to="/admin/user" />;
    const isEdit = Boolean(data);
    if (registerSuccessful) {
      return redirect;
    }

    return (
      <div className="flex-row align-items-center d-flex min-vh-100">
        <Container>
          <Row className="justify-content-center ">
            <Col md="9" lg="7" xl="6">
              <Card className="mx-4 my-2">
                <CardBody className="p-4 ">
                  <RegisterForm
                    data={data}
                    isAdmin={true}
                    isEdit={isEdit}
                    handleSubmit={this.handleSubmit}
                  />
                </CardBody>
                {isEdit && (
                  <CardFooter className="p-1">
                    <Row>
                      <Col xs="12" className="text-center">
                        <Button
                          color="link"
                          onClick={e => this.props.history.goBack()}
                        >
                          <span>Cancel</span>
                        </Button>
                      </Col>
                    </Row>
                  </CardFooter>
                )}
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Register;
