import React from 'react';
import ReactTable from 'react-table';

import { Button, Input, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import ConfirmPopup from '../../../commons/ConfirmPopup/ConfirmPopup';
import {
  NextPageButton,
  PreviousPageButton,
} from '../../Question/commons/PaginationButton';
import { getItem } from '../../../utils/localStorage';
import { getAllUser, deleteUser } from '../UserService';
import { getFormattedDate } from '../../../utils/dateUtils';
import { showApiErrorMessage } from '../../../utils/httpUtil';

import '../style.css';
import CBReactTablePagination from '../../../commons/CustomReactTablePagination/CBReactTablePagination';

class UserTable extends React.Component {
  state = {
    deleteId: null,
    isDeleting: false,
    paginationInfo: {
      pages: 1,
      page: 0,
      pageSize: 50,
    },
    users: [],
    isUserListLoading: false,
    searchValue: '',
  };

  componentDidMount() {
    this.fetchUsersWithCurrentData();
  }

  fetchUsersWithCurrentData = () => {
    const { userRole } = this.props;

    const { page, pageSize } = this.state.paginationInfo;

    this.fetchUserList(userRole, page, pageSize);
  };

  fetchUserList = async (userRole, page, pageSize, searchValue) => {
    this.setState({ isUserListLoading: true });

    const { data: users, pageInfo } = (await getAllUser(
      userRole,
      page,
      pageSize,
      searchValue,
    ).catch(showApiErrorMessage)) || { data: [], pageInfo: {} };

    const paginationInfo = {
      ...this.state.paginationInfo,
      pages: pageInfo.totalPages,
      page: pageInfo.pageNo,
      pageSize: pageSize,
    };

    this.setState({ isUserListLoading: false, users, paginationInfo });
  };

  handleOnClickDelete = ({ original }) => {
    this.setState({
      isDeleting: true,
      deleteId: original.id,
    });
  };

  confirmDelete = async () => {
    const { deleteId } = this.state;

    this.setState({ isUserListLoading: true });
    await deleteUser(deleteId).catch(showApiErrorMessage);
    this.cancelDelete();
    this.fetchUsersWithCurrentData();
  };

  cancelDelete = () => {
    this.setState({
      deleteId: null,
      isDeleting: false,
    });
  };

  getColumnTable = () => {
    return [
      {
        id: 'sn',
        Header: 'S.N',
        sortable: false,
        filterable: false,
        width: 50,
        Cell: ({ index }) => {
          const { page, pageSize } = this.state.paginationInfo;

          return (
            <span className="serial-num">{page * pageSize + (index + 1)}</span>
          );
        },
      },
      {
        Header: 'Name',
        id: 'name',
        Cell: ({ original }) => (
          <div>{`${original.firstName} ${original.middleName || ''} ${
            original.lastName
          }`}</div>
        ),
        filterMethod: (filter, row) => {
          const name = `${row._original.firstName} ${row._original.middleName ||
            ''} ${row._original.lastName}`;

          return name.toLowerCase().indexOf(filter.value.toLowerCase()) !== -1;
        },
      },
      {
        Header: 'Join Date',
        id: 'createdAt',
        accessor: ({ createdAt }) => getFormattedDate(createdAt) || '',
      },
      {
        Header: 'Address',
        accessor: 'address',
      },
      {
        Header: 'Mobile Number',
        accessor: 'mobileNumber',
        width: 120,
        Cell: ({ original }) => {
          return (
            <span>
              {this.props.showHiddenData ? original.mobileNumber : '*****'}
            </span>
          );
        },
      },
      {
        Header: 'Email',
        accessor: 'email',
        Cell: ({ original }) => {
          return (
            <span>{this.props.showHiddenData ? original.email : '*****'}</span>
          );
        },
      },
      {
        Header: 'User Name',
        accessor: 'userName',
      },
      {
        id: 'action',
        sortable: false,
        width: 100,
        Header: 'Action',
        Cell: (row, { value }) => (
          <div>
            <Link
              to={{
                pathname: '/admin/register',
                state: {
                  data: row.original,
                },
              }}
            >
              <Button color="success" className="m-1">
                <i className="fa fa-pencil" />
              </Button>
            </Link>
            {this._hideDeleteButtonForLoggedInUser(row)}
          </div>
        ),
      },
    ];
  };

  _hideDeleteButtonForLoggedInUser = row => {
    let loggedInUser = getItem('oes_user');

    if (loggedInUser && loggedInUser['userName'] !== row['original'].userName) {
      return (
        <Button
          color="danger"
          className="m-1"
          onClick={() => this.handleOnClickDelete(row)}
        >
          <i className="fa fa-trash" />
        </Button>
      );
    }

    return null;
  };

  customDefaultFilterMethod = (filter, row, column) => {
    const id = filter.pivotId || filter.id;

    return row[id] !== undefined
      ? String(row[id])
          .toLowerCase()
          .indexOf(filter.value.toLowerCase()) !== -1
      : true;
  };

  onPaginationInfoChange = info => {
    const paginationInfo = { ...this.state.paginationInfo, ...info };

    this.fetchUserList(
      this.props.userRole,
      paginationInfo.page,
      paginationInfo.pageSize,
    );
  };

  handleOnSearchValueChange = event => {
    this.setState({
      searchValue: event.target.value,
    });

    const { userRole } = this.props;

    const { page, pageSize } = this.state.paginationInfo;

    this.fetchUserList(userRole, page, pageSize, event.target.value);
  };

  render() {
    const {
      isDeleting,
      deleteId,
      paginationInfo,
      users,
      searchValue,
    } = this.state;

    return (
      <div>
        {isDeleting && deleteId ? (
          <ConfirmPopup
            handleCancel={this.cancelDelete}
            handleConfirm={this.confirmDelete}
          />
        ) : null}
        <Row className="cardSearch">
          <Col sm="4">
            <Input
              required
              type="text"
              name="search"
              value={searchValue}
              placeholder="Search Here"
              autoComplete="searchValue"
              onChange={this.handleOnSearchValueChange}
            />
          </Col>
        </Row>

        <ReactTable
          manual
          minRows={0}
          data={users}
          sortable={true}
          NextComponent={NextPageButton}
          columns={this.getColumnTable()}
          PreviousComponent={PreviousPageButton}
          PaginationComponent={CBReactTablePagination}
          defaultFilterMethod={this.customDefaultFilterMethod}
          className="-striped -highlight practice-question-table"
          {...paginationInfo}
          onPageChange={page => this.onPaginationInfoChange({ page })}
          onPageSizeChange={pageSize =>
            this.onPaginationInfoChange({ pageSize })
          }
          loading={this.state.isUserListLoading}
        />
      </div>
    );
  }
}

export default UserTable;
