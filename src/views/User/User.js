import React from 'react';
import classnames from 'classnames';
import 'react-table/react-table.css';
import {
  Card,
  CardBody,
  CardHeader,
  Button,
  Row,
  TabContent,
  TabPane,
  NavItem,
  Nav,
  NavLink,
  Col,
  Input,
  Label,
} from 'reactstrap';

import UserTable from './common/UserTable';
import { userRole, apiUrls, localStorageKeys } from '../../utils/constants';
import ConfirmPopup from '../../commons/ConfirmPopup/ConfirmPopup';
import { getItem } from '../../utils/localStorage';
import { toast } from 'react-toastify';
import { getHttpErrorMessage, showApiErrorMessage } from '../../utils/httpUtil';
import Axios from 'axios';
import { getJsonFromFile } from '../../utils/fileUtils';
import { maxStudentRegistrationsToUpload } from './constants';
import { postStudentRegistrations } from './UserService';

class User extends React.Component {
  state = {
    activeTab: '1',
    showHiddenData: false,
    isShowHiddenDataLoading: false,
    isUploadingRegistrations: false,
    password: '',
  };

  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  };

  onChangePassword = event => {
    this.setState({
      password: event.target.value,
    });
  };

  handleConfirm = () => {
    // trim userName
    const userName = getItem(localStorageKeys.oesUser).userName;
    const password = this.state.password.trim();
    if (!password) {
      return;
    }

    Axios.post(apiUrls.login, { userName, password })
      .then(response => {
        this.setState({
          password: '',
          isShowHiddenDataLoading: false,
          showHiddenData: true,
        });
      })
      .catch(error => {
        toast.error(getHttpErrorMessage(error), {
          position: toast.POSITION.BOTTOM_CENTER,
        });
      });
  };

  handleCancel = () => {
    this.setState({
      password: '',
      isShowHiddenDataLoading: false,
    });
  };

  handleClickOnHideBtn = () => {
    if (this.state.showHiddenData) {
      this.setState({
        showHiddenData: false,
      });

      return;
    }
    this.setState({ isShowHiddenDataLoading: true });
  };

  handleRegistrationsUpload = e => {
    const file = e.target.files[0];

    // clearing the input value, in case the user wants to import same file again
    e.target.value = null;

    this.setState({ isUploadingRegistrations: true });

    getJsonFromFile(file)
      .then(async registrations => {
        if (!(registrations && registrations.length)) {
          return toast.error('Empty data extracted');
        }

        if (registrations.length > maxStudentRegistrationsToUpload) {
          return toast.error(
            `Only upto ${maxStudentRegistrationsToUpload} rows are allowed`,
          );
        }

        const formattedRegistrations = registrations.reduce(
          (registrations, registrationItem) => {
            const formattedRegistrationItem = {};
            Object.entries(registrationItem || {}).forEach(([key, value]) => {
              const trimmedLowerCasedKey = (key || '').trim().toLowerCase();
              const trimmedValue = (value || '').trim();

              if (trimmedLowerCasedKey === 'reg_no') {
                formattedRegistrationItem.number = trimmedValue;
              } else if (trimmedLowerCasedKey === 'college') {
                formattedRegistrationItem.collegeName = trimmedValue;
              } else if (trimmedLowerCasedKey === 'group') {
                formattedRegistrationItem.group = trimmedValue;
              } else if (trimmedLowerCasedKey === 'firstname') {
                formattedRegistrationItem.firstName = trimmedValue;
              } else if (trimmedLowerCasedKey === 'middlename') {
                formattedRegistrationItem.middleName = trimmedValue;
              } else if (trimmedLowerCasedKey === 'lastname') {
                formattedRegistrationItem.lastName = trimmedValue;
              }
            });

            if (Object.keys(formattedRegistrationItem).length) {
              registrations.push(formattedRegistrationItem);
            }

            return registrations;
          },
          [],
        );

        if (!formattedRegistrations.length) {
          return toast.error('No any valid row to upload');
        }

        const response = await postStudentRegistrations(
          formattedRegistrations,
        ).catch(showApiErrorMessage);

        if (response && response.message) {
          toast.success(response.message);
        }

        return;
      })
      .then(() => {
        this.setState({ isUploadingRegistrations: false });
      });
  };

  render() {
    return (
      <Card>
        {this.state.isShowHiddenDataLoading ? (
          <ConfirmPopup
            message={'Enter Password to see hidden data'}
            handleCancel={this.handleCancel}
            confirmText="Continue"
            cancelText="Cancel"
            handleConfirm={this.handleConfirm}
          >
            <Input
              onChange={this.onChangePassword}
              type="password"
              placeholder="Enter password here"
            />
          </ConfirmPopup>
        ) : null}
        <CardHeader>
          <Row>
            <Col xs="12" md="7">
              <Nav tabs>
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === '1',
                    })}
                    onClick={() => {
                      this.toggle('1');
                    }}
                  >
                    Admin Users
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === '2',
                    })}
                    onClick={() => {
                      this.toggle('2');
                    }}
                  >
                    Student Users
                  </NavLink>
                </NavItem>
              </Nav>
            </Col>
            <Col
              xs="12"
              md="5"
              className="d-flex justify-content-around align-items-center"
            >
              <Label
                className="btn btn-secondary d-inline-flex m-0"
                htmlFor={`upload-registrations`}
                style={
                  this.state.isUploadingRegistrations
                    ? {
                        opacity: '0.5',
                        pointerEvents: 'none',
                      }
                    : {
                        opacity: '1',
                        pointerEvents: 'auto',
                      }
                }
              >
                {this.state.isUploadingRegistrations
                  ? 'Uploading...'
                  : 'Upload Student Registrations'}
                <Input
                  type="file"
                  id={`upload-registrations`}
                  style={{ display: 'none' }}
                  onChange={this.handleRegistrationsUpload}
                />
              </Label>

              <Button onClick={this.handleClickOnHideBtn}>
                {this.state.showHiddenData
                  ? 'Hide Sensitive Data'
                  : 'Show Hidden Data'}
              </Button>
            </Col>
          </Row>
        </CardHeader>
        <CardBody>
          <TabContent activeTab={this.state.activeTab}>
            <TabPane tabId="1">
              <UserTable
                deleteUser={this.confirmDelete}
                userRole={userRole.admin}
                showHiddenData={this.state.showHiddenData}
              />
            </TabPane>
            <TabPane tabId="2">
              <div>
                <UserTable
                  deleteUser={this.confirmDelete}
                  userRole={userRole.student}
                  showHiddenData={this.state.showHiddenData}
                />
              </div>
            </TabPane>
          </TabContent>
        </CardBody>
      </Card>
    );
  }
}

export default User;
