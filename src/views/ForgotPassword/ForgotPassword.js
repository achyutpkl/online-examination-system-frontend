import { toast } from 'react-toastify';
import React, { Component } from 'react';
import {
  Row,
  Col,
  Container,
  Card,
  CardBody,
  Form,
  Input,
  Button,
  InputGroup,
  InputGroupText,
  InputGroupAddon,
} from 'reactstrap';

import { post } from '../../utils/httpUtil';
import { apiUrls } from '../../utils/constants';
import forgotPasswordImage from '../../assets/images/forgot-password.svg';

import CircularIconImage from '../../commons/CircularIconImage/CircularIconImage';

export default class ForgotPassword extends Component {
  state = {
    email: '',
    isSending: false,
    isSuccess: false,
  };

  handleSubmit = async e => {
    e.preventDefault();

    const { email, isSending, isSuccess: currentSuccessState } = this.state;

    if (!email || isSending) {
      return;
    }

    this.setState({ isSending: true });

    let isSuccess = currentSuccessState;

    await post(apiUrls.forgotPassword, { email })
      .then(response => {
        const message =
          (response && response.data && response.data.message) ||
          'Please check your email for link to reset password.';

        toast.success(message, { position: toast.POSITION.BOTTOM_CENTER });
        isSuccess = email;
      })
      .catch(() => {
        // TODO: Use error from server after server sends appropriate message
        toast.error('Given email has not been registered');
        isSuccess = false;
      });

    this.setState({ isSending: false, email: '', isSuccess });
  };

  handleInputChange = e => {
    const { name, value } = (e && e.target) || {};

    this.setState({ [name]: value });
  };

  render() {
    const { email, isSending, isSuccess } = this.state;

    return (
      <div className="flex-row align-items-center justify-content-center d-flex min-vh-100">
        <Container>
          <Row className="justify-content-center">
            <Col xs="12" md="8">
              <Card className="my-2">
                <CardBody className="text-center">
                  <CircularIconImage image={forgotPasswordImage} />

                  <Form onSubmit={this.handleSubmit}>
                    <h3 className="mb-1">Forgot Password?</h3>

                    <p className="text-muted mb-4">
                      Provide your email address to reset your password.
                    </p>

                    <InputGroup className="mb-2">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-envelope" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        type="email"
                        required
                        placeholder="Enter your email"
                        name="email"
                        value={email}
                        onChange={this.handleInputChange}
                      />
                    </InputGroup>

                    <Button
                      block
                      disabled={!email || isSending}
                      type="submit"
                      color="primary"
                      className="px-4"
                    >
                      Send
                    </Button>
                  </Form>

                  {isSuccess && (
                    <div className="text-muted">
                      Password reset link has been successfully been sent to
                      email address: {isSuccess}.{' '}
                      <a href="/">Go to Login Page.</a>
                    </div>
                  )}
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
