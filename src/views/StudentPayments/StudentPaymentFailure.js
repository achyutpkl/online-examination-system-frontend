import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Card, CardBody, Button } from 'reactstrap';

import { viewRoutes } from '../../utils/constants';
import CircularIconImage from '../../commons/CircularIconImage/CircularIconImage';

import failureImage from '../../assets/images/incorrect.png';

export default () => {
  return (
    <div className="flex-row justify-content-center align-items-center d-flex min-vh-100">
      <Container>
        <Row className="justify-content-center">
          <Col xs="12" md="8">
            <Card className="my-2">
              <CardBody className="text-center">
                <div className="mb-5">
                  <CircularIconImage
                    image={failureImage}
                    style={{
                      borderRadius: '50%',
                      padding: '6px',
                      filter: 'grayscale(0.2)',
                    }}
                    imageProps={{
                      style: {
                        width: '80px',
                      },
                    }}
                  />
                  <h3 style={{ color: '#505b65' }}>Payment Failed</h3>
                </div>

                <Button
                  block
                  className="px-4 my-2"
                  tag={Link}
                  to={viewRoutes.studentDashboard}
                >
                  Back To Dashboard
                </Button>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
};
