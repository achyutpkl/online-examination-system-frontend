import _ from 'lodash';
import { toast } from 'react-toastify';
import React, { Component } from 'react';
import { Container, Row, Col, Card, CardBody, Button } from 'reactstrap';

import browserHistory from '../../utils/history';
import { viewRoutes } from '../../utils/constants';
import {
  paymentUrls,
  merchantInfo,
  paymentServices,
  invalidFieldName,
  paymentFieldNames,
  paymentSuccessUrl,
  paymentFailureUrl,
  eSewaPaymentFieldsMap,
  eSewaPaymentDerivedFieldsMap,
} from './constants';

import CircularIconImage from '../../commons/CircularIconImage/CircularIconImage';

// TODO: Check if we need to clear location state value.
import paymentImage from '../../assets/images/money-transfer.svg';

export default class StudentPayment extends Component {
  state = {
    isPaying: false,
    paymentService: paymentServices.eSewa,
    paymentInfo: {
      [paymentFieldNames.taxAmount]: 0,
      [paymentFieldNames.productServiceCharge]: 0,
      [paymentFieldNames.productDeliveryCharge]: 0,
      [paymentFieldNames.productId]: 'pid',
      [paymentFieldNames.eSewaMerchantCode]: merchantInfo.eSewaMerchantCode,
      [paymentFieldNames.successUrl]: paymentSuccessUrl,
      [paymentFieldNames.failureUrl]: paymentFailureUrl,
    },
  };

  addHiddenInput = (form, name, value) => {
    const hiddenField = document.createElement('input');
    hiddenField.setAttribute('type', 'hidden');
    hiddenField.setAttribute('name', name);
    hiddenField.setAttribute('value', value);
    form.appendChild(hiddenField);
  };

  appendDerivedFields = form => {
    if (this.state.paymentService === paymentServices.eSewa) {
      // add the total amount
      const { fieldName: totalFieldName, value: totalAmount } =
        this.getTotalPaymentInfo() || {};

      if (
        !totalAmount ||
        !totalFieldName ||
        totalFieldName === invalidFieldName
      ) {
        return false;
      }

      this.addHiddenInput(form, totalFieldName, totalAmount);

      return true;
    }

    return false;
  };

  payWithEsewa = () => {
    const paymentInfo = this.state.paymentInfo;

    const form = document.createElement('form');
    form.setAttribute('method', 'POST');
    form.setAttribute('action', paymentUrls.eSewa);

    // append derived values separately
    if (!this.appendDerivedFields(form)) {
      toast.error('Invalid Data. Payment Cancelled');

      return;
    }

    Object.entries(eSewaPaymentFieldsMap).forEach(([key, fieldName]) => {
      this.addHiddenInput(form, fieldName, paymentInfo[key]);
    });

    document.body.appendChild(form);
    form.submit();
  };

  componentDidMount() {
    const { paymentFields, examId, userId } =
      (this.props.location && this.props.location.state) || {};

    if (!(examId && userId && paymentFields)) {
      return browserHistory.push(viewRoutes.studentDashboard);
    }

    this.setState({
      paymentInfo: {
        ...this.state.paymentInfo,
        ...paymentFields,
        [paymentFieldNames.successUrl]: `${paymentSuccessUrl}?usr=${userId}&exm=${examId}`,
      },
    });
  }

  getTotalPaymentInfo() {
    const invalidInfo = { fieldName: invalidFieldName, value: '0' };

    if (this.state.paymentService === paymentServices.eSewa) {
      const totalCalculationInfo =
        eSewaPaymentDerivedFieldsMap[paymentFieldNames.totalAmount];

      if (
        !(
          totalCalculationInfo &&
          totalCalculationInfo.sumFields &&
          totalCalculationInfo.name
        )
      ) {
        return invalidInfo;
      }

      const totalAmount = totalCalculationInfo.sumFields.reduce(
        (total, fieldName) => {
          const sumFieldValue = _.get(
            this.state,
            ['paymentInfo', fieldName],
            0,
          );

          total = total + (Number(sumFieldValue) || 0);

          return total;
        },
        0,
      );

      return { fieldName: totalCalculationInfo.name, value: totalAmount };
    }

    return invalidInfo;
  }

  render() {
    const { isPaying } = this.state;
    const { paymentName } = this.state.paymentInfo;
    const { value: totalAmount } = this.getTotalPaymentInfo() || {};

    return (
      <div className="flex-row justify-content-center d-flex min-vh-100">
        <Container>
          <Row className="justify-content-center">
            <Col xs="12" md="8">
              <Card className="my-2">
                <CardBody className="text-center">
                  <CircularIconImage
                    image={paymentImage}
                    className="d-inline-block my-1 bg-green"
                    style={{
                      borderRadius: '50%',
                      padding: '12px',
                      filter: 'grayscale(0.2)',
                    }}
                    imageProps={{
                      style: {
                        width: '100px',
                      },
                    }}
                  />

                  <div className="mt-4 mb-5">
                    <h3 className="mb-1" style={{ color: '#505b65' }}>
                      {paymentName}
                    </h3>
                    <h4 style={{ color: '#5c6873' }}>
                      Total Amount: Rs. {totalAmount}
                    </h4>
                  </div>

                  <Button
                    block
                    className="px-4 my-2"
                    color="primary"
                    onClick={this.payWithEsewa}
                    disabled={!Number(totalAmount) || isPaying}
                  >
                    Pay with eSewa
                  </Button>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
