import qs from 'query-string';
import { Link } from 'react-router-dom';
import React, { Component } from 'react';
import { Container, Row, Col, Card, CardBody, Button } from 'reactstrap';

import { viewRoutes } from '../../utils/constants';
import { showApiErrorMessage } from '../../utils/httpUtil';
import { addExamPaymentDetail } from '../../services/ExamPaymentService';

import CircularIconImage from '../../commons/CircularIconImage/CircularIconImage';

import successImage from '../../assets/images/success.png';

export default class StudentPaymentSucces extends Component {
  state = {
    isVerified: false,
    isVerifying: true,
  };

  componentDidMount() {
    this.verifyPayment();
  }

  async verifyPayment() {
    const values = qs.parse(this.props.location && this.props.location.search);

    const paymentInfo = {
      amount: values.amt,
      transactionCode: values.refId,
      paymentId: values.oid,
      exam: {
        id: values.exm,
      },
      user: {
        id: values.usr,
      },
    };

    let verificationErrorOccurred = false;

    await addExamPaymentDetail(paymentInfo).catch(error => {
      verificationErrorOccurred = true;
      showApiErrorMessage(error);
    });

    this.setState({
      isVerifying: false,
      isVerified: !verificationErrorOccurred,
    });
  }

  render() {
    const { isVerified, isVerifying } = this.state;

    return (
      <div className="flex-row justify-content-center align-items-center d-flex min-vh-100">
        <Container>
          <Row className="justify-content-center">
            <Col xs="12" md="8">
              <Card className="my-2">
                <CardBody className="text-center">
                  <>
                    {isVerifying ? (
                      <div className="display-4 p-2">Verifying.....</div>
                    ) : isVerified ? (
                      <>
                        <div className="mb-5">
                          <CircularIconImage
                            image={successImage}
                            style={{ borderRadius: '50%', padding: '6px' }}
                            imageProps={{
                              style: {
                                width: '80px',
                              },
                            }}
                          />
                          <h3 style={{ color: '#505b65' }}>
                            Your Payment Was Successful
                          </h3>
                        </div>

                        <Button
                          block
                          className="px-4 my-2"
                          color="primary"
                          tag={Link}
                          to={viewRoutes.studentDashboard}
                        >
                          Back To Dashboard
                        </Button>
                      </>
                    ) : (
                      <div>
                        {/* TODO: Change the following message */}
                        Sorry your payment could not be verified. Please contact
                        admin
                      </div>
                    )}
                  </>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
