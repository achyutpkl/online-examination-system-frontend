import {
  webserverUrl,
  eSewaPaymentUrl,
  eSewaMerchantCode,
} from '../../utils/constants';

export const invalidFieldName = 'invalidField';

export const paymentServices = {
  eSewa: 'E-Sewa',
};

// TODO: Replace esewa url
export const paymentUrls = {
  eSewa: eSewaPaymentUrl,
};

export const paymentSuccessUrl = `${webserverUrl}/student/payment-success`;
export const paymentFailureUrl = `${webserverUrl}/student/payment-failure`;

export const paymentFieldNames = {
  amount: 'amount',
  paymentName: 'paymentName',
  taxAmount: 'taxAmount',
  productServiceCharge: 'psc',
  productDeliveryCharge: 'pdc',
  productId: 'pid',
  eSewaMerchantCode: 'scd',
  successUrl: 'su',
  failureUrl: 'fu',
  totalAmount: 'total',
};

export const eSewaPaymentFieldsMap = {
  [paymentFieldNames.amount]: 'amt',
  [paymentFieldNames.taxAmount]: 'txAmt',
  [paymentFieldNames.productServiceCharge]: 'psc',
  [paymentFieldNames.productDeliveryCharge]: 'pdc',
  [paymentFieldNames.productId]: 'pid',
  [paymentFieldNames.eSewaMerchantCode]: 'scd',
  [paymentFieldNames.successUrl]: 'su',
  [paymentFieldNames.failureUrl]: 'fu',
};

export const eSewaPaymentDerivedFieldsMap = {
  [paymentFieldNames.totalAmount]: {
    name: 'tAmt',
    description: 'totalAmount',
    sumFields: [
      paymentFieldNames.amount,
      paymentFieldNames.taxAmount,
      paymentFieldNames.productServiceCharge,
      paymentFieldNames.productDeliveryCharge,
    ],
  },
};

export const merchantInfo = {
  eSewaMerchantCode: eSewaMerchantCode,
};
