import React, { Component } from 'react';
import { Col } from 'reactstrap';
import { getAllUser } from '../User/UserService';
import { userRole } from '../../utils/constants';
import { showApiErrorMessage } from '../../utils/httpUtil';
import { getSubjects } from '../../services/SubjectService';
import { getExams } from '../../services/ExamService';
import Widget from '../../commons/Widget/Widget';

const cardSecondaryColor = ['bg-info', 'bg-success', 'bg-warning', 'bg-danger'];

export default class _ extends Component {
  state = {
    totalAdmins: 0,
    runningExams: 0,
    totalStudents: 0,
    totalSubjects: 0,
    isUserLoading: false,
  };

  componentDidMount() {
    this.fetchUserList();
  }

  fetchUserList = async () => {
    this.setState({
      isUserLoading: true,
    });

    const {
      pageInfo: { totalElements: totalStudents },
    } = (await getAllUser(userRole.student, 0, 1).catch(
      showApiErrorMessage,
    )) || { pageInfo: {} };

    const {
      pageInfo: { totalElements: totalAdmins },
    } = (await getAllUser(userRole.admin, 0, 1).catch(showApiErrorMessage)) || {
      pageInfo: {},
    };
    const {
      pageInfo: { totalElements: totalSubjects },
    } = (await getSubjects().catch(showApiErrorMessage)) || { pageInfo: {} };

    const {
      pageInfo: { totalElements: runningExams },
    } = (await getExams({ examStatus: 'INPROGRESS' }).catch(
      showApiErrorMessage,
    )) || { pageInfo: {} };

    this.setState({
      isUserLoading: false,
      totalAdmins: totalAdmins ? totalAdmins : 0,
      runningExams: runningExams ? runningExams : 0,
      totalStudents: totalStudents ? totalStudents : 0,
      totalSubjects: totalSubjects ? totalSubjects : 0,
    });
  };

  render() {
    return (
      <div className="row">
        {this.state.isUserLoading ? (
          <div>Loading...</div>
        ) : (
          <>
            <Col className="col-12 col-sm-6 col-lg-3">
              <Widget
                title="Total Students"
                value={this.state.totalStudents}
                icon="users"
                color={cardSecondaryColor[0]}
              />
            </Col>
            <Col className="col-12 col-sm-6 col-lg-3">
              <Widget
                title="Total Admins"
                value={this.state.totalAdmins}
                icon="user"
                color={cardSecondaryColor[1]}
              />
            </Col>
            <Col className="col-12 col-sm-6 col-lg-3">
              <Widget
                title="Total Subjects"
                value={this.state.totalSubjects}
                icon="book"
                color={cardSecondaryColor[2]}
              />
            </Col>
            <Col className="col-12 col-sm-6 col-lg-3">
              <Widget
                title="Total Exams"
                value={this.state.totalSubjects}
                icon="file-text-o"
                color={cardSecondaryColor[3]}
              />
            </Col>
          </>
        )}
      </div>
    );
  }
}
