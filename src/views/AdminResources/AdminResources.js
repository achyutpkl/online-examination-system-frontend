import React, { Component } from 'react';
import { Card, CardTitle, Row, Col } from 'reactstrap';

import {
  formulaCheatSheetUrl,
  examQuestionTemplateUrl,
  practiceQuestionTemplateUrl,
} from '../../utils/constants';

const DownloadButton = ({ url, name }) => (
  <a
    className="btn btn-primary"
    target="_blank"
    rel="noopener noreferrer"
    href={url}
    download
  >
    <i className="fa fa-download" />
    <span className="ml-1">{name}</span>
  </a>
);

class AdminResources extends Component {
  render() {
    return (
      <div>
        <Card body>
          <CardTitle>
            Click the buttons below to download template files.
          </CardTitle>

          <Row>
            <Col xs="12" className="mb-1">
              <DownloadButton
                url={examQuestionTemplateUrl}
                name="Exam Questions Template"
              />
            </Col>

            <Col xs="12">
              <DownloadButton
                url={practiceQuestionTemplateUrl}
                name="Practice Questions Template"
              />
            </Col>
          </Row>
        </Card>

        <Card body>
          <CardTitle>
            Click the button below to download formula cheat sheet.
          </CardTitle>

          <Row>
            <Col xs="12">
              <DownloadButton
                url={formulaCheatSheetUrl}
                name="Formula Cheat Sheet"
              />
            </Col>
          </Row>
        </Card>
      </div>
    );
  }
}

export default AdminResources;
