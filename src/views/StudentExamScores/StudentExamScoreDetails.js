import React, { Component } from 'react';
import { Card, CardHeader, CardBody } from 'reactstrap';

import { showApiErrorMessage } from '../../utils/httpUtil';
import { getSortedExamSections } from '../Exams/utils/utils';
import { getExamScoreDetails } from '../../services/ExamService';

import ExamFact from './commons/ExamFact';
import ExamSectionScore from './commons/ExamSectionScore';

export default class StudentExamScoreDetails extends Component {
  state = {
    examId: '',
    scoreId: '',
    scoreDetails: {},
  };

  componentDidMount() {
    const { examId, scoreId } =
      (this.props.match && this.props.match.params) || {};

    this.setState({ examId, scoreId });

    this.fetchScoreDetails(examId, scoreId);
  }

  fetchScoreDetails = async (examId, scoreId) => {
    const scoreDetails =
      (await getExamScoreDetails(examId, scoreId).catch(showApiErrorMessage)) ||
      {};

    this.setState({
      ...scoreDetails,
      examSectionScores: getSortedExamSections(
        scoreDetails.examSectionScores,
        'questionAnswers',
      ),
    });
  };

  toggleSection = sectionId => {
    const examSectionScores = this.state.examSectionScores || [];

    this.setState({
      examSectionScores: examSectionScores.map(sectionScore => {
        if (sectionScore.id === sectionId) {
          return { ...sectionScore, isOpen: !sectionScore.isOpen };
        }

        return sectionScore;
      }),
    });
  };

  render() {
    const {
      title,
      fullMark,
      passMark,
      markObtained,
      examSectionScores,
    } = this.state;

    return (
      <Card>
        <CardHeader>
          <h4>{title}</h4>
          <div>
            <ExamFact name="Full Marks" value={fullMark} color="light" />

            <ExamFact name="Pass Marks" value={passMark} color="light" />

            <ExamFact name="Obtained Marks" value={markObtained} color="info" />
          </div>
        </CardHeader>

        <CardBody className="smallCardBody py-1">
          {examSectionScores &&
            examSectionScores.map(sectionScore => (
              <div className="mb-2" key={sectionScore.id}>
                <ExamSectionScore
                  {...sectionScore}
                  toggleSection={this.toggleSection}
                />
              </div>
            ))}
        </CardBody>
      </Card>
    );
  }
}
