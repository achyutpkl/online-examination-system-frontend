import { Link } from 'react-router-dom';
import React, { Component } from 'react';
import {
  Row,
  Col,
  Card,
  Badge,
  Button,
  CardBody,
  CardHeader,
} from 'reactstrap';

import browserHistory from '../../utils/history';
import { viewRoutes } from '../../utils/constants';
import { showApiErrorMessage } from '../../utils/httpUtil';
import { getExamScores, getExam } from '../../services/ExamService';
import { getFormattedDateTime, sortListByDate } from '../../utils/dateUtils';

import ListWithRouting from '../StudentDashboard/commons/ListWithRouting';

export default class StudentExamScores extends Component {
  state = {
    examId: '',
    scores: [],
    isLoadingScores: true,
    examDetails: {},
  };

  componentDidMount() {
    const { examId } = (this.props.match && this.props.match.params) || {};

    this.fetchData(examId);
  }

  fetchData = async examId => {
    this.setState({ examId });

    const examScore = await this.fetchExamScores(examId);
    const examDetails = await this.fetchExamDetails(examId);

    this.setState({
      examDetails,
      scores: examScore,
      isLoadingScores: false,
    });
  };

  fetchExamDetails = async examId => {
    const examDetails =
      (await getExam(examId).catch(showApiErrorMessage)) || {};

    // this.setState({ examDetails });

    return examDetails;
  };

  fetchExamScores = async examId => {
    const scores =
      (await getExamScores(examId).catch(showApiErrorMessage)) || [];

    // this.setState({ scores: sortListByDate(scores, 'attemptDate') });

    return sortListByDate(scores, 'attemptDate');
  };

  onTakeExam = () => {
    const { id: examId } = this.state.examDetails || {};

    if (examId) {
      browserHistory.push(viewRoutes.studentExamPaper.replace(':id', examId));
    }
  };

  renderScoresIfAvailable = () => {
    const { isLoadingScores, scores, examId, examDetails } = this.state;

    const examStatus = examDetails && examDetails.examStatus;

    if (isLoadingScores) {
      return 'Loading...';
    }

    if (!(scores && scores.length)) {
      return (
        <div>
          <div>No scores available</div>

          {examStatus === 'INPROGRESS' ? (
            <div>
              You have not taken this exam before.{' '}
              <Button color="primary" onClick={this.onTakeExam}>
                Take Exam Now
              </Button>
            </div>
          ) : examStatus === 'COMPLETED' ? (
            <div>
              You did not take this exam. Try other{' '}
              <Link to={viewRoutes.studentExamList}>exams</Link>
            </div>
          ) : null}
        </div>
      );
    }

    return (
      <ListWithRouting
        keyProp="examScoreId"
        items={scores}
        routePath={viewRoutes.studentExamScoreDetails
          .replace(':examId', examId)
          .replace('/:scoreId', '')}
        render={({ attemptDate, markObtained }) => (
          <Row>
            <Col xs="12" md="6">
              Attempted Date:{' '}
              <Badge color="light">{getFormattedDateTime(attemptDate)}</Badge>
            </Col>

            <Col xs="12" md="6">
              Marks Obtained: <Badge color="success">{markObtained}</Badge>
            </Col>
          </Row>
        )}
      />
    );
  };

  render() {
    const { examDetails } = this.state;

    return (
      <div className="animated-fadeIn">
        <Card>
          <CardHeader>
            Your scores for{' '}
            <b>{(examDetails && examDetails.title) || 'this exam'}</b>:
          </CardHeader>
          <CardBody className="smallCardBody">
            {this.renderScoresIfAvailable()}
          </CardBody>
        </Card>
      </div>
    );
  }
}
