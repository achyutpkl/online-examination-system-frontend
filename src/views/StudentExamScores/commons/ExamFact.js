import React from 'react';
import { Badge } from 'reactstrap';

export default ({ name, value, color }) => (
  <span className="examFact mr-2">
    {name}: <Badge color={color}>{value}</Badge>
  </span>
);
