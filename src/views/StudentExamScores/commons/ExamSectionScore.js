import React from 'react';
import {
  Card,
  CardHeader,
  Button,
  Collapse,
  CardBody,
  CustomInput,
  Alert,
} from 'reactstrap';

import ExamFact from './ExamFact';

import QuestionTitle from '../../../commons/QuestionTitle/QuestionTitle';
import QuestionOption from '../../../commons/QuestionOption/QuestionOption';

const renderCheckAnswer = (key, correctAnswer, studentAnswer) => {
  if (correctAnswer === key) {
    return <i className="answerCheck correct fa fa-check" aria-hidden="true" />;
  }

  if (studentAnswer === key) {
    return <i className="answerCheck wrong fa fa-times" aria-hidden="true" />;
  }
};

export default ({
  id,
  title,
  isOpen,
  totalCorrectAnswer,
  totalSectionScore,
  totalWrongAnswer,
  toggleSection,
  totalTimeTaken,
  questionAnswers,
}) => (
  <Card className="mb-0">
    <CardHeader id="headingOne">
      <Button
        block
        color="link"
        style={{ textDecoration: 'none' }}
        className="text-left m-0 p-0"
        onClick={() => toggleSection(id)}
        aria-expanded={isOpen}
        aria-controls="collapseOne"
      >
        <h5 className="m-0 p-0">{title}</h5>

        <ExamFact
          name="Total Correct Answers"
          value={totalCorrectAnswer}
          color="success"
        />

        <ExamFact
          name="Total Wrong Answers"
          value={totalWrongAnswer}
          color="danger"
        />

        <ExamFact
          name="Total Section Score"
          value={totalSectionScore}
          color="info"
        />

        <ExamFact
          name="Total Time Taken"
          value={totalTimeTaken || 'N/A'}
          color="info"
        />
      </Button>
    </CardHeader>

    <Collapse
      isOpen={isOpen}
      data-parent="#accordion"
      id="collapseOne"
      aria-labelledby="headingOne"
    >
      <CardBody className="smallCardBody">
        {/* TODO: Extract the following into reuseable component */}
        {questionAnswers &&
          questionAnswers.map(question => (
            <div
              key={question.questionId}
              className={
                !!question.studentAnswer
                  ? 'questionAttemptedWithoutFade practiceQuestionWrp'
                  : 'practiceQuestionWrp'
              }
            >
              <QuestionTitle
                className="h5"
                dangerouslySetInnerHTML={{ __html: question.title }}
              />

              <div className="optionWrp">
                {question.options &&
                  Object.keys(question.options).map(key => (
                    <div key={key}>
                      <CustomInput
                        disabled
                        className="practiceQuestionOption"
                        type="radio"
                        id={key}
                        name={question.id}
                        label={
                          <QuestionOption
                            dangerouslySetInnerHTML={{
                              __html: question.options[key],
                            }}
                          />
                        }
                        value={key}
                        checked={question.studentAnswer === key}
                      />

                      {renderCheckAnswer(
                        key,
                        question.correctAnswer,
                        question.studentAnswer,
                      )}
                    </div>
                  ))}
              </div>

              <Alert color="warning" className="explanationWrp">
                <div
                  dangerouslySetInnerHTML={{
                    __html:
                      question.explanation ||
                      'Sorry, there is no explanation available for this question',
                  }}
                />
              </Alert>
            </div>
          ))}
      </CardBody>
    </Collapse>
  </Card>
);
