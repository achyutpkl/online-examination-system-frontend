import React from 'react';
import ReactTable from 'react-table';
import { Button, Badge } from 'reactstrap';

import {
  NextPageButton,
  PreviousPageButton,
} from '../Question/commons/PaginationButton';
import browserHistory from '../../utils/history';
import { showApiErrorMessage } from '../../utils/httpUtil';
import { getAllScoreForExam } from './ExamService';
import { viewRoutes } from '../../utils/constants';
import { getFormattedDate, getFormattedTime } from '../../utils/dateUtils';
import CBReactTablePagination from '../../commons/CustomReactTablePagination/CBReactTablePagination';

class ExamResult extends React.Component {
  state = {
    examResult: [],
    paginationInfo: {
      pages: 1,
      page: 0,
      pageSize: 50,
      totalElements: 0,
    },
    isExamResultLoading: false,
  };

  componentDidMount() {
    this.fetchExamResultWithCurrentData();
  }

  fetchExamResult = async (examId, page, pageSize) => {
    this.setState({ isExamResultLoading: true });

    const { data: examResult, pageInfo } = (await getAllScoreForExam(
      examId,
      page,
      pageSize,
    ).catch(showApiErrorMessage)) || { data: [], pageInfo: {} };

    const paginationInfo = {
      ...this.state.paginationInfo,
      pages: pageInfo ? pageInfo.totalPages : 0,
      page: pageInfo ? pageInfo.pageNo : 0,
      pageSize: pageSize,
      totalElements: pageInfo ? pageInfo.totalElements : 0,
    };

    this.setState({ isExamResultLoading: false, examResult, paginationInfo });
  };

  fetchExamResultWithCurrentData = () => {
    const { examId } = this.props;

    if (!examId) {
      return this.setState({ examResult: [] });
    }

    const { page, pageSize } = this.state.paginationInfo;

    this.fetchExamResult(examId, page, pageSize);
  };

  onPaginationInfoChange = info => {
    const paginationInfo = { ...this.state.paginationInfo, ...info };

    this.fetchExamResult(
      this.props.examId,
      paginationInfo.page,
      paginationInfo.pageSize,
    );
  };

  openDetailView = value => {
    const { examScoreId } = value;
    const { examId } = this.props;
    if (examId) {
      browserHistory.push(
        viewRoutes.adminStudentExamScoreDetails
          .replace(':examId', examId)
          .replace(':scoreId', examScoreId),
      );
    }
  };

  columns = [
    {
      Header: 'Rank',
      accessor: 'rank',
      maxWidth: 50,
    },
    {
      Header: 'Name',
      Cell: ({ original }) => (
        <div>{`${original.studentDetail.firstName} ${original.studentDetail
          .middleName || ''} ${original.studentDetail.lastName}`}</div>
      ),
    },
    {
      Header: 'Score',
      accessor: 'studentScore',
    },
    {
      Header: 'Date',
      accessor: 'attemptDate',
      Cell: ({ value }) => (
        <div>
          {value && getFormattedDate(value) + ' ' + getFormattedTime(value)}
        </div>
      ),
    },
    {
      Header: 'View',
      maxWidth: 100,
      Cell: ({ original }) => (
        <div className="actions d-flex justify-content-around">
          <Button color="success" onClick={() => this.openDetailView(original)}>
            View
          </Button>
        </div>
      ),
    },
  ];

  render() {
    const { paginationInfo, examResult } = this.state;

    return (
      <div>
        <p>
          <span className="mr-2">
            Top Score:{' '}
            <Badge color="success">
              {examResult.length > 0 ? examResult[0].highestScore : 'N/A'}
            </Badge>
          </span>
          <span className="mr-2">
            Average Score:{' '}
            <Badge color="primary">
              {examResult.length > 0 ? examResult[0].averageScore : 'N/A'}
            </Badge>
          </span>
          <span className="mr-2">
            Total Attempts:{' '}
            <Badge color="primary">
              {paginationInfo.totalElements || 'N/A'}
            </Badge>
          </span>
        </p>
        <ReactTable
          manual
          minRows={0}
          sortable={false}
          data={examResult}
          {...paginationInfo}
          columns={this.columns}
          NextComponent={NextPageButton}
          PreviousComponent={PreviousPageButton}
          loading={this.state.isExamResultLoading}
          PaginationComponent={CBReactTablePagination}
          className="-striped -highlight practice-question-table"
          onPageChange={page => this.onPaginationInfoChange({ page })}
          onPageSizeChange={pageSize =>
            this.onPaginationInfoChange({ pageSize })
          }
        />
      </div>
    );
  }
}

export default ExamResult;
