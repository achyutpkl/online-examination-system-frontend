import { apiUrls } from '../../utils/constants';
import { get, del } from '../../utils/httpUtil';

export const getAllScoreForExam = async (examId, page, size) => {
  const url = `${apiUrls.examResult.replace(
    ':examId',
    examId,
  )}?page=${page}&size=${size}`;
  let result = await get(url);

  return (result && result.data) || {};
};

export const getDetailScoreOfStudent = async id => {
  let response = await del(`${apiUrls.user}/${id}`);

  return (response && response.data) || {};
};
