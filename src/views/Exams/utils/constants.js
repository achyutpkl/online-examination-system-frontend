export const sectionTitlePlaceholder = '{sectionTitle}';

export const numberOfQuestionOptions = 4;

export const formValidationMessages = {
  default: 'Please enter all required data correctly', // Used in case browser form validation doesn't work
  noSections: 'Create at least one or more sections',
  noSectionTitle: 'Enter section title',
  noSectionData: 'Section data is not valid',
  noSectionQuestions: `The section '${sectionTitlePlaceholder}' doesn't have any question`,
  noQuestionTitle: 'A question must have a title',
  noCorrectAnswer: 'A question must have a correct answer',
  noOptions: 'A question must have options',
  emptyOptionValue: 'Option value cannot be empty',
  pastDate: 'Date and time must be in the future',
  endDateNotGreater: 'End Date must be greater than Start Date',
  registrationCloseDateNotGreater:
    'Registration Close Date must be greater than Registration Open Date',
  numberOfQuestionOptions: `A question must have exactly ${numberOfQuestionOptions} options`,
  fullMarkGreater: 'Full Mark must be greater than Pass Mark',
};
