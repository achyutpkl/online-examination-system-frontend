import { sortBy } from 'lodash';

import { getTrimmedIfString } from '../../../utils/stringUtils';
import { formValidationMessages, numberOfQuestionOptions } from './constants';

export const getQuestionValidationMessage = (question = {}) => {
  if (!getTrimmedIfString(question.title)) {
    return formValidationMessages.noQuestionTitle;
  }

  if (!getTrimmedIfString(question.correctAnswer)) {
    return formValidationMessages.noCorrectAnswer;
  }

  const options = question.options || {};

  const optionKeys = Object.keys(options);

  if (!optionKeys.length) {
    return formValidationMessages.noOptions;
  }

  if (optionKeys.length !== numberOfQuestionOptions) {
    return formValidationMessages.numberOfQuestionOptions;
  }

  for (let optionKey of optionKeys) {
    const optionValue = getTrimmedIfString(options[optionKey]);

    if (
      optionValue === '' ||
      optionValue === null ||
      optionValue === undefined
    ) {
      return formValidationMessages.emptyOptionValue;
    }
  }

  return '';
};

export const getSortedExamSections = (sections = [], questions = 'questions') =>
  sortBy(sections, 'ordering').map(props => ({
    ...props,
    [questions]: sortBy(props[questions] || [], 'questionNo'),
  }));
