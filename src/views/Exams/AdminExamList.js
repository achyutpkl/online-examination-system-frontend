import React, { Component } from 'react';
import { Card, CardTitle } from 'reactstrap';

import ExamList from '../StudentExam/commons/ExamList';

// TODO: May be move this, ExamList and ExamListItem in commons folder and merge this and ExamList into one component
export default class AdminExamList extends Component {
  render() {
    return (
      <div>
        <Card body>
          <CardTitle>Exams</CardTitle>
          <ExamList />
        </Card>
      </div>
    );
  }
}
