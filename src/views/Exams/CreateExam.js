import React from 'react';

import './style.css';

import ExamForm from './commons/ExamForm';
import ConfirmModal from '../../commons/ConfirmModal/ConfirmModal';

export default props => (
  <ConfirmModal>
    {confirmModalProps => (
      <ExamForm isEdit={false} {...confirmModalProps} {...props} />
    )}
  </ConfirmModal>
);
