import React, { Component } from 'react';
import { Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap';
import classnames from 'classnames';

import ExamForm from './commons/ExamForm';
import ConfirmModal from '../../commons/ConfirmModal/ConfirmModal';
import ExamResult from './ExamResult';

class EditExam extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1',
    };
  }
  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }
  render() {
    return (
      <div>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '1' })}
              onClick={() => {
                this.toggle('1');
              }}
            >
              Exam Details
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '2' })}
              onClick={() => {
                this.toggle('2');
              }}
            >
              Student Progress
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <div>
              <ConfirmModal>
                {confirmModalProps => (
                  <ExamForm isEdit {...confirmModalProps} {...this.props} />
                )}
              </ConfirmModal>
            </div>
          </TabPane>
          <TabPane tabId="2">
            <div>
              <ExamResult examId={this.props.match.params.id} />
            </div>
          </TabPane>
        </TabContent>
      </div>
    );
  }
}

export default EditExam;
