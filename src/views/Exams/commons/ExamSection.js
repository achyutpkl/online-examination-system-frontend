import React, { Component } from 'react';
import {
  Col,
  Card,
  CardHeader,
  Label,
  Input,
  Button,
  FormGroup,
} from 'reactstrap';

import Questions from '../../../commons/Questions/Questions';

export default class ExamSection extends Component {
  setQuestions = questions => {
    this.props.handlePropChange(this.props.id, {
      target: {
        name: 'questions',
        value: questions,
      },
    });
  };

  deleteAllQuestions = () => {
    const confirmationMessage = `Are you sure you want to remove all question from ${this
      .props.title || 'this section'}?`;

    this.props.showConfirmModal(confirmationMessage, () =>
      this.setQuestions([]),
    );
  };

  handlePropChange = event => {
    this.props.handlePropChange(this.props.id, event);
  };

  render() {
    const {
      id: sectionId,
      title,
      description,
      priority,
      questions,
      negativeMarks,
      questionWeightage,
      deleteSection,
    } = this.props;

    return (
      <Card body>
        <CardHeader
          className="h2"
          style={{ marginBottom: '10px', borderBottom: 'none' }}
        >
          <span
            style={{
              width: '65%',
              display: 'inline-block',
              whiteSpace: 'nowrap',
              overflow: 'hidden',
              textOverflow: 'ellipsis',
            }}
          >
            {title || 'New Section'}
          </span>
          <div className="card-header-actions">
            {/* eslint-disable-next-line */}
            <a
              className="card-header-action btn btn-close"
              onClick={() => deleteSection(sectionId)}
            >
              <i className="icon-close deleteButton" />
            </a>
          </div>
        </CardHeader>

        <FormGroup row>
          <Label for={`${sectionId}-sectionTitle`} sm={2}>
            Title*
          </Label>
          <Col sm={{ size: 10 }}>
            <Input
              type="text"
              name="title"
              required
              id={`${sectionId}-sectionTitle`}
              value={title}
              onChange={this.handlePropChange}
            />
          </Col>
        </FormGroup>

        <FormGroup row>
          <Label for={`${sectionId}-sectionDescription`} sm={2}>
            Description
          </Label>
          <Col sm={{ size: 10 }}>
            <Input
              type="text"
              name="description"
              id={`${sectionId}-sectionDescription`}
              value={description}
              onChange={this.handlePropChange}
            />
          </Col>
        </FormGroup>

        <FormGroup row>
          <Label sm="6">
            Question Weightage <em>(for one question)</em>
          </Label>
          <Col sm="6">
            <Input
              type="number"
              min="0"
              step="any"
              name="questionWeightage"
              id={`${sectionId}-questionWeightage`}
              value={questionWeightage}
              onChange={this.handlePropChange}
            />
          </Col>
        </FormGroup>

        <FormGroup row>
          <Label sm="6">Negative Marking</Label>
          <Col sm="6">
            <Input
              type="number"
              min="0"
              step="any"
              name="negativeMarks"
              id={`${sectionId}-negativeMarks`}
              value={negativeMarks}
              onChange={this.handlePropChange}
            />
          </Col>
        </FormGroup>

        <FormGroup row>
          <Label sm="6">Priority</Label>
          <Col sm="6">
            <Input
              type="number"
              min="1"
              step="any"
              name="priority"
              id={`${sectionId}-priority`}
              value={priority}
              onChange={this.handlePropChange}
            />
          </Col>
        </FormGroup>

        <hr />

        <div>
          {questions && questions.length ? (
            <div className="questionTitleWrp">
              <span className="h3">Questions {title && 'for ' + title}</span>
              <Button
                type="reset"
                size="sm"
                color="danger"
                onClick={this.deleteAllQuestions}
              >
                <i className="fa fa-ban" />
                Delete All Questions
              </Button>
            </div>
          ) : null}

          <Questions
            openModal={this.props.openModal}
            parentId={sectionId}
            questions={questions}
            setQuestions={this.setQuestions}
          />
        </div>
      </Card>
    );
  }
}
