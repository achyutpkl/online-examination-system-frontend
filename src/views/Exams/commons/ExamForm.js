import React from 'react';
import { toast } from 'react-toastify';
import { Row, Col, Card, CardBody, Form, Button } from 'reactstrap';

import browserHistory from '../../../utils/history';
import { viewRoutes } from '../../../utils/constants';
import { SUCCESS_DELETE_MESSAGE } from '../../../lang/en';
import { getTrimmedIfString } from '../../../utils/stringUtils';
import { editItemById } from '../../../utils/stateModifications';
import { showApiErrorMessage, HTTP_STATUS } from '../../../utils/httpUtil';
import { getDateObjectOrNull, isPastDateTime } from '../../../utils/dateUtils';
import {
  formValidationMessages,
  sectionTitlePlaceholder,
} from '../utils/constants';
import {
  getSortedExamSections,
  getQuestionValidationMessage,
} from '../utils/utils';
import {
  addExam,
  editExam,
  getExamTypes,
  getExamDetails,
  publishExamResults,
  deleteExam,
} from '../../../services/ExamService';

import ExamConfig from './ExamConfig';
import ExamSection from './ExamSection';
import ExamFormHeader from './ExamFormHeader';
import ConfirmPopup from '../../../commons/ConfirmPopup/ConfirmPopup';
import GotoResourceButton from '../../../commons/GotoResourceButton/GotoResourceButton';

const NEW_SECTION_ID = '**New_Section**';

class ExamForm extends React.Component {
  constructor() {
    super();

    this.state = this.getInitialState();
  }

  getInitialState = () => {
    return {
      isSaving: false,
      isPublishing: false,
      isDetailsLoading: false,
      title: '',
      examTypes: [],
      description: '',
      timeLimit: 180,
      examStatus: 'UPCOMING',
      passMark: 40,
      examTypeId: '',
      startTime: null,
      endTime: null,
      registrationOpenDate: null,
      registrationCloseDate: null,
      examSections: [],
      isDeleting: false,
    };
  };

  componentDidMount() {
    this.fetchExamTypes();

    if (this.props.isEdit) {
      const { id } = (this.props.match && this.props.match.params) || {};

      this.fetchExamDetails(id);
    }
  }

  fetchExamDetails = async id => {
    if (!id) {
      return;
    }

    this.setState({ isDetailsLoading: true });

    const examDetails =
      (await getExamDetails(id).catch(showApiErrorMessage)) || {};

    // Extract examTypeId for use in ExamConfig
    const examTypeId = examDetails.examType && examDetails.examType.id;

    // TODO: Remove conversion of date after issue is fixed in backend
    const modifiedDates = {
      startTime: getDateObjectOrNull(examDetails.startTime),
      endTime: getDateObjectOrNull(examDetails.endTime),
      registrationCloseDate: getDateObjectOrNull(
        examDetails.registrationCloseDate,
      ),
      registrationOpenDate: getDateObjectOrNull(
        examDetails.registrationOpenDate,
      ),
    };

    const sortedExamSections = getSortedExamSections(examDetails.examSections);

    this.setState({
      ...examDetails,
      ...modifiedDates,
      examSections: sortedExamSections,
      examTypeId,
      isDetailsLoading: false,
    });
  };

  fetchExamTypes = async () => {
    const examTypes = (await getExamTypes().catch(showApiErrorMessage)) || [];

    this.setState({ examTypes });
  };

  handleInvalidSubmit = validationMessage => {
    toast.error(validationMessage);
  };

  handleSubmitSuccess = () => {
    let successMessage = 'Successfully created new exam';

    if (this.props.isEdit) {
      successMessage = 'Exam has been edited';
    }

    toast.success(successMessage, {
      position: toast.POSITION.TOP_CENTER,
    });

    if (this.props.isEdit) {
      browserHistory.push(viewRoutes.adminExamList);

      return;
    }

    this.clearOldFormValues();
  };

  clearOldFormValues = () => {
    const examTypes = this.state.examTypes;

    this.setState(() => this.getInitialState());

    // restore examTypes
    this.setState({ examTypes });
  };

  handleExamPropChange = event => {
    const { name, value } = (event && event.target) || {};

    this.setState({ [name]: value });
  };

  getNewSectionData = () => {
    return {
      id: `${NEW_SECTION_ID}-${String(Math.random())}`,
      title: '',
      description: '',
      questionTimeLimit: 0,
      questionWeightage: 1,
      negativeMarks: 0,
      questions: [],
      priority: 1,
    };
  };

  addNewSection = () => {
    this.setState(prevState => ({
      ...prevState,
      examSections: [...prevState.examSections, this.getNewSectionData()],
    }));
  };

  deleteSection = sectionId => {
    const matchingSection = (this.state.examSections || []).find(
      ({ id }) => id === sectionId,
    );

    if (!matchingSection) {
      return;
    }

    const confirmationMessage = `Are you sure you want to delete ${matchingSection.title ||
      'this section'}?`;

    this.showConfirmModal(confirmationMessage, () =>
      this.setState({
        examSections: this.state.examSections.filter(
          ({ id }) => id !== sectionId,
        ),
      }),
    );
  };

  showConfirmModal = (message, confirmationCallback) => {
    this.props.openModal({
      message,
      handleClose: ({ isConfirmed }) => {
        if (!isConfirmed) {
          return;
        }

        confirmationCallback();
      },
    });
  };

  handleSectionPropChange = (id, event) => {
    const { name, value } = (event && event.target) || {};

    this.setState({
      examSections: editItemById(this.state.examSections, {
        id,
        [name]: value,
      }),
    });
  };

  // eslint-disable-next-line complexity
  getExamDatesValidationMessage = () => {
    const {
      startTime,
      endTime,
      registrationOpenDate,
      registrationCloseDate,
    } = this.state;

    // if not in edit mode check if past date for all dates
    if (!this.props.isEdit) {
      if (
        isPastDateTime(startTime) ||
        isPastDateTime(endTime) ||
        isPastDateTime(registrationOpenDate) ||
        isPastDateTime(registrationCloseDate)
      ) {
        return formValidationMessages.pastDate;
      }
    }

    if (startTime && endTime && new Date(startTime) >= new Date(endTime)) {
      return formValidationMessages.endDateNotGreater;
    }

    if (
      registrationOpenDate &&
      registrationCloseDate &&
      new Date(registrationOpenDate) >= new Date(registrationCloseDate)
    ) {
      return formValidationMessages.registrationCloseDateNotGreater;
    }

    return '';
  };

  getExamPropsValidationMessage = () => {
    const { title, examTypeId, startTime, endTime, timeLimit } = this.state;

    if (!(title && examTypeId && timeLimit && startTime && endTime)) {
      return formValidationMessages.default;
    }

    return this.getExamDatesValidationMessage();
  };

  getExamSectionValidationMessage(sectionData) {
    if (!sectionData) {
      return formValidationMessages.noSectionData;
    }

    const { title, questions } = sectionData;

    if (!getTrimmedIfString(title)) {
      return formValidationMessages.noSectionTitle;
    }

    if (!(questions && questions.length)) {
      return formValidationMessages.noSectionQuestions.replace(
        sectionTitlePlaceholder,
        title,
      );
    }

    for (const question of questions) {
      const validationMessage = getQuestionValidationMessage(question);

      if (validationMessage) {
        return validationMessage;
      }
    }

    return '';
  }

  getExamFormValidationMessage = () => {
    const examPropsValidationMessage = this.getExamPropsValidationMessage();

    if (examPropsValidationMessage) {
      return examPropsValidationMessage;
    }

    const { examSections } = this.state;

    if (!(examSections && examSections.length)) {
      return '';
    }

    // check validaty of all exam sections
    for (let section of examSections) {
      const validationMessage = this.getExamSectionValidationMessage(section);

      if (validationMessage) {
        return validationMessage;
      }
    }

    if (this.getFullMark() <= Number(this.state.passMark)) {
      return formValidationMessages.fullMarkGreater;
    }

    return '';
  };

  getFormattedExamDataForNewExam = () => ({
    ...this.state,
    fullMark: this.getFullMark(),
    examSections: this.state.examSections.map(
      ({ id, questions, ...otherProps }, index) => ({
        ...otherProps,
        ordering: index + 1,
        totalQuestions: questions.length,
        questions: (questions || []).map(({ id, ...questionProps }, index) => ({
          ...questionProps,
          questionNo: index + 1,
        })),
      }),
    ),
  });

  getFormattedExamDataForEditExam = () => {
    const {
      examType,
      examTypes,
      isDetailsLoading,
      isSaving,
      examSections,
      ...examDetails
    } = this.state;

    return {
      ...examDetails,
      fullMark: this.getFullMark(),
      examSections: examSections.map(
        ({ id = '', questions, ...otherProps }, index) => {
          const formattedSection = {
            ...otherProps,
            ordering: index + 1,
            totalQuestions: questions.length,
            questions: questions.map((questionProps, index) => ({
              ...questionProps,
              questionNo: index + 1,
            })),
          };

          if (!(id && String(id).includes(NEW_SECTION_ID))) {
            formattedSection.id = id;
          }

          return formattedSection;
        },
      ),
    };
  };

  handleSubmit = async e => {
    e.preventDefault();

    if (this.state.isSaving) {
      return;
    }

    const validationMessage = this.getExamFormValidationMessage();

    if (validationMessage) {
      return this.handleInvalidSubmit(validationMessage);
    }

    this.setState({ isSaving: true });

    let submitRequest;

    if (this.props.isEdit) {
      const examDetails = this.getFormattedExamDataForEditExam();
      submitRequest = editExam(examDetails);
    } else {
      const examDetails = this.getFormattedExamDataForNewExam();
      submitRequest = addExam(examDetails);
    }

    let apiErrorOccurred = false;

    await submitRequest.catch(error => {
      showApiErrorMessage(error);

      apiErrorOccurred = true;
    });

    this.setState({ isSaving: false });

    if (apiErrorOccurred) {
      return;
    }

    this.handleSubmitSuccess();
  };

  handleExamPublish = async () => {
    if (this.state.isPublishing) {
      return;
    }

    this.setState({ isPublishing: true });

    await publishExamResults(this.state.id)
      .then(() => {
        this.setState({ publishResult: true });

        const successMessage = 'Exam results have been published';

        toast.success(successMessage, {
          position: toast.POSITION.TOP_CENTER,
        });
      })
      .catch(showApiErrorMessage);

    this.setState({ isPublishing: false });
  };

  /**
   * By changing State-isDeleting to true modal confirmation to delete is pop up.
   *
   * @memberof ExamForm
   * @returns {null} Returns null.
   */
  handleOnClickDelete = () => {
    this.setState({
      isDeleting: true,
    });
  };

  handleDeleteConfirm = async () => {
    if (this.props.isEdit) {
      try {
        const { id } = (this.props.match && this.props.match.params) || {};
        const response = await deleteExam(id);

        if (response.status === HTTP_STATUS.OK) {
          toast.success(SUCCESS_DELETE_MESSAGE, {
            position: toast.POSITION.TOP_CENTER,
          });

          if (this.props.isEdit) {
            browserHistory.push(viewRoutes.adminExamList);

            return;
          }
        }
      } catch (err) {
        showApiErrorMessage(err);
      } finally {
        this.cancelDelete();
      }
    }
  };

  cancelDelete = () => {
    this.setState({
      isDeleting: false,
    });
  };

  getFullMark = () => {
    const { examSections } = this.state;

    return (examSections || []).reduce(
      (totalMarks, { questionWeightage, questions = [] }) =>
        totalMarks + Number(questionWeightage) * questions.length,
      0,
    );
  };

  renderPublishSection = () => {
    const { examStatus, publishResult } = this.state;

    if (!this.props.isEdit) {
      return null;
    }

    return (
      <div className="d-flex justify-content-between mt-1 mb-2">
        <div>Status: {examStatus}</div>
        {publishResult ? (
          'Results for this exam have been published'
        ) : (
          <div>
            <Button
              type="button"
              color="success"
              className="btn btn-block"
              onClick={this.handleExamPublish}
            >
              Publish Results
            </Button>
          </div>
        )}
      </div>
    );
  };

  render() {
    const { isEdit } = this.props;

    const {
      isSaving,
      title,
      isDeleting,
      description,
      examStatus,
      examSections,
      publishResult,
      ...examConfig
    } = this.state;

    return (
      <Form onSubmit={this.handleSubmit}>
        {this.renderPublishSection()}
        {isDeleting && (
          <ConfirmPopup
            handleCancel={this.cancelDelete}
            handleConfirm={this.handleDeleteConfirm}
          />
        )}
        <Row>
          <Col sm="8">
            <Card>
              <CardBody>
                <ExamFormHeader
                  title={title}
                  description={description}
                  handleChange={this.handleExamPropChange}
                />

                <div>
                  {examSections &&
                    examSections.map(section => (
                      <ExamSection
                        key={section.id}
                        {...section}
                        isEdit={isEdit}
                        openModal={this.props.openModal}
                        showConfirmModal={this.showConfirmModal}
                        handlePropChange={this.handleSectionPropChange}
                        deleteSection={this.deleteSection}
                      />
                    ))}
                </div>
              </CardBody>
            </Card>

            <div className="d-flex addMoreButton sectionAdd">
              <Button color="primary" onClick={this.addNewSection}>
                Add new section
              </Button>
            </div>
          </Col>

          <Col sm="4">
            <ExamConfig
              {...examConfig}
              fullMark={this.getFullMark()}
              isEdit={this.props.isEdit}
              handleExamPropChange={this.handleExamPropChange}
            />

            <GotoResourceButton
              name="Get question template"
              className="mb-2 mt-n2"
            />

            {isEdit && (
              <Button
                color="danger"
                className="btn btn-block"
                onClick={this.handleOnClickDelete}
              >
                Delete Exam
              </Button>
            )}
          </Col>
        </Row>

        <Row>
          <Col xs="12" sm="12" md="8" lg="8" className="saveButton">
            <Button
              disabled={isSaving}
              type="submit"
              color="success"
              className="btn btn-block"
            >
              {isEdit ? 'Update Exam Details' : 'Save Exam'}
            </Button>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default ExamForm;
