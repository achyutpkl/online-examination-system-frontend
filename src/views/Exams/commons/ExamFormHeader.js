import React, { Fragment } from 'react';
import { FormGroup, Label, Col, Input } from 'reactstrap';

export default ({ title, description, handleChange }) => (
  <Fragment>
    <FormGroup row>
      <Label for="examTitle" sm={2}>
        Exam Title*
      </Label>
      <Col sm={10}>
        <Input
          type="text"
          name="title"
          id="examTitle"
          value={title}
          required
          onChange={handleChange}
        />
      </Col>
    </FormGroup>

    <FormGroup row>
      <Label for="examDesc" sm={2}>
        Description
      </Label>
      <Col sm={10}>
        <Input
          type="text"
          name="description"
          id="examDesc"
          value={description}
          onChange={handleChange}
        />
      </Col>
    </FormGroup>
  </Fragment>
);
