import React from 'react';
import DatePicker from 'react-datepicker';

import {
  isToday,
  getEndOfDay,
  getStartOfDay,
  getDateObjectOrNull,
} from '../../../utils/dateUtils';

const getMinTime = selectedDate => {
  // if date is not selected assume that it is today
  if (!selectedDate || isToday(selectedDate)) {
    return new Date();
  }

  return getStartOfDay();
};

export default ({
  propName,
  selectedDate,
  required,
  disabled,
  handleExamPropChange,
}) => (
  <DatePicker
    autoComplete="off"
    required={required}
    disabled={disabled}
    name={propName}
    selected={getDateObjectOrNull(selectedDate)}
    onChange={newTime =>
      handleExamPropChange({
        target: { name: propName, value: newTime },
      })
    }
    showTimeSelect
    timeIntervals={15}
    dateFormat="MMMM d, yyyy h:mm aa"
    timeCaption="time"
    minDate={new Date()}
    minTime={getMinTime(selectedDate)}
    maxTime={getEndOfDay()}
  />
);
