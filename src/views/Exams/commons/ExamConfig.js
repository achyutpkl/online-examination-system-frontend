import React from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Label,
  Input,
  FormGroup,
} from 'reactstrap';

import { isPastDateTime } from '../../../utils/dateUtils';

import ExamDatePicker from './ExamDatePicker';
import HelpButton from '../../../commons/HelpButton/HelpButton';

const datePickerIsDisabled = (isEdit, selectedDate) => {
  if (!isEdit && selectedDate) {
    return false;
  }

  return isPastDateTime(selectedDate);
};

export default ({
  isEdit,
  startTime,
  endTime,
  examTypeId,
  examTypes,
  fullMark,
  passMark,
  timeLimit,
  registrationOpenDate,
  registrationCloseDate,
  handleExamPropChange,
}) => (
  <Card>
    <CardHeader>Exam Config</CardHeader>
    <CardBody>
      <FormGroup>
        <Label>
          Enrollment Start Date*
          <HelpButton
            className="ml-1"
            id="enrollment-start-date"
            message="Date from which student can give(enroll) exam."
            placement="top"
          />
        </Label>

        <div>
          <ExamDatePicker
            disabled={datePickerIsDisabled(isEdit, startTime)}
            propName="startTime"
            required
            selectedDate={startTime}
            handleExamPropChange={handleExamPropChange}
          />
        </div>
      </FormGroup>

      <FormGroup>
        <Label>
          Enrollment End Date*
          <HelpButton
            className="ml-1"
            id="enrollment-end-date"
            message="Date upto which student can give(enroll) exam. After this date student will not see this exam in list of exams."
            placement="top"
          />
        </Label>

        <div>
          <ExamDatePicker
            disabled={datePickerIsDisabled(isEdit, startTime)}
            propName="endTime"
            required
            selectedDate={endTime}
            handleExamPropChange={handleExamPropChange}
          />
        </div>
      </FormGroup>

      <FormGroup>
        <Label>
          Registration Open Date*
          <HelpButton
            className="ml-1"
            id="enrollment-open-date"
            message="Date from which student can view this exam on dashboard and is able to subscribe to the exam if date is before Enrollment Start Date."
            placement="top"
          />
        </Label>

        <div>
          <ExamDatePicker
            required
            disabled={datePickerIsDisabled(isEdit, startTime)}
            propName="registrationOpenDate"
            selectedDate={registrationOpenDate}
            handleExamPropChange={handleExamPropChange}
          />
        </div>
      </FormGroup>

      <FormGroup>
        <Label>
          Registration Close Date*
          <HelpButton
            className="ml-1"
            id="enrollment-close-date"
            message="Date after which student cannot subscribe to the exam."
            placement="top"
          />
        </Label>

        <div>
          <ExamDatePicker
            required
            disabled={datePickerIsDisabled(isEdit, startTime)}
            propName="registrationCloseDate"
            selectedDate={registrationCloseDate}
            handleExamPropChange={handleExamPropChange}
          />
        </div>
      </FormGroup>

      <FormGroup>
        <Label>Time Limit (In Minutes)*</Label>
        <Input
          placeholder="Time Limit"
          required
          type="number"
          name="timeLimit"
          min="0"
          step="any"
          value={timeLimit}
          onChange={handleExamPropChange}
        />
      </FormGroup>

      <FormGroup>
        <Label>
          Full Mark*
          <HelpButton
            className="ml-1"
            id="full-mark"
            message="Full Marks will be automatically calculated from question weightage and no. of questions of a section"
            placement="top"
          />
        </Label>
        <Input
          placeholder="Full Mark"
          required
          type="number"
          name="fullMark"
          min="0"
          step="any"
          value={fullMark}
          disabled
          onChange={handleExamPropChange}
        />
      </FormGroup>

      <FormGroup>
        <Label>Pass Mark</Label>
        <Input
          placeholder="Pass Mark"
          type="number"
          name="passMark"
          min="0"
          step="any"
          value={passMark}
          onChange={handleExamPropChange}
        />
      </FormGroup>

      <FormGroup>
        <Label htmlFor="exam-type-select">Exam Type*</Label>
        <Input
          type="select"
          required
          id="exam-type-select"
          name="examTypeId"
          value={examTypeId}
          onChange={handleExamPropChange}
        >
          <option value={''} disabled hidden>
            Select Exam Type
          </option>
          {examTypes &&
            examTypes.map(type => (
              <option key={type.id} value={type.id}>
                {type.title}
              </option>
            ))}
        </Input>
      </FormGroup>
    </CardBody>
  </Card>
);
