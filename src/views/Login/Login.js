import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import {
  Col,
  Row,
  Form,
  Card,
  Input,
  Button,
  CardBody,
  Container,
  CardGroup,
  CardFooter,
  InputGroup,
  InputGroupText,
  InputGroupAddon,
  CardHeader,
} from 'reactstrap';

import './index.css';
import { userRole, viewRoutes } from '../../utils/constants';

class Login extends Component {
  constructor(props) {
    super(props);

    this.handleLogin = this.handleLogin.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  state = {
    userName: '',
    password: '',
  };

  handleInputChange(event) {
    const { name, value } = event.target;

    this.setState({ [name]: value });
  }

  handleLogin(e) {
    e.preventDefault();

    // trim userName
    const userName = (this.state.userName || '').trim();

    this.props.handleLogin({ userName, password: this.state.password.trim() });
  }

  getDefaultRedirectPath() {
    const { roles } = this.props;

    if (!(roles && roles.length)) {
      return '/';
    }

    if (roles.includes(userRole.admin)) {
      return viewRoutes.admin;
    }

    if (roles.includes(userRole.student)) {
      return viewRoutes.student;
    }

    return '/';
  }

  render() {
    const { from } = (this.props.location && this.props.location.state) || {
      from: { pathname: this.getDefaultRedirectPath() },
    };

    if (this.props.isAuthenticated) {
      return <Redirect to={from} />;
    }

    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <CardGroup>
                <Card>
                  <CardHeader>
                    <div className="logo">
                      <div className="group-box">
                        <span className="entrance-cart">
                          <span className="absolute-color">ENTRANCE</span> CART
                        </span>
                        <br />
                        <span className="supported-by-name">
                          Supported by NAME
                        </span>
                      </div>
                    </div>
                  </CardHeader>
                  <CardBody>
                    <Form onSubmit={this.handleLogin}>
                      <h1>Login</h1>
                      <p className="text-muted">
                        Sign in with your entrance cart account
                      </p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="text"
                          placeholder="Email"
                          autoComplete="username"
                          name="userName"
                          value={this.state.userName}
                          onChange={this.handleInputChange}
                        />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="password"
                          placeholder="Password"
                          autoComplete="current-password"
                          name="password"
                          value={this.state.password}
                          onChange={this.handleInputChange}
                        />
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                          <Button
                            type="submit"
                            color="primary"
                            className="px-4"
                          >
                            Login
                          </Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          <Link to="/forgot-password">
                            <Button type="button" color="link" className="px-0">
                              Forgot password?
                            </Button>
                          </Link>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                  <CardFooter className="p-1">
                    <Row>
                      <Col xs="12" className="text-center">
                        <Link to="/register">
                          <Button type="button" color="link">
                            <span>Not registered? Register Now!</span>
                          </Button>
                        </Link>
                      </Col>
                    </Row>
                  </CardFooter>
                </Card>
              </CardGroup>
              <div className="link-to-entrance-cart">
                <a href="http://entrancecart.com">Go to Entrance Cart</a>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
