import { get } from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { replace } from 'connected-react-router';
import { ToastContainer, toast } from 'react-toastify';
import { Switch, Route, withRouter, Redirect } from 'react-router-dom';

import 'react-toastify/dist/ReactToastify.css';
import 'react-datepicker/dist/react-datepicker.css';

import './styles.css';
import { getItem, setItem, removeItem } from '../../utils/localStorage';
import {
  apiUrls,
  userRole,
  viewRoutes,
  localStorageKeys,
} from '../../utils/constants';
import { post, getHttpErrorMessage, axiosInstance } from '../../utils/httpUtil';

import Login from '../Login/Login';
import NotFound from '../NotFound/NotFound';
import Register from '../Register/Register';
import ResetPassword from '../ResetPassword/ResetPassword';
import AuthLayout from '../../commons/AuthLayout/AuthLayout';
import ForgotPassword from '../ForgotPassword/ForgotPassword';
import PrivateRoute from '../../commons/PrivateRoute/PrivateRoute';
import StudentPaymentSucces from '../StudentPayments/StudentPaymentSuccess';
import StudentPaymentFailure from '../StudentPayments/StudentPaymentFailure';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: getItem(localStorageKeys.oesUser),
      prevPath: '',
    };

    this.setAxiosInterceptor();
  }

  handleLogin = userInfo => {
    post(apiUrls.login, userInfo, { isLoginRequest: true })
      .then(response => {
        const responseData = (response && response.data) || {};

        setItem(localStorageKeys.oesUser, responseData);
        this.setState({ user: responseData });
      })
      .catch(error => {
        toast.error(getHttpErrorMessage(error), {
          position: toast.POSITION.BOTTOM_CENTER,
        });
      });
  };

  handleLogout = () => {
    // TODO: Call api to logout
    removeItem(localStorageKeys.oesUser);

    this.setState({ user: {} });
    // route to login and clear router location state
    this.props.replaceHistory('/login', undefined);
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.location !== this.props.location) {
      this.setState({ prevPath: this.props.location });
    }
  }

  setAxiosInterceptor = () => {
    axiosInstance.interceptors.response.use(
      response => response,
      error => {
        const errorStatusCode = error.response && error.response.status;

        const isLoginRequest = get(error, 'response.config.isLoginRequest');

        if (Number(errorStatusCode) === 401 && !isLoginRequest) {
          this.handleLogout();
          // We know that we should logout and there's no need to show error message. If in future
          // server returns appropriate message, and we want to display it, we won't need blockErrorToaser.
          error.blockErrorToaser = true;
        }

        return Promise.reject(error);
      },
    );
  };

  render() {
    const { accessToken, roles } = this.state.user || {};

    let homePath = '/login';

    if (roles && roles.includes(userRole.admin)) {
      homePath = '/admin';
    } else if (roles && roles.includes(userRole.student)) {
      homePath = '/student';
    }
    const { pathname } = this.props.location;

    return (
      <div className="app">
        <ToastContainer />
        <Switch>
          <Redirect exact from="/" to={homePath} />

          <Route
            path="/404"
            render={props => (
              <NotFound
                {...props}
                initialPath={
                  this.props.location.state
                    ? this.props.location.state.pathname
                    : pathname
                }
              />
            )}
          />
          <Route
            path="/login"
            render={props => (
              <Login
                {...props}
                roles={roles}
                isAuthenticated={accessToken}
                handleLogin={this.handleLogin}
              />
            )}
          />
          {/* TODO: Confirm if following two should be protected routes or not */}
          <Route
            path={viewRoutes.studentPaymentSuccess}
            render={props => <StudentPaymentSucces {...props} />}
          />
          <Route
            path={viewRoutes.studentPaymentFailure}
            render={props => <StudentPaymentFailure {...props} />}
          />
          <PrivateRoute
            isAllowed={!accessToken}
            path="/register"
            component={Register}
            redirectPath={homePath}
          />
          <PrivateRoute
            isAllowed={!accessToken}
            path="/forgot-password"
            component={ForgotPassword}
            redirectPath={homePath}
          />
          <PrivateRoute
            isAllowed={!accessToken}
            path="/reset-password"
            component={ResetPassword}
            redirectPath={homePath}
          />

          <PrivateRoute
            isAllowed={accessToken}
            path="/admin"
            component={AuthLayout}
            componentProps={{ roles, handleLogout: this.handleLogout }}
          />

          <PrivateRoute
            isAllowed={accessToken}
            path="/student"
            component={AuthLayout}
            componentProps={{ roles, handleLogout: this.handleLogout }}
          />

          <Redirect
            to={{
              pathname: '/404',
              state: { pathname },
            }}
          />
        </Switch>
      </div>
    );
  }
}

export default withRouter(
  connect(
    null,
    { replaceHistory: replace },
  )(App),
);
