import React from 'react';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';

import './style.css';

/**
 * Render not found view
 * @param {*} props Props.
 * @returns {Node} React node containing not found view
 */
export default function NotFound(props) {
  return (
    <div className="wrapper">
      <h1 className="httpStatus">404</h1>
      <h4 className="statusText">
        Page you searched for
        <br /> {props.initialPath}
        <br />
        doesn't exist.
      </h4>
      <Link to="/">
        <Button color="primary">Home</Button>
      </Link>
    </div>
  );
}
