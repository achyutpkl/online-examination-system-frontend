import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import { sectionTypes } from './constants';

export default ({ type, item, handleInputChange }) => (
  <Form>
    <FormGroup>
      <Label htmlFor="form-item-title">Title*</Label>
      <Input
        type="text"
        id="form-item-title"
        name="title"
        placeholder="Enter Title.."
        value={item.title}
        onChange={handleInputChange}
      />
    </FormGroup>
    <FormGroup>
      <Label htmlFor="form-item-description">Description*</Label>
      <Input
        type="text"
        id="form-item-description"
        name="description"
        placeholder="Enter Description.."
        value={item.description}
        onChange={handleInputChange}
      />
    </FormGroup>
    {type === sectionTypes.subject ? (
      <React.Fragment>
        <FormGroup>
          <Label htmlFor="form-item-code">Subject Code*</Label>
          <Input
            type="text"
            id="form-item-code"
            name="subjectCode"
            placeholder="Enter Subject Code.."
            value={item.subjectCode}
            onChange={handleInputChange}
          />
        </FormGroup>
        <FormGroup>
          <Label for="form-item-color-code">Color Code</Label>
          <Input
            type="color"
            name="colorCode"
            id="form-item-color-code"
            placeholder="color placeholder"
            value={item.colorCode}
            onChange={handleInputChange}
          />
        </FormGroup>
      </React.Fragment>
    ) : null}
  </Form>
);
