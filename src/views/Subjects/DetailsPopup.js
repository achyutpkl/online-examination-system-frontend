import React from 'react';
import { Modal, ModalBody, ModalHeader, ModalFooter, Button } from 'reactstrap';

import SubjectSectionForm from './SubjectSectionForm';

export default ({
  popupData: { popupHeader, isRequesting, mode, type, ...itemDetails },
  canSubmit,
  handleSubmit,
  handleCancel,
  handleInputChange,
  handleClose,
}) => (
  <Modal isOpen={true} backdrop="static" className={'modal-lg'}>
    <ModalHeader toggle={handleClose}>{popupHeader}</ModalHeader>
    <ModalBody>
      <SubjectSectionForm
        type={type}
        item={itemDetails}
        handleInputChange={handleInputChange}
      />
    </ModalBody>
    <ModalFooter>
      <Button
        color="primary"
        onClick={handleSubmit}
        disabled={!canSubmit || isRequesting}
      >
        {mode === 'edit' ? 'Edit' : 'Add'}
      </Button>
      <Button disabled={isRequesting} color="secondary" onClick={handleCancel}>
        {mode === 'edit' ? 'Cancel' : 'Reset'}
      </Button>
    </ModalFooter>
  </Modal>
);
