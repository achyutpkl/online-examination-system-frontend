import React from 'react';
import { Row, Col } from 'reactstrap';

import { sectionTypes } from './constants';
import browserHistory from '../../utils/history';
import { apiUrls, viewRoutes } from '../../utils/constants';
import { editItemById } from '../../utils/stateModifications';
import { post, put, del, showApiErrorMessage } from '../../utils/httpUtil';

import SectionList from './SectionList';
import DetailsPopup from './DetailsPopup';
import ConfirmPopup from '../../commons/ConfirmPopup/ConfirmPopup';

import {
  getDivisions,
  getSubDivisions,
  getSubjects,
} from '../../services/SubjectService';

const BLACK_COLOR = '#000000';

class Subjects extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isRequesting: false,
      deleteType: '',
      deleteItemId: '',
      selectedSubjectId: '',
      subjects: [],
      isSubjectsLoading: false,
      subjectFilter: '',
      divisions: [],
      divisionFilter: '',
      isDivisionsLoading: false,
      selectedDivisionId: '',
      subDivisions: [],
      subDivisionFilter: '',
      isSubDivisionsLoading: false,
      popupData: this.initialPopupData,
    };

    this.sections = [
      {
        type: sectionTypes.subject,
        selectedParentIdName: '',
        title: 'Subjects',
        activeIdName: 'selectedSubjectId',
        actionName: 'Add subject',
        isLoadingName: 'isSubjectsLoading',
        itemsName: 'subjects',
        filterValueName: 'subjectFilter',
        parentName: '',
      },
      {
        type: sectionTypes.division,
        selectedParentIdName: 'selectedSubjectId',
        title: 'Divisions',
        activeIdName: 'selectedDivisionId',
        actionName: 'Add division',
        isLoadingName: 'isDivisionsLoading',
        itemsName: 'divisions',
        filterValueName: 'divisionFilter',
        parentName: 'subject',
      },
      {
        type: sectionTypes.subDivision,
        selectedParentIdName: 'selectedDivisionId',
        title: 'Sub Divisions',
        activeIdName: '',
        actionName: 'Add sub-division',
        isLoadingName: 'isSubDivisionsLoading',
        itemsName: 'subDivisions',
        filterValueName: 'subDivisionFilter',
        parentName: 'division',
      },
    ];
  }

  get initialPopupData() {
    return {
      isOpen: false,
      popupHeader: '',
      mode: '',
      id: '',
      type: sectionTypes.subject,
      isRequesting: false,
      title: '',
      description: '',
      subjectCode: '',
      colorCode: BLACK_COLOR,
    };
  }

  get canSubmit() {
    const { title, type, description, subjectCode, colorCode } =
      (this.state && this.state.popupData) || {};

    if (!(title && description)) {
      return false;
    }

    if (type === sectionTypes.subject && !(subjectCode && colorCode)) {
      return false;
    }

    return true;
  }

  setPopupData(data) {
    this.setState({ popupData: { ...this.state.popupData, ...data } });
  }

  componentDidMount() {
    const { subjectId, divisionId, subDivisionId } = (this.props.location &&
      this.props.location.state) || {
      subjectId: '',
      divisionId: '',
      subDivisionId: '',
    };

    // TODO: May be extract this to parent component for reuse
    this.loadSubjectsDivisionsAndSubDivisions(
      subjectId,
      divisionId,
      subDivisionId,
    );
  }

  loadSubjectsDivisionsAndSubDivisions = async (
    subjectId,
    divisionId,
    subDivisionId,
  ) => {
    const subjects = await this.fetchSubjects();

    if (!(subjects && subjects.length)) {
      return;
    }

    const selectedSubjectId = subjectId || subjects[0].id;

    this.setState({ selectedSubjectId });

    const divisions = await this.fetchDivisions(selectedSubjectId);

    if (!(divisions && divisions.length)) {
      return;
    }

    const selectedDivisionId = divisionId || divisions[0].id;

    this.setState({ selectedDivisionId });

    const subDivisions = await this.fetchSubDivisions(selectedDivisionId);

    if (!(subDivisions && subDivisions.length)) {
      return;
    }

    const selectedSubDivisionId = subDivisionId || subDivisions[0].id;

    this.setState({ selectedSubDivisionId });
  };

  fetchSubjects = async () => {
    this.setState({ isSubjectsLoading: true });

    let { data } = await getSubjects().catch(showApiErrorMessage);

    this.setState({ subjects: data, isSubjectsLoading: false });

    return data;
  };

  fetchDivisions = async subjectId => {
    this.setState({ isDivisionsLoading: true });

    let { data } = (await getDivisions(subjectId).catch(
      showApiErrorMessage,
    )) || { data: [] };

    this.setState({ divisions: data, isDivisionsLoading: false });

    return data;
  };

  fetchSubDivisions = async divisionId => {
    this.setState({ isSubDivisionsLoading: true });

    let { data } = (await getSubDivisions(divisionId).catch(
      showApiErrorMessage,
    )) || { data: [] };

    this.setState({ subDivisions: data, isSubDivisionsLoading: false });

    return data;
  };

  handleInputChange = event => {
    const name = event.target.name;
    const value = event.target.value;

    this.setPopupData({ [name]: value });
  };

  handleSubmit = () => {
    // validate that all fields are present
    if (!this.canSubmit) {
      return;
    }

    const {
      id,
      title,
      description,
      subjectCode,
      colorCode,
      mode,
      type,
    } = this.state.popupData;

    let url, statePropName;
    const body = { title, description };

    if (type === sectionTypes.subject) {
      url = apiUrls.subject;
      statePropName = 'subjects';

      body.colorCode = colorCode;
      body.subjectCode = subjectCode;
    } else if (type === sectionTypes.division) {
      url = apiUrls.division;
      statePropName = 'divisions';

      // add subjectId
      body.subjectId = this.state.selectedSubjectId;
    } else {
      url = apiUrls.subDivision;
      statePropName = 'subDivisions';

      // add divisionId
      body.divisionId = this.state.selectedDivisionId;
    }

    this.setPopupData({ isRequesting: true });

    if (mode === 'edit') {
      // Add id if you want to edit
      body.id = id;
      url += `/${id}`;

      put(url, body)
        .then(response => {
          const editedItem = response && response.data;

          if (!editedItem) {
            throw new Error('Could not retrieve data after edit request');
          }

          this.setState(prevState => ({
            ...prevState,
            [statePropName]: editItemById(prevState[statePropName], editedItem),
          }));
        })
        .catch(showApiErrorMessage)
        .then(() => {
          this.closePopup();
        });
    } else {
      post(url, body)
        .then(response => {
          const newItem = response && response.data;

          if (!newItem) {
            throw new Error('Could not retreive new data from response');
          }

          this.setState(state => ({
            ...state,
            [statePropName]: [...state[statePropName], newItem],
          }));
        })
        .catch(showApiErrorMessage)
        .then(() => {
          this.closePopup();
        });
    }
  };

  showAddForm = type => {
    const header =
      type === sectionTypes.subject
        ? 'Add subject'
        : type === sectionTypes.division
        ? 'Add division'
        : 'Add Sub-division';

    this.setPopupData({ type, mode: 'add', isOpen: true, popupHeader: header });
  };

  handleSelect = (type, itemId) => {
    if (type === sectionTypes.subject) {
      if (itemId === this.state.selectedSubjectId) {
        return;
      }

      this.setState({
        selectedSubjectId: itemId,
        selectedDivisionId: '',
        divisions: [],
        subDivisions: [],
      });

      this.fetchDivisions(itemId);
    } else if (type === sectionTypes.division) {
      if (itemId === this.state.selectedDivisionId) {
        return;
      }

      this.setState({
        selectedDivisionId: itemId,
        subDivisions: [],
      });

      this.fetchSubDivisions(itemId);
    }
  };

  showDetails = (type, item) => {
    const header =
      type === sectionTypes.subject
        ? 'Edit subject'
        : type === sectionTypes.division
        ? 'Edit division'
        : 'Edit Sub-division';

    this.setPopupData({
      ...item,
      type,
      mode: 'edit',
      isOpen: true,
      popupHeader: header,
    });
  };

  closePopup = () => {
    this.setState({ popupData: this.initialPopupData });
  };

  handleCancel = () => {
    const { mode } = this.state.popupData;

    if (mode === 'edit') {
      return this.closePopup();
    }

    // reset popup input values
    return this.setPopupData({
      title: '',
      description: '',
      subjectCode: '',
      colorCode: BLACK_COLOR,
    });
  };

  handleDeleteClick = (type, itemId) => {
    this.setState({ deleteType: type, deleteItemId: itemId });
  };

  cancelDelete = () => {
    this.setState({ deleteType: '', deleteItemId: '' });
  };

  handleDeleteConfirm = () => {
    const type = this.state.deleteType;
    const itemId = this.state.deleteItemId;

    if (!(type && itemId)) {
      return;
    }

    this.setState({ isRequesting: true });

    let apiUrl, statePropName;

    if (type === sectionTypes.subject) {
      apiUrl = apiUrls.subject;
      statePropName = 'subjects';
    } else if (type === sectionTypes.division) {
      apiUrl = apiUrls.division;
      statePropName = 'divisions';
    } else if (type === sectionTypes.subDivision) {
      apiUrl = apiUrls.subDivision;
      statePropName = 'subDivisions';
    }

    del(`${apiUrl}/${itemId}`)
      .then(response => {
        this.setState({
          [statePropName]: (this.state[statePropName] || []).filter(
            ({ id }) => id !== itemId,
          ),
        });

        if (itemId === this.state.selectedSubjectId) {
          this.setState({
            divisions: [],
            subDivisions: [],
            selectedSubjectId: '',
            selectedDivisionId: '',
          });
        }

        if (itemId === this.state.selectedDivisionId) {
          this.setState({
            subDivisions: [],
            selectedDivisionId: '',
          });
        }
      })
      .catch(showApiErrorMessage)
      .then(() =>
        this.setState({
          isRequesting: false,
          deleteItemId: '',
          deleteType: '',
        }),
      );
  };

  addSubDivisionQuestion = (type, subDivisionId) => {
    if (type !== sectionTypes.subDivision) {
      return;
    }

    browserHistory.push(viewRoutes.addQuestions, {
      subjectId: this.state.selectedSubjectId,
      divisionId: this.state.selectedDivisionId,
      subDivisionId,
    });
  };

  viewSubDivisionQuestions = (type, subDivisionId) => {
    if (type !== sectionTypes.subDivision) {
      return;
    }

    browserHistory.push(viewRoutes.viewPracticeQuestions, {
      subjectId: this.state.selectedSubjectId,
      divisionId: this.state.selectedDivisionId,
      subDivisionId,
    });
  };

  handleFilterChange = (type, value) => {
    if (type === sectionTypes.subject) {
      this.setState({ subjectFilter: value });
    } else if (type === sectionTypes.division) {
      this.setState({ divisionFilter: value });
    } else if (type === sectionTypes.subDivision) {
      this.setState({ subDivisionFilter: value });
    }
  };

  render() {
    const { popupData, deleteType, deleteItemId, isRequesting } = this.state;

    return (
      <React.Fragment>
        {popupData && popupData.isOpen ? (
          <DetailsPopup
            popupData={popupData}
            canSubmit={this.canSubmit}
            handleClose={this.closePopup}
            handleSubmit={this.handleSubmit}
            handleCancel={this.handleCancel}
            handleInputChange={this.handleInputChange}
          />
        ) : null}

        {deleteType && deleteItemId ? (
          <ConfirmPopup
            isRequesting={isRequesting}
            handleCancel={this.cancelDelete}
            handleConfirm={this.handleDeleteConfirm}
          />
        ) : null}
        <Row>
          {this.sections.map(section => (
            <Col key={section.type}>
              {!section.selectedParentIdName ||
              this.state[section.selectedParentIdName] ? (
                <SectionList
                  type={section.type}
                  title={section.title}
                  actionName={section.actionName}
                  activeId={this.state[section.activeIdName]}
                  isLoading={this.state[section.isLoadingName]}
                  items={this.state[section.itemsName]}
                  filterValue={this.state[section.filterValueName]}
                  onFilterChange={this.handleFilterChange}
                  handleSelect={this.handleSelect}
                  showDetails={this.showDetails}
                  handleAdd={this.showAddForm}
                  handleDelete={this.handleDeleteClick}
                  addSubDivisionQuestion={this.addSubDivisionQuestion}
                  viewSubDivisionQuestions={this.viewSubDivisionQuestions}
                />
              ) : (
                <span>
                  {section.parentName ? `Select a ${section.parentName}` : null}
                </span>
              )}
            </Col>
          ))}
        </Row>
      </React.Fragment>
    );
  }
}

export default Subjects;
