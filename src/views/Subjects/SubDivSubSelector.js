import React, { PureComponent } from 'react';
import { FormGroup, Label, Input, Row, Col } from 'reactstrap';
import PropTypes from 'prop-types';

class SubDivSubSelector extends PureComponent {
  state = {
    subject: [],
    division: [],
    subjectId: '',
    subDivision: [],
    divisionId: '',
    subDivisionId: '',
  };

  render() {
    return (
      <Row>
        <Col xs="4">
          <FormGroup>
            <Label for="subject">Subject</Label>
            <Input
              type="select"
              name="subjectId"
              id="subject"
              value={this.state.subjectId}
              onChange={this._handleOnChange}
            >
              <option value="1">Physic</option>
              <option value="2">Chemistry</option>
              <option value="3">Math</option>
              <option value="4">Drawing</option>
            </Input>
          </FormGroup>
        </Col>

        <Col xs="4">
          <FormGroup>
            <Label for="division">Division</Label>
            <Input
              type="select"
              name="divisionId"
              id="division"
              value={this.state.divisionId}
              onChange={this._handleOnChange}
            >
              <option value="101">Heat</option>
              <option value="102">Light</option>
              <option value="103">Mechanics</option>
              <option value="104">Viscocity</option>
            </Input>
          </FormGroup>
        </Col>

        <Col xs="4">
          <FormGroup>
            <Label for="subDivision">Sub Division</Label>
            <Input
              type="select"
              name="subDivisionId"
              id="subDivision"
              value={this.state.subDivisionId}
              onChange={this._handleOnChange}
            >
              <option value="201">Projectile Motion</option>
              <option value="202">Friction</option>
              <option value="203">Wave Length</option>
              <option value="204">I don't know</option>
            </Input>
          </FormGroup>
        </Col>
      </Row>
    );
  }

  _handleOnChange = e => {
    e.preventDefault();

    this.setState({ [e.target.name]: e.target.value }, () => {
      this.props.onChange({
        subject: this.state.subjectId,
        division: this.state.divisionId,
        subDivision: this.state.subDivisionId,
      });
    });
  };
}

SubDivSubSelector.propTypes = {
  onChange: PropTypes.func.isRequired,
};

export default SubDivSubSelector;
