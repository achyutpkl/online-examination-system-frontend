import React, { Fragment } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
  Button,
  ListGroup,
  ListGroupItem,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from 'reactstrap';

import { sectionTypes } from './constants';

export default ({
  type,
  title,
  items,
  isLoading,
  actionName,
  handleAdd,
  activeId,
  filterValue,
  onFilterChange,
  handleSelect,
  handleDelete,
  showDetails,
  addSubDivisionQuestion,
  viewSubDivisionQuestions,
}) => (
  <Card>
    <CardHeader>
      <div> {title} </div>
      <div>
        <InputGroup>
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className="fa fa-filter" />
            </InputGroupText>
          </InputGroupAddon>
          <Input
            type="text"
            id="input1-group1"
            value={filterValue}
            onChange={e => onFilterChange(type, e.target.value)}
            placeholder="Filter by text.."
          />
        </InputGroup>
      </div>
    </CardHeader>
    <CardBody>
      {isLoading ? (
        <span>Loading...</span>
      ) : (
        <ListGroup id="list-tab" role="tablist">
          {items &&
            items
              .filter(
                ({ title }) =>
                  title &&
                  title.toLowerCase().includes(filterValue.toLowerCase()),
              )
              .map(item => (
                <ListGroupItem
                  key={item.id}
                  onClick={() => handleSelect(type, item.id)}
                  action
                  active={item.id === activeId}
                  className="d-flex flex-row justify-content-between"
                >
                  <span>{item.title}</span>
                  <div>
                    <Button
                      size="sm"
                      title="Edit"
                      color="success"
                      className="mx-1"
                      onClick={e => {
                        e.stopPropagation();

                        showDetails(type, item);
                      }}
                    >
                      <i className="fa fa-edit" />
                    </Button>

                    {type === sectionTypes.subDivision ? (
                      <Fragment>
                        <Button
                          size="sm"
                          title="View questions"
                          color="primary"
                          className="mr-1"
                          onClick={e => {
                            e.stopPropagation();

                            viewSubDivisionQuestions(type, item.id);
                          }}
                        >
                          <i className="icon-eye" />
                        </Button>

                        <Button
                          size="sm"
                          title="Add questions"
                          color="info"
                          className="mr-1"
                          onClick={e => {
                            e.stopPropagation();

                            addSubDivisionQuestion(type, item.id);
                          }}
                        >
                          <i className="fa fa-plus" />
                        </Button>
                      </Fragment>
                    ) : null}

                    <Button
                      size="sm"
                      title="Delete"
                      color="danger"
                      onClick={e => {
                        e.stopPropagation();

                        handleDelete(type, item.id);
                      }}
                    >
                      <i className="fa fa-remove" />
                    </Button>
                    {item.id === activeId ? (
                      <span>
                        <i className="fa fa-chevron-right fa-lg ml-1" />
                      </span>
                    ) : null}
                  </div>
                </ListGroupItem>
              ))}
        </ListGroup>
      )}
    </CardBody>
    <CardFooter>
      <Button color="primary" onClick={() => handleAdd(type)}>
        {actionName}
      </Button>
    </CardFooter>
  </Card>
);
