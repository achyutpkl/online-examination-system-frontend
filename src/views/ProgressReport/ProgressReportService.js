import { apiUrls } from '../../utils/constants';
import { get } from '../../utils/httpUtil';

export const getExamReports = async () => {
  let examReports = await get(`${apiUrls.examReports}`);

  return (examReports && examReports.data) || {};
};
