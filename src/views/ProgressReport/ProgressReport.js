import React from 'react';
import { Line } from 'react-chartjs-2';

import { getExamReports } from './ProgressReportService';
import { showApiErrorMessage } from '../../utils/httpUtil';

import './style.css';

class ProgressReport extends React.Component {
  constructor(props) {
    super(props);
    this.chart = null;
  }

  state = {
    xAxisLabels: [],
    studentScoreDataPoints: [],
    highestScoreDataPoints: [],
    averageScoreDataPoints: [],
  };

  componentDidMount() {
    getExamReports()
      .then(res => {
        let xAxisLabels = [];
        let studentScoreDataPoints = [];
        let highestScoreDataPoints = [];
        let averageScoreDataPoints = [];

        ((res && res.data) || []).forEach((item, index) => {
          xAxisLabels.push(item.examDetail.startTime);
          studentScoreDataPoints.push({
            x: index,
            y: item.studentScore,
            examType: item.examDetail.title,
          });
          highestScoreDataPoints.push({
            x: index,
            y: item.highestScore,
            examType: item.examDetail.title,
          });
          averageScoreDataPoints.push({
            x: index,
            y: item.averageScore,
            examType: item.examDetail.title,
          });
        });
        this.setState({
          xAxisLabels,
          studentScoreDataPoints,
          averageScoreDataPoints,
          highestScoreDataPoints,
        });
      })
      .catch(showApiErrorMessage);
  }

  /**
   * Return style for char title with label argument.
   *
   * @memberof ProgressReport
   * @param {String} label Title to be kept in char title.
   * @returns {Object} Style property of label in chart title.
   */
  getStyleForChartTitle = label => {
    return {
      display: true,
      labelString: label,
      fontStyle: 'bold',
      fontSize: 16,
    };
  };

  /**
   * Return options for chart.
   *
   * @memberof ProgressReport
   * @returns {Object} Property of chart.
   */
  getOtherPropertiesForChart = () => {
    return {
      scales: {
        yAxes: [
          {
            scaleLabel: this.getStyleForChartTitle('Score'),
            ticks: {
              beginAtZero: true,
              max: 100,
            },
          },
        ],
        xAxes: [
          {
            scaleLabel: this.getStyleForChartTitle('Exam'),
          },
        ],
      },
      tooltips: {
        callbacks: {
          afterTitle: function(tooltipItem, data) {
            const currentData = data.datasets[tooltipItem[0].datasetIndex].data;

            return currentData[tooltipItem[0].index].examType;
          },
        },
      },
    };
  };

  getDataForChart = () => {
    const {
      xAxisLabels,
      studentScoreDataPoints,
      highestScoreDataPoints,
      averageScoreDataPoints,
    } = this.state;

    return {
      labels: xAxisLabels,
      datasets: [
        {
          label: 'Student Score',
          fill: false,
          lineTension: 0.1,
          backgroundColor: 'rgba(0,255,0,0.4)',
          borderColor: 'rgba(0,255,0,1)',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: 'rgba(0,255,0,1)',
          pointBackgroundColor: '#fff',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: 'rgba(0,255,0,1)',
          pointHoverBorderColor: 'rgba(220,220,220,1)',
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: studentScoreDataPoints,
        },
        {
          label: 'Highest Score',
          fill: false,
          lineTension: 0.1,
          backgroundColor: 'rgba(255,0,0,0.4)',
          borderColor: 'rgba(255,0,0,1)',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: 'rgba(255,0,0,1)',
          pointBackgroundColor: '#fff',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: 'rgba(255,0,0,1)',
          pointHoverBorderColor: 'rgba(220,220,220,1)',
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: highestScoreDataPoints,
        },
        {
          label: 'Average Score',
          fill: false,
          lineTension: 0.1,
          backgroundColor: 'rgba(0,0,255,0.4)',
          borderColor: 'rgba(0,0,255,1)',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: 'rgba(0,0,255,1)',
          pointBackgroundColor: '#fff',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: 'rgba(0,0,255,1)',
          pointHoverBorderColor: 'rgba(220,220,220,1)',
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: averageScoreDataPoints,
        },
      ],
    };
  };

  render() {
    return (
      <div className="progress-report-container">
        <Line
          data={this.getDataForChart()}
          options={this.getOtherPropertiesForChart()}
        />
      </div>
    );
  }
}

export default ProgressReport;
