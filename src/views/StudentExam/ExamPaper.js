import _isEmpty from 'lodash/isEmpty';
import { toast } from 'react-toastify';
import { Prompt } from 'react-router-dom';
import React, { PureComponent } from 'react';
import { differenceInSeconds } from 'date-fns';
import { Row, Col, Card, CardHeader, CardBody, Button } from 'reactstrap';

import './style.scss';
import SectionView from './commons/SectionView';
import PaperHeader from './commons/PaperHeader';
import browserHistory from '../../utils/history';
import { apiUrls, viewRoutes } from '../../utils/constants';
import { getSortedExamSections } from '../Exams/utils/utils';
import { submitExamAnswers } from '../../services/ExamService';
import { get, showApiErrorMessage } from '../../utils/httpUtil';

import ConfirmPopup from '../../commons/ConfirmPopup/ConfirmPopup';

class ExamPaper extends PureComponent {
  state = {
    currentPage: 0,
    isLoading: true,
    examDetails: {},
    isTimeUp: false,
    totalQuestions: 0,
    studentAnswers: {},
    isSubmitted: false,
    lastPauseTime: null, // Set when route-blocking prompts pause the timer
    isSubmitting: false,
    isPaginationView: true,
    remainingTimeInSeconds: -1,
    isShowSubmissionPopup: false,
  };

  componentDidMount() {
    const { id: examId } = (this.props.match && this.props.match.params) || {};

    this._fetchData(examId);

    window.onbeforeunload = () => {
      if (!this._canLeaveExam()) {
        this.setState({ lastPauseTime: new Date() });

        return true;
      }
    };
  }

  componentWillUnmount() {
    window.onbeforeunload = undefined;
  }

  render() {
    let {
      isTimeUp,
      isLoading,
      examDetails,
      isSubmitted,
      isSubmitting,
      lastPauseTime,
      studentAnswers,
      totalQuestions,
      remainingTimeInSeconds,
      isShowSubmissionPopup,
    } = this.state;

    if (isLoading) {
      return 'Loading....';
    }

    if (isTimeUp) {
      return 'Time Up!!';
    }

    if (isSubmitted) {
      return 'Thank you, Please go to score view to see your answers';
    }

    if (
      examDetails.examStatus &&
      examDetails.examStatus.toLowerCase() !== 'inprogress'
    ) {
      return 'Exam has not started yet!!! or might have ended!!!';
    }

    return (
      <div className="animated fadeIn">
        {isShowSubmissionPopup ? (
          <ConfirmPopup
            message="Are you sure to submit your answers? Or you can hit No and verify your answers."
            isDisableClose={true}
            isRequesting={isSubmitting}
            handleCancel={this._closeConfirmSubmissionPopup}
            handleConfirm={this._submitAnswers}
          />
        ) : null}

        <Prompt message={this._getRouterPromptMessage} />

        <Row>
          <Col xs="12" sm="12" md="12" lg="8" className="examPaper">
            <Card>
              <CardHeader>
                <PaperHeader
                  examDetails={examDetails}
                  lastPauseTime={lastPauseTime}
                  clearLastPauseTime={this._clearLastPauseTime}
                  handleTimeUp={this._onTimeUp}
                  totalQuestions={totalQuestions}
                  remainingTimeInSeconds={remainingTimeInSeconds}
                  attemptedQuestions={Object.keys(studentAnswers).length}
                />
              </CardHeader>

              {!_isEmpty(examDetails.examSections) ? (
                <CardBody className="examPaperBody">
                  {this._renderExamSections()}

                  <div className="mb-1">
                    {this._getPaginationComponent()}
                    {this._renderToggleButton()}
                  </div>

                  <div className="submitButtonWrp">
                    <Button
                      color="success"
                      disabled={!this._getNumberOfSelectedAnswers()}
                      onClick={this._handleSubmitAnswersClick}
                    >
                      Submit Your Answers
                    </Button>
                    <span className="studentProgressMsg">{`${
                      Object.keys(studentAnswers).length
                    } out of ${totalQuestions} question completed`}</span>
                  </div>
                </CardBody>
              ) : (
                <div className="emptyQuestionView">
                  Sorry, we haven't added any questions so far.
                </div>
              )}
            </Card>
          </Col>
        </Row>
      </div>
    );
  }

  _renderExamSections = () => {
    const {
      isPaginationView,
      examDetails,
      currentPage,
      studentAnswers,
    } = this.state;

    if (isPaginationView && !_isEmpty(examDetails.examSections)) {
      return (
        <SectionView
          key={examDetails.examSections[currentPage].id}
          section={examDetails.examSections[currentPage]}
          index={currentPage + 1}
          onSelectAnswer={this._setStudentAnswer}
          studentAnswers={studentAnswers}
        />
      );
    }

    if (!_isEmpty(examDetails.examSections)) {
      return examDetails.examSections.map((section, index) => (
        <SectionView
          key={section.id}
          section={section}
          index={index + 1}
          onSelectAnswer={this._setStudentAnswer}
          studentAnswers={studentAnswers}
        />
      ));
    }

    return null;
  };

  _getPaginationComponent = () => {
    const { isPaginationView, examDetails, currentPage } = this.state;
    if (isPaginationView && examDetails.examSections) {
      const paginationNumber = examDetails.examSections.map((item, index) => (
        <li
          className={`page-item ${currentPage === index && 'active'}`}
          key={item.id}
        >
          <Button
            className="page-link"
            onClick={() => this._gotoCurrentPage(index)}
          >
            {item.title}
          </Button>
        </li>
      ));

      return <ul className="pagination flex-wrap">{paginationNumber}</ul>;
    }

    return null;
  };

  _renderToggleButton = () => {
    if (this.state.isPaginationView) {
      return (
        <>
          <span>
            Currently you are seeing this paper into different sections.
          </span>
          <Button color="link" onClick={this._handleTogglePaginationClick}>
            {'See full paper view'}
          </Button>
        </>
      );
    }

    return (
      <>
        <span>Trouble seeing long paper?</span>
        <Button color="link" onClick={this._handleTogglePaginationClick}>
          {'Break into sections'}
        </Button>
      </>
    );
  };

  _incrementCurrentPage = () => {
    this.setState({
      currentPage: this.state.currentPage + 1,
    });
  };

  _decrementCurrentPage = () => {
    this.setState({
      currentPage: this.state.currentPage - 1,
    });
  };

  _gotoCurrentPage = pageNumber => {
    this.setState({
      currentPage: pageNumber,
    });
  };

  _fetchData = async examId => {
    // TODO handle 404
    const examDetailsWithQuestionsUrl = apiUrls.examDetailsWithQuestions.replace(
      ':examId',
      examId,
    );

    const { data } = (await get(examDetailsWithQuestionsUrl).catch(
      showApiErrorMessage,
    )) || { data: {} };

    data.examSections = getSortedExamSections(data.examSections);

    const totalQuestions = this._countTotalQuestions(data);

    this.setState({
      examDetails: data,
      isLoading: false,
      totalQuestions,
      remainingTimeInSeconds: this.getTimeRemainingInSeconds(
        data.endTime,
        data.timeLimit * 60, // convert minutes into second
      ),
    });
  };

  getTimeRemainingInSeconds = (endTime, timeLimitInSeconds) => {
    const currentTime = new Date();
    const endingTime = new Date(endTime);

    let secondsRemaining = differenceInSeconds(endingTime, currentTime);

    if (secondsRemaining <= 0) {
      return 0;
    }

    if (secondsRemaining < timeLimitInSeconds) {
      return secondsRemaining;
    }

    return timeLimitInSeconds;
  };

  _onTimeUp = () => {
    this.setState({
      isTimeUp: true,
      remainingTimeInSeconds: 0,
      isShowSubmissionPopup: false,
    });

    if (this.state.isSubmitting || this.state.isSubmitted) {
      return;
    }

    this._submitAnswers();
  };

  _clearLastPauseTime = () => {
    this.setState({ lastPauseTime: null });
  };

  _setStudentAnswer = (examSectionId, questionId, studentAnswer) => {
    if (
      this.state.isTimeUp ||
      this.state.isSubmitted ||
      this.state.isSubmitting
    ) {
      return;
    }

    let studentAnswers = { ...this.state.studentAnswers };
    let detailsForSelectedQuestion = studentAnswers[questionId];

    if (detailsForSelectedQuestion === undefined) {
      detailsForSelectedQuestion = {
        examSectionId,
        questionId,
        studentAnswer,
      };
    } else {
      detailsForSelectedQuestion.studentAnswer = studentAnswer;
    }

    studentAnswers[questionId] = detailsForSelectedQuestion;
    this.setState({ studentAnswers });
  };

  _countTotalQuestions = examDetails => {
    let count = 0;

    examDetails.examSections &&
      examDetails.examSections.forEach(section => {
        count += section.totalQuestions;
      });

    return count;
  };

  _getNumberOfSelectedAnswers = () => {
    const { studentAnswers } = this.state;

    return (studentAnswers && Object.keys(studentAnswers).length) || 0;
  };

  _canLeaveExam = () =>
    !this.state.isSubmitting &&
    (this.state.isSubmitted || !(this._getNumberOfSelectedAnswers() > 0));

  _getRouterPromptMessage = () => {
    if (!this._canLeaveExam()) {
      this.setState({ lastPauseTime: new Date() });

      return 'Your progress has not been saved. Are you sure you want to leave?';
    }

    return true;
  };

  _handleTogglePaginationClick = () => {
    this.setState({ isPaginationView: !this.state.isPaginationView });
  };

  _handleSubmitAnswersClick = () => {
    this.setState({ isShowSubmissionPopup: true });
  };

  _closeConfirmSubmissionPopup = () => {
    this.setState({ isShowSubmissionPopup: false });
  };

  _getUnAnsweredQuestionsForSection = (questions, examSectionId) => {
    const unAnsweredQuestions = [];

    (questions || []).forEach(({ id }) => {
      if (!(this.state.studentAnswers && this.state.studentAnswers[id])) {
        unAnsweredQuestions.push({
          examSectionId,
          questionId: id,
          studentAnswer: '',
        });
      }
    });

    return unAnsweredQuestions;
  };

  _submitAnswers = async () => {
    this.setState({ isSubmitting: true });

    const { isTimeUp, examDetails, studentAnswers } = this.state;

    const examId = examDetails.id;

    const answeredQuestions =
      (studentAnswers && Object.values(studentAnswers)) || [];

    if (!isTimeUp && !(answeredQuestions && answeredQuestions.length)) {
      this.setState({ isSubmitting: false, isShowSubmissionPopup: false });

      toast.error('Invalid Submission!!');

      return;
    }

    const allUnAnsweredQuestions = (examDetails.examSections || []).reduce(
      (unAnsweredQuestions, examSection) => {
        const examSectionId = examSection.id;

        const unAnsweredQuestionsForSection = this._getUnAnsweredQuestionsForSection(
          examSection.questions,
          examSectionId,
        );

        return [...unAnsweredQuestions, ...unAnsweredQuestionsForSection];
      },
      [],
    );

    const finalQuestionsToSubmit = [
      ...answeredQuestions,
      ...allUnAnsweredQuestions,
    ];

    const response = await submitExamAnswers(
      examId,
      finalQuestionsToSubmit,
    ).catch(error => {
      showApiErrorMessage(error);

      return false;
    });

    if (response) {
      this._onSubmissionSuccess(response);
    }

    this.setState({ isSubmitting: false, isShowSubmissionPopup: false });
  };

  _onSubmissionSuccess = submissionResponse => {
    this.setState({
      isSubmitted: true,
      isSubmitting: false,
      isShowSubmissionPopup: false,
    });

    const message =
      (submissionResponse && submissionResponse.message) ||
      'Your answers have been submitted.';

    toast.success(message, {
      position: toast.POSITION.TOP_CENTER,
      autoClose: 2000,
    });

    let redirectPath = viewRoutes.studentDashboard;

    if (submissionResponse.resultPublished) {
      const examId = this.state.examDetails && this.state.examDetails.id;
      redirectPath = viewRoutes.studentExamScores.replace(':examId', examId);
    }

    setTimeout(() => {
      browserHistory.push(redirectPath);
    }, 1000);
  };
}

export default ExamPaper;
