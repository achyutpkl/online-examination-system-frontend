import React, { Component } from 'react';
import { Card, CardTitle, CardBody } from 'reactstrap';

import ExamList from './commons/ExamList';
import { getExamTypes } from '../../services/ExamService';
import { showApiErrorMessage } from '../../utils/httpUtil';

// TODO: May be move this, ExamList and ExamListItem in commons folder and merge this and ExamList into one component
export default class StudentExamList extends Component {
  constructor(props) {
    super(props);

    const { title } = (props.match && props.match.params) || {};

    this.status = ['UpComing', 'Completed', 'In Progress'];
    this.state = {
      dateFrom: '',
      dateTo: '',
      examType: '',
      examTypeList: [],
      isLoading: false,
      examStatus: this.status[0],
      filter: !!title
        ? {
            examType: {
              [title]: true,
            },
          }
        : {},
    };
  }

  componentWillReceiveProps(props) {
    const { title } = (props.match && props.match.params) || {};
    this.setState({
      filter: !!title
        ? {
            examType: {
              [title]: true,
            },
          }
        : {},
    });
  }

  componentDidMount() {
    this.fetchExamTypes();
  }

  fetchExamTypes = async () => {
    this.setState({ isLoading: true });

    let data = (await getExamTypes().catch(showApiErrorMessage)) || [];
    this.setState({
      examTypeList: data,
      examType: data[0].title,
      isLoading: false,
    });
  };

  handleOnChange = event => {
    const { name, value } = event.target;

    const filter = Object.assign({}, this.state.filter);
    if (name === 'examType' || name === 'examStatus') {
      if (!filter[name]) {
        filter[name] = {};
      }

      filter[name][value] = true;
    } else {
      filter[name] = value;
    }
    this.setState({
      [name]: value,
      filter,
    });
  };

  flatten = obj => {
    return Object.keys(obj);
  };

  isObject = value => {
    return value && typeof value === 'object' && value.constructor === Object;
  };

  getChips = () => {
    let chips = [];
    const { filter } = this.state;

    for (let key in filter) {
      if (this.isObject(filter[key])) {
        const flattenKeys = this.flatten(filter[key]);
        chips = chips.concat(flattenKeys.map(item => [key, item]));
      } else {
        if (Object.prototype.toString.call(filter[key]) === '[object Date]') {
          chips.push([key, filter[key].toLocaleDateString()]);
        } else {
          chips.push([key, filter[key]]);
        }
      }
    }

    return chips;
  };

  deleteChip = (key, value) => {
    const filter = Object.assign({}, this.state.filter);

    if (this.isObject(filter[key])) {
      delete filter[key][value];
    } else {
      this.setState({
        [key]: '',
      });
      delete filter[key];
    }

    this.setState({
      filter,
    });
  };

  render() {
    const { title } = (this.props.match && this.props.match.params) || {};
    const {
      filter: {
        dateFrom: filterDateFrom,
        dateTo: filterDateTo,
        examType: filterExamType,
        examStatus: filterExamStatus,
      },
    } = this.state;

    return (
      <div>
        <Card body>
          <CardTitle>Exams</CardTitle>
          {/* {isLoading ? (
            'Loading...'
          ) : (
            <CardHeader>
              <Row>
                <Col>
                  <DatePicker
                    className="form-control date-input"
                    required
                    name="dateFrom"
                    placeholderText="From Date"
                    autoComplete="off"
                    selected={dateFrom || null}
                    onChange={value => {
                      this.handleOnChange({
                        target: { name: 'dateFrom', value },
                      });
                    }}
                    showMonthDropdown
                    showYearDropdown
                    dropdownMode="select"
                  />
                </Col>
                <Col>
                  <DatePicker
                    className="form-control date-input"
                    required
                    name="dateTo"
                    selected={dateTo || null}
                    placeholderText="To Date"
                    autoComplete="off"
                    onChange={value => {
                      this.handleOnChange({
                        target: { name: 'dateTo', value },
                      });
                    }}
                    showMonthDropdown
                    showYearDropdown
                    dropdownMode="select"
                  />
                </Col>
                <Col>
                  <Input
                    name="examType"
                    value={examType}
                    id="examType"
                    type="select"
                    placeholder="Exam Type"
                    autoComplete="examType"
                    onChange={this.handleOnChange}
                    required
                  >
                    {examTypeList.map(item => (
                      <option key={item.id} value={item.title}>
                        {item.title}
                      </option>
                    ))}
                  </Input>
                </Col>
                <Col>
                  <Input
                    name="examStatus"
                    value={examStatus}
                    id="examStatus"
                    type="select"
                    placeholder="Exam Status"
                    autoComplete="examStatus"
                    onChange={this.handleOnChange}
                    required
                  >
                    {this.status.map(item => (
                      <option key={item} value={item}>
                        {item}
                      </option>
                    ))}
                  </Input>
                </Col>
              </Row>
              <Row style={{ paddingTop: 10 }}>
                {this.getChips().map(item => (
                  <Chip
                    pair={item}
                    key={item[0] + item[1]}
                    onPressCross={this.deleteChip}
                  />
                ))}
              </Row>
            </CardHeader>
          )} */}
          <CardBody>
            <ExamList
              {...this.props}
              filter={{
                dateFrom: filterDateFrom,
                dateTo: filterDateTo,
                examType: filterExamType ? Object.keys(filterExamType) : [],
                examStatus: filterExamStatus
                  ? Object.keys(filterExamStatus)
                  : [],
              }}
              filterExamType={{ title: title }}
            />
          </CardBody>
        </Card>
      </div>
    );
  }
}
