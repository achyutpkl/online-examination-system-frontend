import { isFunction } from 'lodash';
import { toast } from 'react-toastify';
import React, { PureComponent } from 'react';
import { isBefore } from 'date-fns';
import {
  Jumbotron,
  Button,
  Row,
  Col,
  Card,
  CardHeader,
  Badge,
  CardBody,
} from 'reactstrap';

import './style.scss';

import {
  getFormattedDateTime,
  getDifferenceInMilliseconds,
  getFormattedDate,
  getCurrentDate,
} from '../../utils/dateUtils';
import browserHistory from '../../utils/history';
import { getItem } from '../../utils/localStorage';
import { getExam } from '../../services/ExamService';
import { getAllScoreForExam } from '../Exams/ExamService';
import { showApiErrorMessage } from '../../utils/httpUtil';
import { paymentFieldNames } from '../StudentPayments/constants';
import { viewRoutes, localStorageKeys } from '../../utils/constants';
import { getPaymentIdForExam } from '../../services/ExamPaymentService';

import NotFound from '../NotFound/NotFound';
import ExamFact from '../StudentExamScores/commons/ExamFact';
import ListWithRouting from '../StudentDashboard/commons/ListWithRouting';

class StudentExam extends PureComponent {
  state = {
    examDetails: {},
    examResult: [],
    paginationInfo: {
      pages: 1,
      page: 0,
      pageSize: 50,
      totalElements: 0,
    },
    examNotFound: false,
    examLoading: false,
    user: getItem(localStorageKeys.oesUser) || {},
    paymentInfo: {
      isPaid: false,
      isLoading: false,
    },
    currentTime: new Date(),
  };

  timerId = null;

  componentDidMount() {
    const { id: examId } = (this.props.match && this.props.match.params) || {};

    this._fetchData(examId);
    this._fetchPaymentDetails(examId);
  }

  componentWillUnmount() {
    clearTimeout(this.timerId);
  }

  // eslint-disable-next-line complexity
  render() {
    const {
      examDetails,
      examResult,
      examNotFound,
      paginationInfo,
      examLoading,
      paymentInfo,
    } = this.state;

    const showResult = examResult.length > 0;

    if (examNotFound) {
      return <NotFound initialPath={this.props.location.pathname || ''} />;
    }

    return (
      <div className="animated fadeIn">
        <Row>
          {examLoading || paymentInfo.isLoading ? (
            'Loading Exam...'
          ) : (
            <Col
              xs="12"
              sm="12"
              md={showResult ? 8 : 12}
              lg={showResult ? 8 : 12}
            >
              <Card>
                <CardHeader>
                  <Jumbotron>
                    <span className="h4 mr-2">
                      <Badge color="primary">{examDetails.examStatus}</Badge>
                    </span>

                    <span className="h4">
                      <Badge color="warning">
                        {examDetails.examType && examDetails.examType.title}
                      </Badge>
                    </span>
                    <h1>{examDetails.title}</h1>
                    <p className="lead">{examDetails.description}</p>
                    <hr className="my-2" />
                    {this._renderExamFacts(examDetails)}
                    <hr className="my-2" />
                    <div>
                      <div className="studentExamBtnWrp mb-1 mr-1">
                        {this._isNeedToPay() ? (
                          <Button
                            color="primary"
                            onClick={this._onPayForExam}
                            disabled={!this._canPayForExam(examDetails)}
                          >
                            Pay For Exam
                          </Button>
                        ) : (
                          <Button
                            color="success"
                            onClick={this._onTakeExam}
                            disabled={!this._canTakeExam(examDetails)}
                          >
                            Take Exam Now
                          </Button>
                        )}
                      </div>
                      <div className="studentExamBtnWrp">
                        <Button
                          color="info"
                          disabled={!examDetails.publishResult}
                          onClick={this._onViewExamScores}
                        >
                          View Scores
                        </Button>
                        {!examDetails.publishResult && (
                          <span className="resultNotPublishedText">
                            (Result has not been published yet)
                          </span>
                        )}
                      </div>
                    </div>
                  </Jumbotron>
                </CardHeader>
              </Card>
            </Col>
          )}
          {showResult && (
            <Col xs="12" sm="12" md="4" lg="4">
              <Card>
                <CardHeader>Top Scorers</CardHeader>
                <CardBody>
                  <p className="exam-top-scores">
                    <ExamFact
                      name="Top Score"
                      color="success"
                      value={
                        examResult.length > 0 && examResult[0].highestScore
                      }
                    />
                    <ExamFact
                      name="Average Score"
                      color="primary"
                      value={Math.round(
                        (examResult.length > 0 && examResult[0].averageScore) ||
                          0,
                      )}
                    />
                    <ExamFact
                      name="Total Attempts"
                      color="primary"
                      value={paginationInfo.totalElements}
                    />
                  </p>

                  <ListWithRouting
                    keyProp="examScoreId1"
                    items={examResult}
                    render={({
                      attemptDate,
                      studentScore,
                      rank,
                      studentDetail: { firstName, middleName, lastName },
                    }) => (
                      <div>
                        <span className="mr-2">
                          <Badge color="light">{rank}</Badge>
                        </span>
                        <span className="mr-2">
                          {`${firstName} ${middleName || ''} ${lastName}`}
                        </span>
                        <span className="mr-2">
                          <Badge color="success">{studentScore}</Badge>
                        </span>
                        <span className="mr-2">
                          <Badge color="light">
                            {getFormattedDate(attemptDate)}
                          </Badge>
                        </span>
                      </div>
                    )}
                  />
                </CardBody>
              </Card>
            </Col>
          )}
        </Row>
      </div>
    );
  }

  _renderExamFacts = (examDetails = {}) => (
    <div className="examFacts">
      <ExamFact
        name="Start Date"
        value={getFormattedDateTime(examDetails.startTime)}
        color="light"
      />
      <ExamFact
        name="End Date"
        value={getFormattedDateTime(examDetails.endTime)}
        color="light"
      />
      <ExamFact name="Full Marks" value={examDetails.fullMark} color="light" />
      <ExamFact name="Pass Marks" value={examDetails.passMark} color="light" />
      <ExamFact name="Time Limit" value={examDetails.timeLimit} color="light" />
    </div>
  );

  _fetchData = async examId => {
    this.setState({ examLoading: true });
    const currentTime = await getCurrentDate();

    const examDetails =
      (await getExam(examId).catch(
        this._getHandleIfNotFound({}, () =>
          this.setState({ examNotFound: true }),
        ),
      )) || {};

    this.setState({
      examDetails,
      currentTime,
      examLoading: false,
    });

    const { data: examResult, pageInfo } = (await getAllScoreForExam(
      examId,
      0,
      20,
    ).catch(error => null)) || { data: [], pageInfo: {} };
    const paginationInfo = {
      ...this.state.paginationInfo,
      pages: pageInfo ? pageInfo.totalPages : 0,
      page: pageInfo ? pageInfo.pageNo : 0,
      pageSize: pageInfo.pageSize,
      totalElements: pageInfo ? pageInfo.totalElements : 0,
    };

    this.setState({
      examResult,

      paginationInfo,
    });
    this._startTimer(examDetails.startTime);
  };

  _getHandleIfNotFound = (returnValue, notFoundHandler) => error => {
    const errorStatusCode = error && error.response && error.response.status;

    if (errorStatusCode === 404) {
      if (isFunction(notFoundHandler)) {
        notFoundHandler(error);
      }

      return returnValue;
    }

    return showApiErrorMessage(error);
  };

  _fetchPaymentDetails = examId => {
    // const userId = this.state.user.userId;

    this.setState({
      paymentInfo: { ...this.state.paymentInfo, isLoading: true },
    });

    let isPaid = false;

    const paymentDetails = [];
    // (await getExamPaymentDetails(examId, userId).catch(
    //   this._getHandleIfNotFound([]),
    // )) || [];

    if (paymentDetails && paymentDetails.length) {
      isPaid = true;
    }

    this.setState({
      paymentInfo: {
        ...this.state.paymentInfo,
        isLoading: false,
        isPaid,
      },
    });
  };

  _isNeedToPay() {
    const { isPaid } = this.state.paymentInfo;

    const examPrice = this._getExamPrice();

    if (isPaid || isNaN(examPrice) || examPrice <= 0) {
      return false;
    }

    return true;
  }

  _getExamPrice() {
    const { examType = {} } = this.state.examDetails || {};

    return Number(examType.price);
  }

  _onPayForExam = () => {
    const { id: examId, title: examTitle } = this.state.examDetails || {};

    if (!examId) {
      return toast.error('Invalid exam id');
    }

    const userId = this.state.user.userId;

    if (!userId) {
      return toast.error('Invalid user information');
    }

    browserHistory.push(viewRoutes.studentPayments, {
      paymentFields: {
        [paymentFieldNames.amount]: this._getExamPrice(),
        [paymentFieldNames.productId]: getPaymentIdForExam(),
        [paymentFieldNames.paymentName]: examTitle || 'Exam',
      },
      examId,
      userId,
    });
  };

  _onTakeExam = () => {
    const { id: examId } = this.state.examDetails || {};

    if (examId) {
      browserHistory.push(viewRoutes.studentExamPaper.replace(':id', examId));
    }
  };

  _onViewExamScores = () => {
    const { id: examId } = this.state.examDetails || {};

    if (examId) {
      browserHistory.push(
        viewRoutes.studentExamScores.replace(':examId', examId),
      );
    }
  };

  /*
   *
   * @param startTime
   * @returns {}
   *
   * This is hackish way to handle real time system.
   * Why?
   * When user remains on the page stale then at the time of start date/time we need to refresh the page so that Take Exam Now button will enabled.
   * Winter is Comming
   */
  _startTimer = startTime => {
    let millisecondsLeftToStartExam = getDifferenceInMilliseconds(
      new Date(),
      startTime,
    );

    if (millisecondsLeftToStartExam <= 3600000) {
      // 1 hr = 3600000 milliseconds

      this.timerId = setTimeout(() => {
        this.forceUpdate();
      }, millisecondsLeftToStartExam);
    }
  };

  _canTakeExam = ({ examStatus }) => {
    // const currentTime = this.state.currentTime;

    // if (!endTime) {
    //   return isAfter(currentTime, startTime);
    // }

    // return isBetween(currentTime, startTime, endTime);
    return examStatus && examStatus.toLowerCase() === 'inprogress';
  };

  _canPayForExam = ({ endTime }) => {
    const currentTime = new Date();

    if (!endTime) {
      return true;
    }

    return isBefore(currentTime, endTime);
  };
}

export default StudentExam;
