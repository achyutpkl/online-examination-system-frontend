import React from 'react';
import { Badge } from 'reactstrap';

import QuestionView from './QuestionView';

const SectionView = ({ section, onSelectAnswer, studentAnswers, index }) => (
  <div className="sectionWrp">
    <div className="sectionHeading">
      <span className="h4">
        {index}. {section.title}
      </span>
      <span>
        <Badge
          className="fixSpace"
          color="primary"
          title="Total number of questions"
        >
          {section.totalQuestions}
        </Badge>
        <Badge
          className="fixSpace"
          color="success"
          title="Marks of each question"
        >
          +{section.questionWeightage}
        </Badge>
        <Badge color="danger" title="Negative mark">
          -{section.negativeMarks}
        </Badge>
      </span>
    </div>
    <p className="descriptionText">{section.description}</p>
    <hr />

    <ol className="questions">
      {section.questions &&
        section.questions.map((question, index) => (
          <li key={question.id}>
            <QuestionView
              question={question}
              sectionId={section.id}
              onSelectAnswer={onSelectAnswer}
              studentAnswers={studentAnswers}
            />
          </li>
        ))}
    </ol>
  </div>
);

export default SectionView;
