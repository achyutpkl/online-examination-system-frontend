import React from 'react';
import { Button } from 'reactstrap';

import './../style.scss';

class Chip extends React.Component {
  render() {
    const { pair, onPressCross } = this.props;

    return (
      <span className="chipWrapper">
        {`${pair[0]}: ${pair[1]}`}
        <Button
          style={{ float: 'right' }}
          color="link"
          size="sm"
          onClick={() => onPressCross(pair[0], pair[1])}
        >
          <i className={`fa fa-times`} />
        </Button>
      </span>
    );
  }
}

export default Chip;
