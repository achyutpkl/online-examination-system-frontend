import { toast } from 'react-toastify';
import React, { PureComponent } from 'react';

class Timer extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      timer: props.maxTimeInSecond,
      stopTime: props.maxTimeInSecond,
    };
  }

  componentDidMount() {
    this._checkExamTime();
  }

  render() {
    return (
      <div className="timerWrapper">
        <div className="timer">
          Remaining Time: {this._displayFormattedTime(this.state.timer)}
        </div>
        <div className="timeoutNotice">
          Paper will submitted by itself when time is over{' '}
        </div>
      </div>
    );
  }

  _getSecondsLeftAfterTimerPause = () => {
    const currentTime = new Date();

    const secondsPassedSinceLastPause =
      (currentTime.getTime() - this.props.lastPauseTime.getTime()) / 1000;

    this.props.clearLastPauseTime();

    const secondsLeft = this.state.timer - secondsPassedSinceLastPause;

    return secondsLeft;
  };

  _checkExamTime = () => {
    this.examTimerId = setInterval(() => {
      let secondsLeft = this.state.timer;

      if (this.props.lastPauseTime) {
        secondsLeft = this._getSecondsLeftAfterTimerPause();
      }

      if (secondsLeft === 0.15 * this.state.stopTime) {
        toast.warn('Time is almost up!');
      }

      if (secondsLeft <= 0) {
        clearInterval(this.examTimerId);
        this.props.handleTimeUp();

        return;
      }

      this.setState({ timer: secondsLeft - 1 });
    }, 1000);
  };

  _displayFormattedTime = timeInSeconds => {
    let hours = Math.floor(timeInSeconds / 3600);
    let hourRemainder = timeInSeconds % 3600;
    let minutes = Math.floor(hourRemainder / 60);
    let seconds = Math.floor(hourRemainder % 60);

    // let hours = Math.floor(timeInSeconds / 3600);
    // let minutes = Math.floor((timeInSeconds % 3600) / 60);
    // let seconds = Math.floor(timeInSeconds % 60);

    return `${hours}h ${minutes}m ${seconds}s`;
  };

  componentWillUnmount() {
    clearInterval(this.examTimerId);
  }
}

export default Timer;
