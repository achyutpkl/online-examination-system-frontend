import React from 'react';
import { Card, CardBody, Badge } from 'reactstrap';

import TestResultImage from '../../../assets/images/test-result.svg';

import { getFormattedDateTime } from '../../../utils/dateUtils';

const BadgeSpacing = props => (
  <span className={`badge ${props.className}`}>{props.children}</span>
);

const getIconClasses = examStatus => {
  if (examStatus === 'INPROGRESS') {
    return 'fa fa-unlock';
  }

  if (examStatus === 'COMPLETED') {
    return 'fa fa-check';
  }

  return 'fa fa-lock';
};

export default ({
  id,
  title,
  examType,
  startTime,
  examStatus,
  handleSelect,
  publishResult,
}) => (
  <Card
    className="shadow"
    style={{ cursor: 'pointer' }}
    onClick={() => handleSelect(id)}
  >
    <CardBody className="px-0 pt-1 pb-0 text-white">
      <div className="d-flex justify-content-between">
        <BadgeSpacing className="text-success">{examStatus}</BadgeSpacing>

        <BadgeSpacing className="text-primary">
          {getFormattedDateTime(startTime)}
        </BadgeSpacing>
      </div>

      <div
        className="bg-cyan d-flex flex-row justify-content-between px-3 py-2"
        style={{ color: '#f9f9f9' }}
      >
        <div className="d-flex align-items-center">
          {publishResult ? (
            <i
              style={{
                backgroundImage: `url(${TestResultImage})`,
                width: '2.5rem',
                height: '2.5rem',
                backgroundSize: '100% 100%',
                transform: 'scale(1.2, 1.2)',
              }}
            />
          ) : (
            <i
              className={getIconClasses(examStatus)}
              style={{ fontSize: '2.5rem' }}
            />
          )}
        </div>
        <div className="text-right">
          <div className="h5 mt-2 mb-1">{title}</div>

          <div className="pb-1">
            <Badge color="danger">{examType && examType.title}</Badge>
          </div>
        </div>
      </div>
    </CardBody>
  </Card>
);
