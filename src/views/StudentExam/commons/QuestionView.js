import React from 'react';
import { CustomInput } from 'reactstrap';

import QuestionTitle from '../../../commons/QuestionTitle/QuestionTitle';
import QuestionOption from '../../../commons/QuestionOption/QuestionOption';

const QuestionView = ({
  question,
  sectionId,
  onSelectAnswer,
  studentAnswers,
}) => {
  const options = question.options;
  let isAttempted = question.id in studentAnswers;
  let answerGiven = isAttempted
    ? studentAnswers[question.id].studentAnswer
    : '';

  return (
    <div className="questionWrp">
      <div style={{ flex: 1 }}>
        <QuestionTitle
          className="questionTitle"
          dangerouslySetInnerHTML={{ __html: question.title }}
        />

        {options &&
          Object.keys(options).map(key => {
            let uniqueKey = `${question.id}${key}${options[key]}`;

            return (
              <CustomInput
                className="option"
                type="radio"
                id={uniqueKey}
                key={uniqueKey}
                name={question.id}
                label={
                  <QuestionOption
                    dangerouslySetInnerHTML={{ __html: options[key] }}
                  />
                }
                value={key}
                checked={answerGiven === key}
                onChange={data =>
                  onSelectAnswer(sectionId, question.id, data.target.value)
                }
              />
            );
          })}
      </div>
      <span className={isAttempted ? 'questionAttempted' : ''} />
    </div>
  );
};

export default QuestionView;
