import { Row, Col } from 'reactstrap';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import browserHistory from '../../../utils/history';
import { getItem } from '../../../utils/localStorage';
import {
  userRole,
  viewRoutes,
  localStorageKeys,
} from '../../../utils/constants';

import ExamListItem from './ExamListItem';
import { getExams } from '../../../services/ExamService';
import { sortListByDate } from '../../../utils/dateUtils';
import { showApiErrorMessage } from '../../../utils/httpUtil';

class ExamList extends Component {
  state = {
    exams: [],
    isLoading: false,
    userRoles: [],
  };

  componentDidMount() {
    this.fetchExams(this.props.filter);
    this.fetchUserRoles();
  }

  componentWillReceiveProps(props) {
    this.fetchExams(props.filter);
  }

  fetchExams = async filter => {
    this.setState({ isLoading: true });
    const { data } = (await getExams(filter).catch(showApiErrorMessage)) || {
      data: [],
    };

    let sortedExams = sortListByDate(data, 'startTime');

    if (this.props.maxExamsInList) {
      sortedExams = sortedExams.slice(0, this.props.maxExamsInList);
    }

    this.setState({
      exams: sortedExams,
      isLoading: false,
    });
  };

  fetchUserRoles = () => {
    const { roles } = getItem(localStorageKeys.oesUser) || { roles: [] };

    this.setState({ userRoles: roles });
  };

  handleSelect = id => {
    if (this.state.userRoles.includes(userRole.admin)) {
      return browserHistory.push(viewRoutes.adminExamEdit.replace(':id', id));
    }

    browserHistory.push(viewRoutes.studentExamDetails.replace(':id', id));
  };

  render() {
    const { exams, userRoles, isLoading } = this.state;

    if (isLoading) {
      return <div>Loading...</div>;
    }
    const { title: examType } = this.props.filterExamType || {};

    const { roles } = getItem(localStorageKeys.oesUser) || { roles: [] };

    if (exams.length <= 0) {
      return (
        <div>
          There is no exam for {examType} exam. <br />
          <Link
            to={
              roles[0] === userRole.student ? '/student/exams' : '/admin/exams'
            }
          >
            Try other exams.
          </Link>
        </div>
      );
    }

    return (
      <Row>
        {exams &&
          exams.map(exam => (
            <Col name={exam.id} key={exam.id} xs="12" sm="6" lg="3">
              <ExamListItem
                {...exam}
                userRoles={userRoles}
                handleSelect={this.handleSelect}
              />
            </Col>
          ))}
      </Row>
    );
  }
}

export default ExamList;
