import React from 'react';
import { Progress, Row, Col } from 'reactstrap';

import '../style.scss';
import Timer from './Timer';

const ExamPaperFact = ({ name, value }) => (
  <div className="examPaperFact d-inline-block">
    {name}: <span className="factValue">{value}</span>
  </div>
);

const PaperHeader = props => (
  <div>
    <Row className="paperTitle">
      <Col xs="12" md="6" className="h1">
        {props.examDetails.title}
      </Col>
      {props.remainingTimeInSeconds > 0 && !!props.totalQuestions && (
        <Col xs="12" md="6" className="headerRight">
          <Timer
            lastPauseTime={props.lastPauseTime}
            clearLastPauseTime={props.clearLastPauseTime}
            handleTimeUp={props.handleTimeUp}
            maxTimeInSecond={props.remainingTimeInSeconds}
          />
        </Col>
      )}
    </Row>
    <p className="descriptionText">{props.examDetails.description} </p>
    <div className="paperHeadDetails">
      <ExamPaperFact name="Full Marks" value={props.examDetails.fullMark} />
      <ExamPaperFact name="Pass Marks" value={props.examDetails.passMark} />
      <ExamPaperFact name="Total Questions" value={props.totalQuestions} />
      <ExamPaperFact
        name="Total Time"
        value={`${props.examDetails.timeLimit} minutes`}
      />
    </div>
    <Progress
      color="success"
      value={props.attemptedQuestions}
      max={props.totalQuestions}
    >
      {props.attemptedQuestions}
    </Progress>
  </div>
);

export default PaperHeader;
