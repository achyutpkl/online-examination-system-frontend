import React from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Input,
  Label,
  Form,
  FormGroup,
} from 'reactstrap';

const QuestionPreview = props => {
  const { questionTitle, options, explanation } = { ...props };

  return (
    <Card>
      <CardHeader>
        <strong>Preview</strong>
      </CardHeader>
      <CardBody>
        <Row>
          <Col xs="12">
            <div
              dangerouslySetInnerHTML={{ __html: questionTitle }}
              className="inline-block-div"
            />
          </Col>
        </Row>

        <Row>
          <Col xs="12">
            <Form>
              {options &&
                Object.values(options).map((option, index) => (
                  <FormGroup check inline key={index}>
                    <Label check>
                      <Input type="radio" name="option" />
                      <div
                        className="inline-block-div"
                        dangerouslySetInnerHTML={{ __html: option.title }}
                      />
                    </Label>
                  </FormGroup>
                ))}
              <div
                className="text-muted"
                dangerouslySetInnerHTML={{ __html: explanation }}
              />
            </Form>
          </Col>
        </Row>
      </CardBody>
    </Card>
  );
};

export default QuestionPreview;
