import ReactTable from 'react-table';
import React, { PureComponent, Fragment, Component } from 'react';
import { Card, CardHeader, CardBody, Button } from 'reactstrap';

import 'react-table/react-table.css';

import './index.scss';
import browserHistory from '../../utils/history';
import { viewRoutes } from '../../utils/constants';
import { showApiErrorMessage } from '../../utils/httpUtil';
import {
  getPracticeQuestions,
  editPracticeQuestion,
  deletePracticeQuestion,
} from './QuestionService';

import SubDivSelector from './SubDivSelector';
import ConfirmModal from '../../commons/ConfirmModal/ConfirmModal';
import { NextPageButton, PreviousPageButton } from './commons/PaginationButton';
import QuestionFormModal from '../../commons/Questions/commons/QuestionFormModal';
import CBReactTablePagination from '../../commons/CustomReactTablePagination/CBReactTablePagination';

class ViewPracticeQuestions extends PureComponent {
  state = {
    questions: [],
    isQuestionsLoading: false,
    paginationInfo: {
      pages: 1,
      page: 0,
      pageSize: 50,
    },
    questionForm: {
      isOpen: false,
      question: null,
    },
  };

  componentDidMount() {
    this.fetchPracticeQuestionsWithCurrentData();
  }

  fetchPracticeQuestionsWithCurrentData = () => {
    const { selectedSubDivisionId } = this.props;

    if (!selectedSubDivisionId) {
      return this.setState({ questions: [] });
    }

    const { page, pageSize } = this.state.paginationInfo;

    this.fetchPracticeQuestions(selectedSubDivisionId, page, pageSize);
  };

  fetchPracticeQuestions = async (subDivisionId, page, pageSize) => {
    this.setState({ isQuestionsLoading: true });

    const { data: questions, pageInfo } = (await getPracticeQuestions(
      subDivisionId,
      page,
      pageSize,
    ).catch(showApiErrorMessage)) || { data: [], pageInfo: {} };

    const paginationInfo = {
      ...this.state.paginationInfo,
      pages: pageInfo.totalPages,
      page: pageInfo.pageNo,
      pageSize: pageSize,
    };

    this.setState({ isQuestionsLoading: false, questions, paginationInfo });
  };

  onPaginationInfoChange = info => {
    const paginationInfo = { ...this.state.paginationInfo, ...info };

    this.fetchPracticeQuestions(
      this.props.selectedSubDivisionId,
      paginationInfo.page,
      paginationInfo.pageSize,
    );
  };

  setQuestionFormProps = (props = {}) => {
    const questionForm = this.state.questionForm || {};

    this.setState({ questionForm: { ...questionForm, ...props } });
  };

  onDeleteClick = questionId => {
    if (!questionId) {
      return;
    }

    this.props.openModal({
      handleClose: async ({ isConfirmed }) => {
        if (!isConfirmed) {
          return;
        }

        this.setState({ isQuestionsLoading: true });

        await deletePracticeQuestion(questionId).catch(showApiErrorMessage);

        this.fetchPracticeQuestionsWithCurrentData();
      },
    });
  };

  onEditClick = questionId => {
    const questionToEdit = this.state.questions.find(
      ({ id }) => id === questionId,
    );

    if (!questionToEdit) {
      return;
    }

    this.setQuestionFormProps({ isOpen: true, question: questionToEdit });
  };

  handleFormCancel = () => {
    this.setQuestionFormProps({ isOpen: false, question: null });
  };

  handleFormSubmit = (receivedQuestion = {}) => {
    const formQuestion =
      this.state.questionForm && this.state.questionForm.question;

    if (!formQuestion) {
      return;
    }

    const questionIndex = this.state.questions.findIndex(
      ({ id }) => id === formQuestion.id,
    );

    if (questionIndex === -1) {
      return;
    }

    const currentQuestions = this.state.questions;

    const newQuestion = { ...formQuestion, ...receivedQuestion };

    this.setState({
      questions: [
        ...currentQuestions.slice(0, questionIndex),
        newQuestion,
        ...currentQuestions.slice(questionIndex + 1),
      ],
    });

    this.setQuestionFormProps({ isOpen: false, question: null });

    // TODO: Confirm whether to show some loading state or not.
    editPracticeQuestion(newQuestion).catch(showApiErrorMessage);
  };

  onAddQuestionsClick = () => {
    browserHistory.push(viewRoutes.addQuestions, {
      subjectId: this.props.selectedSubjectId,
      divisionId: this.props.selectedDivisionId,
      subDivisionId: this.props.selectedSubDivisionId,
    });
  };

  questionsIsLoading = () => {
    const { isQuestionsLoading } = this.state;
    const {
      isSubjectsLoading,
      isDivisionsLoading,
      isSubDivisionsLoading,
    } = this.props;

    return (
      isSubjectsLoading ||
      isDivisionsLoading ||
      isSubDivisionsLoading ||
      isQuestionsLoading
    );
  };

  columns = [
    {
      Header: 'S.No.',
      maxWidth: 50,
      Cell: ({ index }) => {
        const { page, pageSize } = this.state.paginationInfo;

        return (
          <span className="serial-num">{page * pageSize + (index + 1)}</span>
        );
      },
    },
    {
      Header: 'Question',
      accessor: 'title',
      Cell: ({ value }) => (
        <div
          className="title no-child-paragraph-margin"
          dangerouslySetInnerHTML={{ __html: value }}
        />
      ),
    },
    {
      Header: 'Actions',
      accessor: 'id',
      maxWidth: 100,
      Cell: ({ value }) => (
        <div className="actions d-flex justify-content-around">
          <Button color="success" onClick={() => this.onEditClick(value)}>
            <i className="fa fa-edit" />
          </Button>
          <Button color="danger" onClick={() => this.onDeleteClick(value)}>
            <i className="fa fa-trash" />
          </Button>
        </div>
      ),
    },
  ];

  render() {
    const { questions, paginationInfo, questionForm } = this.state;

    return (
      <Card>
        <QuestionFormModal
          isOpen={questionForm.isOpen}
          isEdit
          isPracticeQuestion={true}
          questionDetails={questionForm.question}
          handleSubmit={this.handleFormSubmit}
          handleCancel={this.handleFormCancel}
        />

        <CardHeader>
          <strong className="h5">Practice Questions</strong>

          {this.props.selectedSubDivisionId && !this.questionsIsLoading() ? (
            <div className="card-header-actions">
              <Button
                outline
                color="primary"
                onClick={this.onAddQuestionsClick}
              >
                Add questions
              </Button>
            </div>
          ) : null}
        </CardHeader>

        <CardBody>
          <ReactTable
            manual
            sortable={false}
            data={questions}
            minRows={0}
            loading={this.questionsIsLoading()}
            columns={this.columns}
            {...paginationInfo}
            onPageChange={page => this.onPaginationInfoChange({ page })}
            onPageSizeChange={pageSize =>
              this.onPaginationInfoChange({ pageSize })
            }
            className="-striped -highlight practice-question-table"
            NextComponent={NextPageButton}
            PreviousComponent={PreviousPageButton}
            PaginationComponent={CBReactTablePagination}
          />
        </CardBody>
      </Card>
    );
  }
}

export default class extends Component {
  render() {
    return (
      <Fragment>
        <ConfirmModal>
          {confirmModalProps => (
            <SubDivSelector {...this.props}>
              {selectorProps => (
                <ViewPracticeQuestions
                  {...this.props}
                  key={selectorProps.selectedSubDivisionId}
                  {...selectorProps}
                  {...confirmModalProps}
                />
              )}
            </SubDivSelector>
          )}
        </ConfirmModal>
      </Fragment>
    );
  }
}
