import { toast } from 'react-toastify';
import React, { PureComponent } from 'react';
import { Col, Row, Card, Button, CardBody, CardHeader } from 'reactstrap';

import './index.scss';
import browserHistory from '../../utils/history';
import { viewRoutes } from '../../utils/constants';
import { savePracticeQuestions } from './QuestionService';
import { showApiErrorMessage } from '../../utils/httpUtil';
import { getQuestionValidationMessage } from '../Exams/utils/utils';
import {
  getSubjects,
  getDivisions,
  getSubDivisions,
} from '../../services/SubjectService';

import Selector from './commons/Selector';
import QuestionsHeader from './commons/QuestionsHeader';
import Questions from '../../commons/Questions/Questions';
import { withConfirmModal } from '../../commons/ConfirmModal/ConfirmModal';

/**
  {
    "title": "What is SI unit of time?",
    "options": { "A": "sec", "B": "min", "C": "hr","D":"other"},
    "correctAnswer": "min",
    "noOfOption": 4,
    "explanation": "",
    "difficultyLevel": "EASY",
    "type": "free",
    "providedBy": "achyut",
    "subDivisionId": "de9943fd-908f-4657-93c6-c42dbe089446"
  }
 */

const selectorTypes = {
  subject: 'subject',
  division: 'division',
  subDivision: 'subDivision',
};

class AddQuestion extends PureComponent {
  selectors = [
    {
      type: selectorTypes.subject,
      getItems: () => this.state.subjects,
      getSelectedItemId: () => this.state.selectedSubjectId,
    },
    {
      type: selectorTypes.division,
      getItems: () => this.state.divisions,
      getSelectedItemId: () => this.state.selectedDivisionId,
    },
    {
      type: selectorTypes.subDivision,
      getItems: () => this.state.subDivisions,
      getSelectedItemId: () => this.state.selectedSubDivisionId,
    },
  ];

  state = {
    subjects: [],
    divisions: [],
    subDivisions: [],
    questions: [],
    selectedSubjectId: '',
    selectedDivisionId: '',
    selectedSubDivisionId: '',
    isSubjectsLoading: false,
    isDivisionsLoading: false,
    isSubDivisionsLoading: false,
    isAddingQuestions: false,
  };

  componentDidMount() {
    const { subjectId, divisionId, subDivisionId } = (this.props.location &&
      this.props.location.state) || {
      subjectId: '',
      divisionId: '',
      subDivisionId: '',
    };

    this.loadSubjectsDivisionsAndSubDivisions(
      subjectId,
      divisionId,
      subDivisionId,
    );
  }

  loadSubjectsDivisionsAndSubDivisions = async (
    subjectId,
    divisionId,
    subDivisionId,
  ) => {
    const subjects = await this.fetchSubjects();

    if (!(subjects && subjects.length)) {
      return;
    }

    const selectedSubjectId = subjectId || subjects[0].id;

    this.setState({ selectedSubjectId });

    const divisions = await this.fetchDivisions(selectedSubjectId);

    if (!(divisions && divisions.length)) {
      return;
    }

    const selectedDivisionId = divisionId || divisions[0].id;

    this.setState({ selectedDivisionId });

    const subDivisions = await this.fetchSubDivisions(selectedDivisionId);

    if (!(subDivisions && subDivisions.length)) {
      return;
    }

    const selectedSubDivisionId = subDivisionId || subDivisions[0].id;

    this.setState({ selectedSubDivisionId });
  };

  fetchSubjects = async () => {
    this.setState({ isSubjectsLoading: true });

    let { data } = (await getSubjects().catch(showApiErrorMessage)) || {
      data: [],
    };

    this.setState({ subjects: data, isSubjectsLoading: false });

    return data;
  };

  fetchDivisions = async subjectId => {
    this.setState({ isDivisionsLoading: true });

    let { data } = (await getDivisions(subjectId).catch(
      showApiErrorMessage,
    )) || { data: [] };

    this.setState({ divisions: data, isDivisionsLoading: false });

    return data;
  };

  fetchSubDivisions = async divisionId => {
    this.setState({ isSubDivisionsLoading: true });

    let { data } = (await getSubDivisions(divisionId).catch(
      showApiErrorMessage,
    )) || { data: [] };

    this.setState({ subDivisions: data, isSubDivisionsLoading: false });

    return data;
  };

  handleOptionChange = event => {
    const { name, value } = (event && event.target) || {};

    if (name === selectorTypes.subject) {
      this.setState({
        selectedSubjectId: value,
        divisions: [],
        selectedDivisionId: '',
        subDivisions: [],
        selectedSubDivisionId: '',
      });

      this.fetchDivisions(value);
    } else if (name === selectorTypes.division) {
      this.setState({
        selectedDivisionId: value,
        subDivisions: [],
        selectedSubDivisionId: '',
      });

      this.fetchSubDivisions(value);
    } else if (name === selectorTypes.subDivision) {
      this.setState({
        selectedSubDivisionId: value,
      });
    }
  };

  deleteAllQuestions = () => {
    this.props.openModal({
      message: 'Are you sure you want to remove all questions?',
      handleClose: ({ isConfirmed }) => {
        if (!isConfirmed) {
          return;
        }

        this.setState({ questions: [] });
      },
    });
  };

  setQuestions = (questions = []) => this.setState({ questions });

  getFormattedQuestions = (questions = []) =>
    questions.map(({ id, ...questionProps }, index) => ({
      ...questionProps,
      questionNo: index + 1, // TODO: Maybe remove questionNo if it's no longer required in backend
    }));

  onSave = () => {
    const { questions, selectedSubDivisionId: subDivisionId } = this.state;

    if (!(questions && questions.length)) {
      return;
    }

    if (!subDivisionId) {
      return toast.error('Select a SubDivision');
    }

    for (let question of questions) {
      const validationMessage = getQuestionValidationMessage(question);

      if (validationMessage) {
        return toast.error(validationMessage);
      }
    }

    this.setState({ isAddingQuestions: true });

    savePracticeQuestions(subDivisionId, this.getFormattedQuestions(questions))
      .then(() => {
        toast.success('Successfully added questions', {
          position: toast.POSITION.TOP_CENTER,
        });

        this.setState({ isAddingQuestions: false });

        browserHistory.push(viewRoutes.subjects, {
          subjectId: this.state.selectedSubjectId,
          divisionId: this.state.selectedDivisionId,
          subDivisionId: this.state.selectedSubDivisionId,
        });
      })
      .catch(error => {
        showApiErrorMessage(error);

        this.setState({ isAddingQuestions: false });
      });
  };

  onViewQuestionsClick = () => {
    browserHistory.push(viewRoutes.viewPracticeQuestions, {
      subjectId: this.state.selectedSubjectId,
      divisionId: this.state.selectedDivisionId,
      subDivisionId: this.state.selectedSubDivisionId,
    });
  };

  render() {
    const { questions, isAddingQuestions, selectedSubDivisionId } = this.state;

    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                Subject / Division / Sub Division Selector
              </CardHeader>

              <CardBody>
                <Row>
                  {this.selectors &&
                    this.selectors.map(selector => (
                      <Col key={selector.type}>
                        <Selector
                          type={selector.type}
                          items={selector.getItems()}
                          selectedItemId={selector.getSelectedItemId()}
                          handleChange={this.handleOptionChange}
                        />
                      </Col>
                    ))}
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col>
            <Card>
              <CardHeader>
                <QuestionsHeader
                  questions={questions}
                  deleteAllQuestions={this.deleteAllQuestions}
                  selectedSubDivisionId={selectedSubDivisionId}
                  onViewQuestions={this.onViewQuestionsClick}
                />
              </CardHeader>

              <CardBody>
                <Questions
                  isPracticeQuestion={true}
                  openModal={this.props.openModal}
                  parentId={selectedSubDivisionId}
                  questions={questions}
                  setQuestions={this.setQuestions}
                />
              </CardBody>
            </Card>

            <Button
              color="primary"
              disabled={isAddingQuestions}
              onClick={this.onSave}
            >
              Save Question
            </Button>
          </Col>
        </Row>
      </div>
    );
  }
}

export default withConfirmModal(AddQuestion);
