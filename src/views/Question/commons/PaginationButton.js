import React from 'react';
import { Button } from 'reactstrap';

export const getPaginationButton = text => props => (
  <Button
    color={props && props.disabled ? 'secondary' : 'primary'}
    className="h-100 w-100"
    {...props}
  >
    {text}
  </Button>
);

export const NextPageButton = getPaginationButton('Next');

export const PreviousPageButton = getPaginationButton('Previous');
