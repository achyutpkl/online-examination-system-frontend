import { Button } from 'reactstrap';
import React, { Fragment } from 'react';

import GotoResourceButton from '../../../commons/GotoResourceButton/GotoResourceButton';

export default ({
  questions,
  deleteAllQuestions,
  selectedSubDivisionId,
  onViewQuestions,
}) =>
  questions && questions.length ? (
    <React.Fragment>
      <strong className="h5">Questions</strong>
      <div className="card-header-actions">
        <Button
          type="reset"
          size="sm"
          color="danger"
          onClick={deleteAllQuestions}
        >
          <i className="fa fa-ban" />
          Delete All Questions
        </Button>
      </div>
    </React.Fragment>
  ) : (
    <Fragment>
      <strong className="h5">Add Questions</strong>

      {selectedSubDivisionId ? (
        <div className="card-header-actions">
          <GotoResourceButton name="Get question template" className="mr-2" />

          <Button outline color="primary" onClick={onViewQuestions}>
            View questions
          </Button>
        </div>
      ) : null}
    </Fragment>
  );
