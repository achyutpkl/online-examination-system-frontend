import React from 'react';
import { Input, Label, FormGroup } from 'reactstrap';

import { upperCaseFirstLetter } from '../../../utils/stringUtils';

export default ({ type, items, selectedItemId, handleChange }) => (
  <FormGroup>
    <Label htmlFor={`${type}-select`}>{upperCaseFirstLetter(type)}</Label>
    <Input
      type="select"
      required
      name={type}
      id={`${type}-select`}
      value={selectedItemId}
      onChange={handleChange}
    >
      <option value={''} disabled hidden>
        {`Select a ${type}`}
      </option>
      {items && items.length ? (
        items.map(item => (
          <option key={item.id} value={item.id}>
            {item.title}
          </option>
        ))
      ) : (
        <option disabled>No options available</option>
      )}
    </Input>
  </FormGroup>
);
