import React, { PureComponent } from 'react';
import { Col, Row, Card, CardBody, CardHeader } from 'reactstrap';

import { showApiErrorMessage } from '../../utils/httpUtil';
import {
  getSubjects,
  getDivisions,
  getSubDivisions,
} from '../../services/SubjectService';

import Selector from './commons/Selector';

// TODO: Use a parent render props component for for selectors to reuse same in Subjects view
// TODO: Also use a common selector component for AddQuestion and this component
const selectorTypes = {
  subject: 'subject',
  division: 'division',
  subDivision: 'subDivision',
};

class SubDivSelector extends PureComponent {
  selectors = [
    {
      type: selectorTypes.subject,
      getItems: () => this.state.subjects,
      getSelectedItemId: () => this.state.selectedSubjectId,
    },
    {
      type: selectorTypes.division,
      getItems: () => this.state.divisions,
      getSelectedItemId: () => this.state.selectedDivisionId,
    },
    {
      type: selectorTypes.subDivision,
      getItems: () => this.state.subDivisions,
      getSelectedItemId: () => this.state.selectedSubDivisionId,
    },
  ];

  state = {
    subjects: [],
    divisions: [],
    subDivisions: [],
    selectedSubjectId: '',
    selectedDivisionId: '',
    selectedSubDivisionId: '',
    isSubjectsLoading: false,
    isDivisionsLoading: false,
    isSubDivisionsLoading: false,
  };

  componentDidMount() {
    const { subjectId, divisionId, subDivisionId } = (this.props.location &&
      this.props.location.state) || {
      subjectId: '',
      divisionId: '',
      subDivisionId: '',
    };

    this.loadSubjectsDivisionsAndSubDivisions(
      subjectId,
      divisionId,
      subDivisionId,
    );
  }

  loadSubjectsDivisionsAndSubDivisions = async (
    subjectId,
    divisionId,
    subDivisionId,
  ) => {
    const subjects = await this.fetchSubjects();

    if (!(subjects && subjects.length)) {
      return;
    }

    const selectedSubjectId = subjectId || subjects[0].id;

    this.setState({ selectedSubjectId });

    const divisions = await this.fetchDivisions(selectedSubjectId);

    if (!(divisions && divisions.length)) {
      return;
    }

    const selectedDivisionId = divisionId || divisions[0].id;

    this.setState({ selectedDivisionId });

    const subDivisions = await this.fetchSubDivisions(selectedDivisionId);

    if (!(subDivisions && subDivisions.length)) {
      return;
    }

    const selectedSubDivisionId = subDivisionId || subDivisions[0].id;

    this.setState({ selectedSubDivisionId });
  };

  fetchSubjects = async () => {
    this.setState({ isSubjectsLoading: true });

    let { data } = (await getSubjects().catch(showApiErrorMessage)) || {
      data: [],
    };

    this.setState({ subjects: data, isSubjectsLoading: false });

    return data;
  };

  fetchDivisions = async subjectId => {
    this.setState({ isDivisionsLoading: true });

    let { data } = (await getDivisions(subjectId).catch(
      showApiErrorMessage,
    )) || { data: [] };

    this.setState({ divisions: data, isDivisionsLoading: false });

    return data;
  };

  fetchSubDivisions = async divisionId => {
    this.setState({ isSubDivisionsLoading: true });

    let { data } = (await getSubDivisions(divisionId).catch(
      showApiErrorMessage,
    )) || { data: [] };

    this.setState({ subDivisions: data, isSubDivisionsLoading: false });

    return data;
  };

  handleOptionChange = event => {
    const { name, value } = (event && event.target) || {};

    if (name === selectorTypes.subject) {
      this.setState({
        selectedSubjectId: value,
        divisions: [],
        selectedDivisionId: '',
        subDivisions: [],
        selectedSubDivisionId: '',
      });

      this.fetchDivisions(value);
    } else if (name === selectorTypes.division) {
      this.setState({
        selectedDivisionId: value,
        subDivisions: [],
        selectedSubDivisionId: '',
      });

      this.fetchSubDivisions(value);
    } else if (name === selectorTypes.subDivision) {
      this.setState({
        selectedSubDivisionId: value,
      });
    }
  };

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                Subject / Division / Sub Division Selector
              </CardHeader>

              <CardBody>
                <Row>
                  {this.selectors &&
                    this.selectors.map(selector => (
                      <Col key={selector.type}>
                        <Selector
                          type={selector.type}
                          items={selector.getItems()}
                          selectedItemId={selector.getSelectedItemId()}
                          handleChange={this.handleOptionChange}
                        />
                      </Col>
                    ))}
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col>{this.props.children(this.state)}</Col>
        </Row>
      </div>
    );
  }
}

export default SubDivSelector;

export const withSubDivSelector = Component => {
  return class extends React.Component {
    render() {
      return (
        <SubDivSelector>
          {selectorProps => <Component {...this.props} {...selectorProps} />}
        </SubDivSelector>
      );
    }
  };
};
