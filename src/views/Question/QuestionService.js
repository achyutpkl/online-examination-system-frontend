import { apiUrls } from '../../utils/constants';
import { get, post, put, del } from '../../utils/httpUtil';

// TODO: Move this file into the service folder

export const getPracticeQuestions = async (
  subDivisionId,
  page = 0,
  size = 0,
) => {
  let response = await get(
    `${
      apiUrls.subDivision
    }/${subDivisionId}/practicequestions?page=${page}&size=${size}`,
  );

  return (response && response.data) || [];
};

export const savePracticeQuestions = async (subDivisionId, questions) => {
  let response = await post(
    `${apiUrls.practiceQuestions}/subdivisions/${subDivisionId}`,
    questions,
  );

  return (response && response.data) || [];
};

export const editPracticeQuestion = async (question = {}) => {
  let response = await put(
    `${apiUrls.practiceQuestions}/${question.id}`,
    question,
  );

  return (response && response.data) || {};
};

export const deletePracticeQuestion = async questionId => {
  let response = await del(`${apiUrls.practiceQuestions}/${questionId}`);

  return (response && response.data) || {};
};
