import qs from 'query-string';
import { toast } from 'react-toastify';
import React, { Component } from 'react';
import {
  Row,
  Col,
  Container,
  Card,
  CardBody,
  Form,
  Input,
  Button,
  InputGroup,
  InputGroupText,
  InputGroupAddon,
} from 'reactstrap';

import { apiUrls } from '../../utils/constants';
import browserHistory from '../../utils/history';
import keyImage from '../../assets/images/key.svg';
import { post, showApiErrorMessage } from '../../utils/httpUtil';

import CircularIconImage from '../../commons/CircularIconImage/CircularIconImage';

export default class ResetPassword extends Component {
  state = {
    token: '',
    password: '',
    confirmPassword: '',
    isSubmitting: false,
  };

  componentDidMount() {
    const { token = '' } = qs.parse(
      this.props.location && this.props.location.search,
    );

    this.setState({ token });
  }

  handleSubmit = e => {
    e.preventDefault();

    const { token, password, confirmPassword, isSubmitting } = this.state;

    if (isSubmitting || !(token && password && confirmPassword)) {
      return;
    }

    if (password !== confirmPassword) {
      return toast.error('Passwords do not match');
    }

    this.setState({ isSubmitting: true });

    post(apiUrls.resetPassword, { token, password, confirmPassword })
      .then(response => {
        const message =
          (response && response.data && response.data.message) ||
          'Password has been reset';

        toast.success(message, { position: toast.POSITION.BOTTOM_CENTER });

        setTimeout(() => {
          this.setState({ isSubmitting: false });
          browserHistory.push('/login');
        }, 1000);
      })
      .catch(error => {
        this.setState({ isSubmitting: false });

        showApiErrorMessage(error);
      });
  };

  handleInputChange = e => {
    const { name, value } = (e && e.target) || {};

    this.setState({ [name]: value });
  };

  render() {
    const { token, isSubmitting, password, confirmPassword } = this.state;

    return (
      <div className="flex-row align-items-center justify-content-center d-flex min-vh-100">
        <Container>
          <Row className="justify-content-center">
            <Col xs="12" md="8">
              <Card className="my-2">
                <CardBody className="text-center">
                  <CircularIconImage image={keyImage} />

                  <Form onSubmit={this.handleSubmit}>
                    <h3 className="mb-4">Reset Password</h3>

                    <InputGroup className="mb-2">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        type="password"
                        required
                        placeholder="New password"
                        name="password"
                        value={password}
                        onChange={this.handleInputChange}
                      />
                    </InputGroup>

                    <InputGroup className="mb-2">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        type="password"
                        required
                        placeholder="Confirm password"
                        name="confirmPassword"
                        value={confirmPassword}
                        onChange={this.handleInputChange}
                      />
                    </InputGroup>

                    <Button
                      block
                      disabled={
                        isSubmitting || !(token && password && confirmPassword)
                      }
                      type="submit"
                      color="primary"
                      className="px-4"
                    >
                      Done
                    </Button>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
