import { apiUrls } from '../../utils/constants';
import { get, put } from '../../utils/httpUtil';

export const getUserDetailsById = async id => {
  let userDetails = await get(`${apiUrls.user}/${id}`);

  return (userDetails && userDetails.data) || {};
};

export const updateUserDetailsById = async (id, params) => {
  let response = await put(`${apiUrls.user}/${id}`, params);

  return (response && response.data) || {};
};
