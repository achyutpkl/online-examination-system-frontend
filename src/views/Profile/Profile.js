import React from 'react';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Button,
  CardBody,
  CardHeader,
  CardFooter,
  InputGroup,
  InputGroupText,
  InputGroupAddon,
} from 'reactstrap';
import { toast } from 'react-toastify';

import { getItem } from '../../utils/localStorage';
import avatar from '../../assets/images/avatar.jpg';
import { localStorageKeys } from '../../utils/constants';
import { showApiErrorMessage } from '../../utils/httpUtil';
import { getUserDetailsById, updateUserDetailsById } from './profileService';

import './style.css';

class Profile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      address: '',
      userName: '',
      userId: '',
      lastName: '',
      firstName: '',
      dateOfBirth: '',
      mobileNumber: '',
      profileImage: null,
    };
  }

  handleOnChange = event => {
    const { name, value } = event.target;

    this.setState({
      [name]: value,
    });
  };

  // eslint-disable-next-line complexity
  handleOnSubmit = event => {
    event.preventDefault();

    const {
      email,
      userId,
      address,
      lastName,
      userName,
      firstName,
      middleName,
      dateOfBirth,
      mobileNumber,
    } = this.state;
    if (
      email.trim() &&
      userName.trim() &&
      lastName.trim() &&
      firstName.trim()
    ) {
      const result = updateUserDetailsById(userId, {
        email: email.trim(),
        address: address.trim(),
        userName: userName.trim(),
        lastName: lastName.trim(),
        firstName: firstName.trim(),
        middleName: (middleName && middleName.trim()) || '',
        dateOfBirth: (dateOfBirth && dateOfBirth.trim()) || null,
        mobileNumber: (mobileNumber && mobileNumber.trim()) || null,
      });

      result
        .then(res => {
          toast.success('Updated Successfully', {
            position: toast.POSITION.BOTTOM_CENTER,
          });
        })
        .catch(showApiErrorMessage);
    }
  };

  handleProfileImageUpload = event => {
    this.setState({
      profileImage: URL.createObjectURL(event.target.files[0]),
    });
  };

  componentDidMount() {
    const { userId } = getItem(localStorageKeys.oesUser);

    getUserDetailsById(userId)
      .then(res => {
        const {
          email,
          address,
          userName,
          lastName,
          firstName,
          middleName,
          dateOfBirth,
          mobileNumber,
        } = res;

        this.setState({
          email,
          userId,
          address,
          lastName,
          userName,
          firstName,
          middleName,
          dateOfBirth,
          mobileNumber,
        });
      })
      .catch(showApiErrorMessage);
  }

  render() {
    const {
      email,
      address,
      lastName,
      userName,
      firstName,
      middleName,
      dateOfBirth,
      mobileNumber,
      profileImage,
    } = this.state;

    return (
      <Card>
        <Form onSubmit={this.handleOnSubmit}>
          <CardHeader>Profile Information</CardHeader>
          <CardBody>
            <Row>
              <Col sm="2">
                <Row className="row-profile-avatar">
                  <div className="profile-avatar-wrapper">
                    <img
                      src={profileImage || avatar}
                      alt="Profile"
                      className="profile-avatar-img"
                    />
                  </div>
                </Row>
                <Row className="row-profile-avatar">
                  <div className="addMoreButton">
                    <label
                      className="btn  btn-primary"
                      htmlFor="profileImageChange"
                    >
                      Change
                    </label>
                    <Input
                      type="file"
                      id="profileImageChange"
                      style={{ display: 'none' }}
                      onChange={this.handleProfileImageUpload}
                    />
                  </div>
                </Row>
              </Col>
              <Col>
                <InputGroup className="mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>First Name*</InputGroupText>
                  </InputGroupAddon>
                  <Input
                    required
                    type="text"
                    name="firstName"
                    value={firstName}
                    placeholder="First Name"
                    autoComplete="firstname"
                    onChange={this.handleOnChange}
                  />
                </InputGroup>
                <InputGroup className="mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>Middle Name</InputGroupText>
                  </InputGroupAddon>
                  <Input
                    type="text"
                    name="middleName"
                    value={middleName}
                    placeholder="Middle Name"
                    autoComplete="middlename"
                    onChange={this.handleOnChange}
                  />
                </InputGroup>
                <InputGroup className="mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>Last Name*</InputGroupText>
                  </InputGroupAddon>
                  <Input
                    required
                    type="text"
                    name="lastName"
                    value={lastName}
                    placeholder="Last Name"
                    autoComplete="lastname"
                    onChange={this.handleOnChange}
                  />
                </InputGroup>
                <InputGroup className="mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>Date Of Birth</InputGroupText>
                  </InputGroupAddon>
                  <Input
                    type="date"
                    name="dateOfBirth"
                    value={dateOfBirth}
                    placeholder="dateOfBirth"
                    autoComplete="off"
                    onChange={this.handleOnChange}
                  />
                </InputGroup>
                <InputGroup className="mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>Mobile Number</InputGroupText>
                  </InputGroupAddon>
                  <Input
                    type="number"
                    name="mobileNumber"
                    value={mobileNumber}
                    placeholder="Mobile Number"
                    autoComplete="mobile"
                    onChange={this.handleOnChange}
                  />
                </InputGroup>
                <InputGroup className="mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>User Name*</InputGroupText>
                  </InputGroupAddon>
                  <Input
                    required
                    type="text"
                    name="userName"
                    value={userName}
                    placeholder="User Name"
                    autoComplete="userName"
                    onChange={this.handleOnChange}
                  />
                </InputGroup>
                <InputGroup className="mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>Email*</InputGroupText>
                  </InputGroupAddon>
                  <Input
                    required
                    type="email"
                    name="email"
                    value={email}
                    placeholder="Email"
                    autoComplete="email"
                    onChange={this.handleOnChange}
                  />
                </InputGroup>
                <InputGroup className="mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>Address</InputGroupText>
                  </InputGroupAddon>
                  <Input
                    multiple
                    type="text"
                    name="address"
                    value={address}
                    placeholder="Address"
                    autoComplete="address"
                    onChange={this.handleOnChange}
                  />
                </InputGroup>
              </Col>
            </Row>
          </CardBody>
          <CardFooter>
            <Button color="primary" type="submit">
              Save
            </Button>
          </CardFooter>
        </Form>
      </Card>
    );
  }
}

export default Profile;
