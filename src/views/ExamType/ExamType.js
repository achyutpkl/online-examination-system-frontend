import {
  Col,
  Row,
  Card,
  Form,
  Label,
  Input,
  Button,
  CardBody,
  FormGroup,
  CardFooter,
  CardHeader,
} from 'reactstrap';

import './index.css';
import React, { PureComponent } from 'react';

// student if the prices are same name-student and guest-student if we have seperate pricing

class ExamType extends PureComponent {
  constructor(props) {
    super(props);
    this.formRef = React.createRef();
  }
  state = {
    priceWithRole: [],

    role: '',
    mode: '',
    price: '',
    discount: '',
    isOpen: false,
    roleType: ['new student', 'old student'],
  };

  initialState = this.state;

  componentDidMount = () => {
    this.setState({
      role: this.state.roleType[0],
    });
  };

  render() {
    return (
      <div className="animated fadeIn">
        <Row className="d-flex flex-row justify-content-center align-items-center">
          <Col md="12">
            <Row>
              <Col md="8">
                <Card>
                  <CardBody>
                    <Row>
                      <Col>
                        <Card>
                          <CardHeader>Exam Type</CardHeader>
                          {this._renderExamTypeFormSection()}
                          <CardBody>
                            <div />
                          </CardBody>
                        </Card>
                      </Col>

                      <Col>
                        <Card>
                          <CardHeader>
                            <strong> Add Price For Other Role</strong>
                          </CardHeader>
                          <CardBody>{this._renderForm()}</CardBody>
                        </Card>
                      </Col>
                    </Row>
                  </CardBody>
                  <CardFooter>
                    <Button
                      block
                      color="primary"
                      onClick={this._onSave}
                      className="text-center mb-4 px-4"
                    >
                      Save Exam Type
                    </Button>
                  </CardFooter>
                </Card>
              </Col>

              <Col>
                <Card>
                  <CardHeader>
                    <strong> Accessibility</strong>
                  </CardHeader>
                  <CardBody>{this._renderAccessibilitySection()}</CardBody>
                </Card>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }

  _renderForm = () => {
    const { role, price, discount } = this.state;

    return (
      <Row>
        <Col className="d-flex flex-column">
          <div>
            <Form ref={this.formRef} id="addRolePriceForm">
              <FormGroup row className="m-1">
                <Col md="2">
                  <Label htmlFor="studentRole">Role</Label>
                </Col>
                <Col xs="12" md="10">
                  <Input
                    type="select"
                    id="studentRole"
                    name="role"
                    value={role}
                    onChange={this._handleInputChange}
                    required
                  >
                    {this.state.roleType &&
                      this.state.roleType.map(role => {
                        return <option key={role}>{role}</option>;
                      })}
                  </Input>
                </Col>
              </FormGroup>
              <FormGroup row className="m-1">
                <Col md="2">
                  <Label htmlFor="examPrice">Price</Label>
                </Col>
                <Col xs="12" md="10">
                  <Input
                    type="number"
                    id="examPrice"
                    name="price"
                    value={price}
                    onChange={this._handleInputChange}
                    required
                  />
                </Col>
              </FormGroup>
              <FormGroup row className="m-1">
                <Col md="2">
                  <Label htmlFor="examDiscount">Discount</Label>
                </Col>
                <Col xs="12" md="10">
                  <Input
                    onChange={this._handleInputChange}
                    type="number"
                    id="examDiscount"
                    name="discount"
                    value={discount}
                    required
                  />
                </Col>
              </FormGroup>
            </Form>
          </div>
          <Button
            color="primary"
            onClick={this._handleAddPrice}
            className="mt-4 mx-auto"
          >
            <i className="fa fa-plus-circle px-2" />
            Add Price For Role
          </Button>
        </Col>
      </Row>
    );
  };

  // render exam type form
  _renderExamTypeFormSection = () => {
    return (
      <React.Fragment>
        <Form>
          <FormGroup row className="m-1 pt-4">
            <Col md="2">
              <Label htmlFor="examTypeTitle">Exam Type</Label>
            </Col>
            <Col xs="12" md="10">
              <Input
                type="text"
                id="examTypeTitle"
                name="examTypeTitle"
                placeholder="Enter Exam Type"
                autoComplete="off"
              />
            </Col>
          </FormGroup>
          <hr />
          <FormGroup row className="m-1 mt-2">
            <Col md="2">
              <Label htmlFor="examTypeDescription">Exam Description</Label>
            </Col>
            <Col xs="12" md="10">
              <Input
                type="text"
                id="examTypeDescription"
                name="examTypeDescription"
                placeholder="Enter Exam Type Description"
                autoComplete="off"
              />
            </Col>
          </FormGroup>
        </Form>
      </React.Fragment>
    );
  };
  // render accessibility form
  _renderAccessibilitySection = () => {
    const { priceWithRole } = this.state;

    return (
      <React.Fragment>
        <Row>
          {priceWithRole &&
            priceWithRole.map((val, index) => (
              <Col
                xs="12"
                key={index}
                className="border-bottom border-warning mt-3"
              >
                <Row>
                  <Col xs="12" sm="10">
                    <Card className="border-primary p-1">
                      <FormGroup row className="m-1">
                        <Col md="2">
                          <Label htmlFor="studentRole">Role</Label>
                        </Col>
                        <Col xs="12" md="10">
                          <Input
                            type="text"
                            id="studentRole"
                            name="studentRole"
                            value={val.role}
                          />
                        </Col>
                      </FormGroup>
                      <FormGroup row className="m-1">
                        <Col md="2">
                          <Label htmlFor="examPrice">Price</Label>
                        </Col>
                        <Col xs="12" md="10">
                          <Input
                            type="text"
                            id="examPrice"
                            name="examPrice"
                            value={val.price}
                          />
                        </Col>
                      </FormGroup>
                      <FormGroup row className="m-1">
                        <Col md="2">
                          <Label htmlFor="examDiscount">Discount</Label>
                        </Col>
                        <Col xs="12" md="10">
                          <Input
                            type="text"
                            id="examDiscount"
                            name="examDiscount"
                            value={val.discount}
                          />
                        </Col>
                      </FormGroup>
                    </Card>
                  </Col>
                  <Col xs="12" sm="2" className="p-0 ml-0">
                    <Button color="success" className="m-1">
                      <i className="fa fa-pencil" />
                    </Button>
                    <Button color="danger" className="m-1">
                      <i className="fa fa-trash" />
                    </Button>
                  </Col>
                </Row>
              </Col>
            ))}
        </Row>
      </React.Fragment>
    );
  };

  //  handle input change
  _handleInputChange = e => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  _resetForm = () => {
    this.setState({
      price: '',
      role: this.state.roleType[0],
      discount: '',
    });
  };

  // reset state to previous state
  _resetState = () => {
    this.setState(this.initialState);
  };

  // handle submit we will do api call for now i will push to the array with Price with role
  _handleAddPrice = () => {
    const { role, price, discount } = this.state;
    let newExamType;
    if (role && price) {
      newExamType = {
        role,
        price,
        discount,
      };
    }

    if (newExamType) {
      this.setState(prevState => ({
        ...prevState,
        priceWithRole: [...prevState.priceWithRole, newExamType],
      }));
    }
    this._resetForm();
  };
}

export default ExamType;
