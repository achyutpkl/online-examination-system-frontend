import React from 'react';
import {
  Modal,
  Form,
  Label,
  Input,
  Button,
  ModalBody,
  FormGroup,
  ModalHeader,
  ModalFooter,
} from 'reactstrap';

export default ({
  role,
  mode,
  price,
  isOpen,
  discount,
  roleType,
  modalHeader,
  handleToggle,
  handleCancel,
  handleSubmit,
  handleInputChange,
}) => (
  <Modal isOpen={isOpen} backdrop="static" className={'modal-lg'}>
    <ModalHeader toggle={handleToggle}>{modalHeader}</ModalHeader>
    <ModalBody>
      <Form>
        <FormGroup>
          <Label htmlFor="studentRoleSelect">Student Role*</Label>
          <Input
            type="select"
            id="studentRoleSelect"
            name="role"
            value={role}
            onChange={handleInputChange}
          >
            {roleType &&
              roleType.map(type => (
                <option key={type} value={type}>
                  {type}
                </option>
              ))}
          </Input>
        </FormGroup>
        <FormGroup>
          <Label htmlFor="examTypePrice">Price*</Label>
          <Input
            type="number"
            id="examTypePrice"
            name="price"
            placeholder="Enter Price"
            value={price}
            onChange={handleInputChange}
          />
        </FormGroup>

        <FormGroup>
          <Label htmlFor="examTypeDiscount">Discount</Label>
          <Input
            type="number"
            id="examTypeDiscount"
            name="discount"
            placeholder="Enter Discount"
            value={discount}
            onChange={handleInputChange}
          />
        </FormGroup>
      </Form>
    </ModalBody>
    <ModalFooter>
      <Button color="primary" onClick={handleSubmit}>
        {mode === 'edit' ? 'Edit' : 'Add'}
      </Button>
      <Button color="secondary" onClick={handleCancel}>
        {mode === 'edit' ? 'Cancel' : 'Reset'}
      </Button>
    </ModalFooter>
  </Modal>
);
