import { format } from 'date-fns';
import React, { Fragment } from 'react';
import DatePicker from 'react-datepicker';
import {
  Form,
  Input,
  Button,
  InputGroup,
  InputGroupText,
  InputGroupAddon,
  UncontrolledTooltip,
} from 'reactstrap';

import './styles.scss';

import { userRole } from '../../../utils/constants';
import { SAVE, CREATE_ACCOUNT } from '../../../lang/en';

import PhoneInput from '../../../commons/PhoneInput/PhoneInput';

class RegisterFrom extends React.Component {
  constructor(props) {
    super(props);

    const {
      roles,
      email,
      address,
      password,
      lastName,
      userName,
      firstName,
      middleName,
      collegeName,
      mobileNumber,
      dateOfBirth,
    } = this.props.data || {};

    const dob =
      (Array.isArray(dateOfBirth) &&
        new Date(dateOfBirth[0], dateOfBirth[1] - 1, dateOfBirth[2], 0)) ||
      null;

    if (this.props.data) {
      this.state = {
        dob: dob,
        role: roles[0],
        email: email,
        address: address,
        password: password,
        lastName: lastName,
        userName: userName,
        firstName: firstName,
        collegeName: collegeName,
        middleName: middleName || '',
        repeatPassword: password,
        mobileNumber: mobileNumber,
        registrationNumber: '',
      };
    } else {
      this.state = {
        dob: '',
        email: '',
        address: '',
        password: '',
        lastName: '',
        userName: '',
        firstName: '',
        middleName: '',
        collegeName: '',
        mobileNumber: '',
        repeatPassword: '',
        registrationNumber: '',
        role: userRole.student,
      };
    }

    this.roleType = {
      Student: userRole.student,
      Admin: userRole.admin,
    };
  }

  handleOnChange = event => {
    const { name, value } = event.target;

    this.setState({
      [name]: value,
    });
  };

  handleSubmit = event => {
    event.preventDefault();

    this.props.handleSubmit({
      ...this.state,
      userName: this.state.email,
      dob: format(this.state.dob, 'YYYY-MM-DD'),
    });
  };

  render() {
    const {
      dob,
      role,
      email,
      address,
      lastName,
      password,
      firstName,
      middleName,
      collegeName,
      mobileNumber,
      repeatPassword,
      registrationNumber,
    } = this.state;
    const { isAdmin, isEdit } = this.props;

    return (
      <Form onSubmit={this.handleSubmit} className="register-form">
        {isEdit ? (
          <h1>Edit user information</h1>
        ) : (
          <Fragment>
            <h1>New Registration</h1>
            <p className="text-muted">
              {isAdmin ? 'Create new account' : 'Create a student account'}
            </p>
          </Fragment>
        )}

        <InputGroup className="mb-3">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>F</InputGroupText>
          </InputGroupAddon>
          <Input
            required
            type="text"
            name="firstName"
            value={firstName}
            placeholder="First Name"
            autoComplete="firstname"
            onChange={this.handleOnChange}
          />
        </InputGroup>
        <InputGroup className="mb-3">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>M</InputGroupText>
          </InputGroupAddon>
          <Input
            type="text"
            name="middleName"
            value={middleName}
            placeholder="Middle Name"
            autoComplete="middlename"
            onChange={this.handleOnChange}
          />
        </InputGroup>
        <InputGroup className="mb-3">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>L</InputGroupText>
          </InputGroupAddon>
          <Input
            required
            type="text"
            name="lastName"
            value={lastName}
            placeholder="Last Name"
            autoComplete="lastname"
            onChange={this.handleOnChange}
          />
        </InputGroup>
        <InputGroup className="mb-3">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className="icon-calendar" />
            </InputGroupText>
          </InputGroupAddon>
          <DatePicker
            className="form-control date-input"
            required
            name="dob"
            selected={dob || null}
            placeholderText="Date of birth"
            autoComplete="off"
            onChange={value =>
              this.handleOnChange({ target: { name: 'dob', value } })
            }
            showMonthDropdown
            showYearDropdown
            dropdownMode="select"
          />
        </InputGroup>
        <InputGroup className="mb-3">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className="icon-phone" />
            </InputGroupText>
          </InputGroupAddon>
          <PhoneInput
            name="mobileNumber"
            value={mobileNumber}
            placeholder="Mobile Number"
            autoComplete="mobile"
            onChange={this.handleOnChange}
          />
        </InputGroup>
        {/* <InputGroup className="mb-3">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>U</InputGroupText>
          </InputGroupAddon>
          <UserNameInput
            required
            type="text"
            name="userName"
            value={userName}
            disabled={isEdit}
            placeholder="User Name"
            autoComplete="userName"
            onChange={this.handleOnChange}
          />
        </InputGroup> */}
        <InputGroup className="mb-3">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>@</InputGroupText>
          </InputGroupAddon>
          <Input
            required
            type="email"
            name="email"
            value={email}
            disabled={isEdit}
            placeholder="Email"
            autoComplete="email"
            onChange={this.handleOnChange}
          />
        </InputGroup>
        <InputGroup className="mb-3">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>C</InputGroupText>
          </InputGroupAddon>
          <Input
            required
            type="text"
            name="collegeName"
            value={collegeName}
            placeholder="College Name"
            autoComplete="collegeName"
            onChange={this.handleOnChange}
          />
        </InputGroup>
        <InputGroup className="mb-3">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className="fa fa-address-card-o" />
            </InputGroupText>
          </InputGroupAddon>
          <Input
            multiple
            required
            type="text"
            name="address"
            value={address}
            placeholder="Address"
            autoComplete="address"
            onChange={this.handleOnChange}
          />
        </InputGroup>
        {!isEdit && (
          <InputGroup className="mb-3">
            <InputGroupAddon addonType="prepend">
              <InputGroupText>
                <i className="icon-lock" />
              </InputGroupText>
            </InputGroupAddon>
            <Input
              required
              type="password"
              name="password"
              value={password}
              placeholder="Password"
              autoComplete="new-password"
              onChange={this.handleOnChange}
            />
          </InputGroup>
        )}
        {!isEdit && (
          <InputGroup className="mb-4">
            <InputGroupAddon addonType="prepend">
              <InputGroupText>
                <i className="icon-lock" />
              </InputGroupText>
            </InputGroupAddon>
            <Input
              required
              type="password"
              name="repeatPassword"
              value={repeatPassword}
              placeholder="Repeat password"
              autoComplete="new-password"
              onChange={this.handleOnChange}
            />
          </InputGroup>
        )}
        <InputGroup className="mb-3">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>R</InputGroupText>
          </InputGroupAddon>
          <Input
            type="text"
            name="registrationNumber"
            value={registrationNumber}
            placeholder="Registration Number"
            autoComplete="registrationNumber"
            onChange={this.handleOnChange}
          />
          <InputGroupAddon addonType="prepend">
            <InputGroupText id="registrationToolTip">
              <i className="icon-question" />
            </InputGroupText>
          </InputGroupAddon>
          <UncontrolledTooltip placement="right" target="registrationToolTip">
            If you are already registered to NAME institute then enter your
            registration number
          </UncontrolledTooltip>
        </InputGroup>
        {isAdmin && (
          <InputGroup className="mb-3">
            <InputGroupAddon addonType="prepend">
              <InputGroupText>
                <i className="fa fa-user" />
              </InputGroupText>
            </InputGroupAddon>

            <Input
              name="role"
              value={role}
              id="userRole"
              type="select"
              placeholder="User Role"
              autoComplete="userRole"
              onChange={this.handleOnChange}
              required
            >
              {Object.keys(this.roleType).map(item => (
                <option key={item} value={this.roleType[item]}>
                  {item}
                </option>
              ))}
            </Input>
          </InputGroup>
        )}

        <Button type="submit" color="success" block>
          {isEdit ? SAVE : CREATE_ACCOUNT}
        </Button>
      </Form>
    );
  }
}

export default RegisterFrom;
