import {
  Col,
  Row,
  Card,
  Button,
  CardBody,
  Container,
  CardFooter,
  CardHeader,
} from 'reactstrap';
import { toast } from 'react-toastify';
import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';

import RegisterFrom from './common/RegisterForm';
import { getItem } from '../../utils/localStorage';
import { registerStudentUser } from './RegisterService';
import { canEnter } from '../../commons/AuthLayout/utils';
import { getHttpErrorMessage } from '../../utils/httpUtil';
import { localStorageKeys, userRole } from '../../utils/constants';

class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      registerSuccessful: false,
    };
  }

  // eslint-disable-next-line complexity
  handleSubmit = formData => {
    let {
      dob,
      email,
      address,
      lastName,
      password,
      userName,
      firstName,
      collegeName,
      mobileNumber,
      repeatPassword,
      registrationNumber,
    } = formData;

    if (mobileNumber.trim() && mobileNumber.trim().length !== 10) {
      toast.error('Mobile number must be of length 10', {
        position: toast.POSITION.BOTTOM_CENTER,
      });
    } else if (
      email.trim() &&
      address.trim() &&
      userName.trim() &&
      lastName.trim() &&
      password.trim() &&
      firstName.trim() &&
      collegeName.trim() &&
      repeatPassword.trim() &&
      password === repeatPassword
    ) {
      const result = registerStudentUser({
        email: email.trim(),
        dateOfBirth: dob.trim(),
        address: address.trim(),
        userName: userName.trim(),
        lastName: lastName.trim(),
        password: password.trim(),
        firstName: firstName.trim(),
        collegeName: collegeName.trim(),
        mobileNumber: mobileNumber.trim(),
        registrationNumber: registrationNumber.trim(),
      });
      result
        .then(res => {
          setTimeout(() => {
            this.setState({
              registerSuccessful: true,
            });
          }, 500);

          toast.success('Registered Successfully', {
            position: toast.POSITION.BOTTOM_CENTER,
          });
        })
        .catch(err => {
          toast.error(getHttpErrorMessage(err), {
            position: toast.POSITION.BOTTOM_CENTER,
          });
        });
    }
  };

  render() {
    const { registerSuccessful } = this.state;
    const userDetail = getItem(localStorageKeys.oesUser);
    const isAdmin = userDetail && canEnter(userDetail.roles, [userRole.admin]);
    const redirect = registerSuccessful && !isAdmin && <Redirect to="login" />;

    return (
      <div className="flex-row align-items-center d-flex min-vh-100">
        {redirect}
        <Container>
          <Row className="justify-content-center ">
            <Col md="9" lg="7" xl="6">
              <Card className="mx-4 my-2">
                <CardHeader>
                  <div className="logo">
                    <div className="group-box">
                      <span className="entrance-cart">
                        <span className="absolute-color">ENTRANCE</span> CART
                      </span>
                      <br />
                      <span className="supported-by-name">
                        Supported by NAME
                      </span>
                    </div>
                  </div>
                </CardHeader>
                <CardBody className="p-4 ">
                  <RegisterFrom
                    isAdmin={isAdmin}
                    handleSubmit={this.handleSubmit}
                  />
                </CardBody>
                <CardFooter className="p-1">
                  <Row>
                    <Col xs="12" className="text-center">
                      <Link to="/login">
                        <Button color="link">
                          <span>Go to Login Page</span>
                        </Button>
                      </Link>
                    </Col>
                  </Row>
                </CardFooter>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Register;
