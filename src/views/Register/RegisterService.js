import { apiUrls } from '../../utils/constants';
import { post, put } from '../../utils/httpUtil';

export const registerStudentUser = async params => {
  let register = await post(`${apiUrls.register}`, params);

  return (register && register.data) || {};
};

export const addUser = async params => {
  let register = await post(`${apiUrls.addUser}`, params);

  return (register && register.data) || {};
};

export const editUser = async (id, params) => {
  let response = await put(`${apiUrls.user}/${id}`, params);

  return (response && response.data) || {};
};
