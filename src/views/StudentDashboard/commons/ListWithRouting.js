import React from 'react';
import { Link } from 'react-router-dom';
import { ListGroup, ListGroupItem } from 'reactstrap';

export default ({ items, routePath, render, keyProp }) => (
  <ListGroup>
    {items &&
      items.map((item, i) => {
        if (routePath) {
          return (
            <ListGroupItem
              tag={Link}
              to={`${routePath}/${item[keyProp] || item.id}`}
              key={item.id}
              action
            >
              {render(item)}
            </ListGroupItem>
          );
        }

        return <ListGroupItem key={i}>{render(item)}</ListGroupItem>;
      })}
  </ListGroup>
);
