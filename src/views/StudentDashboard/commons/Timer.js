import React from 'react';
import { CardTitle } from 'reactstrap';
import { getDifferenceInSeconds } from '../../../utils/dateUtils';

class Timer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      diffTime: getDifferenceInSeconds(props.startDate, props.endDate),
    };
  }

  componentDidMount() {
    setInterval(() => {
      this.setState({
        diffTime: this.state.diffTime - 1,
      });
    }, 1000);
  }

  formatTime = diffTime => {
    let tempTime = diffTime;
    const sec = tempTime % 60;
    tempTime = parseInt(tempTime / 60);
    const min = tempTime % 60;
    tempTime = parseInt(tempTime / 60);
    const hr = tempTime;

    return { hr, min, sec };
  };

  render() {
    const { hr, min, sec } = this.formatTime(this.state.diffTime);

    return (
      <CardTitle className="cardTitle">{`${hr} hr: ${min} min: ${sec} sec`}</CardTitle>
    );
  }
}

export default Timer;
