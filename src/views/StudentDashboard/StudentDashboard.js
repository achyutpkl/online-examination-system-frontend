import React from 'react';
import { Row, Col, Card, CardTitle, CardFooter, CardHeader } from 'reactstrap';

import browserHistory from '../../utils/history';
import { viewRoutes } from '../../utils/constants';
import { showApiErrorMessage } from '../../utils/httpUtil';
import { getSubjects } from '../../services/SubjectService';

import { getExamTypes, getExams } from '../../services/ExamService';

import './style.css';
import { sortListByDate, isPastDateTime } from '../../utils/dateUtils';
import ExamListItem from '../StudentExam/commons/ExamListItem';
import Timer from './commons/Timer';

const cardPrimaryColor = ['primary', 'success', 'warning', 'danger'];
const cardSecondaryColor = [
  'bg-info',
  'bg-teal',
  'bg-orange',
  'bg-google-plus',
];
const subjectIcon = {
  english: 'book',
  health: 'user-md',
  chemistry: 'flask',
  physics: 'space-shuttle',
  botany: 'leaf',
  zoology: 'bug',
  math: 'percent',
  'applied science': 'stethoscope',
};

class StudentDashBoard extends React.Component {
  state = {
    subjects: [],
    examTypes: [],
    isLoadingSubjects: false,
    isLoadingExamTypes: false,
    isSaturdayExamLoading: false,
    saturdayExams: null,
    isTestExamLoading: false,
    testExams: [],
  };

  componentDidMount() {
    this.fetchExamTypes();
    this.fetchSubjects();
  }

  fetchSubjects = async () => {
    this.setState({ isLoadingSubjects: true });

    let { data } = (await getSubjects().catch(showApiErrorMessage)) || {
      data: [],
    };

    this.setState({ subjects: data, isLoadingSubjects: false });
  };

  fetchExamTypes = async () => {
    this.setState({ isLoadingExamTypes: true });

    let data = (await getExamTypes().catch(showApiErrorMessage)) || [];
    this.setState({ examTypes: data, isLoadingExamTypes: false });

    const saturdayExamType = [];
    const testExamType = [];
    data.forEach(item => {
      if (item.title.toLowerCase().includes('saturday')) {
        saturdayExamType.push(item.title);
      } else if (item.title.toLowerCase().includes('daily test')) {
        testExamType.push(item.title);
      }
    });
    this.fetchSaturdayExams(
      saturdayExamType.length > 0 ? saturdayExamType[0] : undefined,
    );
    this.fetchTestExams(testExamType.length > 0 ? testExamType[0] : undefined);
  };

  fetchSaturdayExams = async (examType = 'SATURDAY') => {
    this.setState({ isSaturdayExamLoading: true });
    const { data } = (await getExams({ examType: examType }).catch(
      showApiErrorMessage,
    )) || {
      data: [],
    };

    let sortedExams = sortListByDate(
      data.filter(item => item.examStatus !== 'COMPLETED'),
      'startTime',
    );
    this.setState({
      saturdayExams: sortedExams[0],
      isSaturdayExamLoading: false,
    });
  };

  fetchTestExams = async (examType = 'Daily Test') => {
    this.setState({ isSaturdayExamLoading: true });
    const { data } = (await getExams({ examType: examType }, 5).catch(
      showApiErrorMessage,
    )) || {
      data: [],
    };

    let sortedExams = sortListByDate(data, 'startTime');
    this.setState({
      testExams: sortedExams,
      isTestExamLoading: false,
    });
  };

  handleViewMoreExams = () => {
    browserHistory.push(viewRoutes.studentExamList);
  };

  handleSelect = (baseUrl, id) => {
    browserHistory.push(`${baseUrl}${id}`);
  };

  getIconForSubject = subjectname => {
    const keys = Object.keys(subjectIcon);
    const index = keys.findIndex(key => {
      return subjectname.toLowerCase().includes(key);
    });
    if (index !== -1) {
      return subjectIcon[keys[index]];
    }

    return false;
  };

  getExamTypes = (
    examTypes,
    baseUrl,
    key,
    viewTxt = 'View',
    icon = 'file-archive-o',
  ) => {
    let items = [];
    const rows = Math.ceil(examTypes.length / 4);
    for (let i = 0; i < rows; i++) {
      let colItems = [];
      for (let j = 0; j < 4; j++) {
        if (i * 4 + j >= examTypes.length) {
          colItems.push(<Col />);
          continue;
        }

        colItems.push(
          <Col key={i + '' + j} className="col-12 col-sm-6 col-lg-3">
            <Card
              color={cardPrimaryColor[j]}
              onClick={() =>
                this.handleSelect(baseUrl, examTypes[i * 4 + j][key])
              }
              style={{ cursor: 'pointer' }}
            >
              <CardHeader>
                <i
                  className={`fa fa-${
                    viewTxt.toLowerCase().includes('subject')
                      ? this.getIconForSubject(examTypes[i * 4 + j].title) ||
                        icon
                      : icon
                  } ${cardSecondaryColor[j]} p-3 font-2xl mr-3 float-left`}
                />
                <div
                  className={`
                  ${
                    true ? '' : 'text-muted'
                  } text-uppercase font-weight-bold font-xs`}
                >
                  {examTypes[i * 4 + j].title}
                </div>
              </CardHeader>
              <CardFooter>
                <div className="font-weight-bold font-xs btn-block text-muted">
                  {viewTxt}
                  <i className="fa fa-angle-right float-right font-lg" />
                </div>
              </CardFooter>
            </Card>
          </Col>,
        );
      }
      items.push(<Row key={i}>{colItems.map(item => item)}</Row>);
    }

    return <div>{items.map(item => item)}</div>;
  };

  handleSelectSaturdayExam = id => {
    browserHistory.push(viewRoutes.studentExamDetails.replace(':id', id));
  };

  renderSaturdayExam = () => {
    const { isSaturdayExamLoading, saturdayExams } = this.state;

    if (!saturdayExams) {
      return null;
    }

    return (
      <Card body>
        <CardTitle className="cardTitle">Upcoming Saturday Exam</CardTitle>
        {isSaturdayExamLoading ? (
          'Loading...'
        ) : (
          <Row>
            <Col xs="12" sm="6" lg="3">
              <ExamListItem
                {...saturdayExams}
                handleSelect={this.handleSelectSaturdayExam}
              />
            </Col>
            {isPastDateTime(new Date(saturdayExams.startTime)) ? null : (
              <Col xs="12" sm="6" lg="3">
                <Timer
                  startDate={new Date(saturdayExams.startTime)}
                  endDate={new Date()}
                />
              </Col>
            )}
          </Row>
        )}
      </Card>
    );
  };

  renderTestExam = () => {
    const { isTestExamLoading, testExams } = this.state;

    if (testExams.length <= 0) {
      return null;
    }

    return (
      <Card body>
        <CardTitle className="cardTitle">Daily Test Exams</CardTitle>
        {isTestExamLoading ? (
          'Loading...'
        ) : (
          <Row>
            {testExams.map(exam => (
              <Col name={exam.id} key={exam.id} xs="12" sm="6" lg="3">
                <ExamListItem
                  {...exam}
                  handleSelect={this.handleSelectSaturdayExam}
                />
              </Col>
            ))}
          </Row>
        )}
      </Card>
    );
  };

  render() {
    const {
      subjects,
      examTypes,
      isLoadingSubjects,
      isLoadingExamTypes,
    } = this.state;

    return (
      <div>
        <Card body>
          <CardTitle className="cardTitle">Subjects</CardTitle>
          {isLoadingSubjects
            ? 'Loading...'
            : this.getExamTypes(
                subjects,
                'practice/',
                'id',
                'View Subject',
                'book',
              )}
        </Card>
        {this.renderSaturdayExam()}
        <Card body>
          <CardTitle className="cardTitle">Exams Types</CardTitle>
          {isLoadingExamTypes
            ? 'Loading...'
            : this.getExamTypes(examTypes, 'exams/', 'title', 'View Exams')}
        </Card>
        {this.renderTestExam()}
      </div>
    );
  }
}

export default StudentDashBoard;
