import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';

import 'react-app-polyfill/ie9'; // For IE 9-11 support
import 'react-app-polyfill/ie11'; // For IE 11 support
import './polyfill';

import App from './views/App/App';
import history from './utils/history';
import configureStore from './configureStore';
import * as serviceWorker from './serviceWorker';
import ErrorBoundary from './commons/ErrorBoundary';

import './styles.scss';
import { initializeAnalytics } from './config/initialConfigurations';

require('dotenv').config();

initializeAnalytics();

const store = configureStore({});

const rootElement = document.getElementById('root');

const render = Component =>
  ReactDOM.render(
    <ErrorBoundary>
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Component />
        </ConnectedRouter>
      </Provider>
    </ErrorBoundary>,
    rootElement,
  );

render(App);

if (module.hot) {
  module.hot.accept('./views/App/App', () => {
    const NextApp = require('./views/App/App').default;

    render(NextApp);
  });
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
