import * as Sentry from '@sentry/browser';
import ReactGA from 'react-ga';

export const initializeAnalytics = () => {
  if (process.env.REACT_APP_BUILD) {
    _initializeSentry();
    _initializeGoogleAnalytics();
  }
};

const _initializeSentry = () => {
  Sentry.init({
    dsn: process.env.REACT_APP_SENTRY_DNS,
  });
};

const _initializeGoogleAnalytics = () => {
  if (process.env.REACT_APP_ENV === 'prod') {
    ReactGA.initialize(process.env.REACT_APP_GOOGLE_ANALYTICS_TRACK_ID);
    ReactGA.pageview(window.location.pathname);
  }
};
